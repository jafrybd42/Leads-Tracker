import axios from "axios";
import localStorageService from "./localStorageService";
import history from '../../history'
class JwtAuthService {

  // Dummy user object just for the demo
  // constructor(){
    user ={}
  //   user = this.state.user
  // }
  //  user = {
  //   // userId: "1",
  //   // role: 'ADMIN',
  //   // displayName: "Jason Alexander",
  //   // email: "jasonalexander@gmail.com",
  //   // photoURL: "/assets/images/face-6.jpg",
  //   // age: 25,
  //   // token: "faslkhfh423oiu4h4kj432rkj23h432u49ufjaklj423h4jkhkjh"
  // }

  // You need to send http request with email and passsword to your server in this method
  // Your server will return user object & a Token
  // User should have role property
  // You can define roles in app/auth/authRoles.js
  loginWithEmailAndPassword = (email, password) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.user);
      }, 1000);
    }).then(data => {
      // Login successful
      // Save token
      this.setSession(data.token);
      // Set user
      this.setUser(data);
      return data;
    });
  };

  loginPhonePass = (phone, password) => {
    try {
      //console.log(phone)
      const token =  axios.post("http://127.0.0.1:3001/intraco/lpg/authentication/login", { phone, password })
      .then(res => {
        //console.log(res);
        //console.log(res.data.data);

          
          user = {
            "userId": res.data.data.profile.id,
            "role": res.data.data.user_type,
            "displayName": res.data.data.profile.name,
            "phone": res.data.data.profile.phone_number,
            "email": res.data.data.profile.email,
            "photoURL": '/' + res.data.data.profile.image,
            "age": 24,
            "token": res.data.data.token
          }


          // this.setUser = {
          //   "userId": res.data.data.profile.id,
          //   "role": res.data.data.user_type,
          //   "displayName": res.data.data.profile.name,
          //   "phone": res.data.data.profile.phone_number,
          //   "email": res.data.data.profile.email,
          //   "photoURL": '/' + res.data.data.profile.image,
          //   "age": 24,
          //   "token": res.data.data.token
          // }

         

          // return new Promise((resolve, reject) => {
          //   setTimeout(() => {
          //     resolve(this.user);
          //   }, 1000);
          // }).then(data => {
            // Login successfuls
            // Save token

             // Login successfuls
              // Save token
              this.setUser(user);
              //console.log('token:'+res.data.data.token);
              this.setSession(res.data.data.token);
              localStorage.setItem("jwt_token", res.data.data.token);
              axios.defaults.headers.common["Authorization"] = "Bearer " + token;
              // Set user
              // //console.log(res.data.data);
              

              history.push({
                pathname: "/"
              });
            // //console.log(state?this.state.user:'error');
            return data;
          // });
            
    })
    
} catch (err) {
    this.setState({
        error: "Wrong Phone Number / Password !"
    })
    history.push({
      pathname: "/signin"
    });
}
}
  // You need to send http requst with existing token to your server to check token is valid
  // This method is being used when user logged in & app is reloaded
  loginWithToken = () => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.user);
      }, 100);
    }).then(data => {
      // Token is valid
      this.setSession(data.token);
      this.setUser(data);
      return data;
    });
  };

  logout = () => {
    this.setSession(null);
    this.removeUser();
  }

  // Set token to all http request header, so you don't need to attach everytime
  setSession = token => {
    if (token) {
      localStorage.setItem("jwt_token", token);
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
    } else {
      localStorage.removeItem("jwt_token");
      delete axios.defaults.headers.common["Authorization"];
    }
  };

  // Save user to localstorage
  setUser = (user) => {    
    localStorageService.setItem("auth_user", user);
  }
  // Remove user from localstorage
  removeUser = () => {
    localStorage.removeItem("auth_user");
  }
}

export default new JwtAuthService();
