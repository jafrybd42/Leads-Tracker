import routes from '../../RootRoutes'
import { isEqual, merge } from 'lodash'
import { matchRoutes } from "react-router-config"

export const SET_LAYOUT_SETTINGS = "LAYOUT_SET_SETTINGS";
export const SET_DEFAULT_LAYOUT_SETTINGS = "LAYOUT_SET_DEFAULT_SETTINGS";
export const SET_LAYOUT_LOADING = "LAYOUT_LOADING"

export const setLayoutSettings = (data = null, path) => (dispatch, getState) => {
  /**
   * Sets the applications layout
   * @param {Object} [null] data Settings data object for immediate execution
   * @param {String} path Path to fetch settings for
   * @param {String} isMdScreen Either close or open display
   * @return {Func}
   */

  /*
    If data is passed immediately dispatch the new data
  */
  if (data) {
    return dispatch({
      type: SET_LAYOUT_SETTINGS,
      data: data
    });
  }

  /*
    Pull settings from state
  */
  let settings = getState().layout.settings;
  let defaultSettings = getState().layout.defaultSettings;

  /*
    Gets settings from route definition and compare with path argument

    This is the same code that you had in MatxLayoutSFC
  */
  const matched = matchRoutes(routes, path)[0];

  if (matched && matched.route.settings) {

    const updatedSettings = merge({}, settings, matched.route.settings);

    if (!isEqual(settings, updatedSettings)) {
      return dispatch({
        type: SET_LAYOUT_SETTINGS,
        data: updatedSettings
      });
    }

  } else if (!isEqual(settings, defaultSettings)) {
    
    return dispatch({
      type: SET_LAYOUT_SETTINGS,
      data: defaultSettings
    });
  }
};

export const setDefaultSettings = data => dispatch => {
  dispatch({
    type: SET_DEFAULT_LAYOUT_SETTINGS,
    data: data
  });
};
