import history from "history.js";
import jwtAuthService from "../../services/jwtAuthService";
import { merge } from "lodash";

export const SET_USER_DATA = "USER_SET_DATA";
export const REMOVE_USER_DATA = "USER_REMOVE_DATA";
export const USER_LOGGED_OUT = "USER_LOGGED_OUT";
export const UPDATE_USER_PROFILE = "UPDATE_USER_PROFILE";

export function setUserData(user) {
  return dispatch => {
    dispatch({
      type: SET_USER_DATA,
      data: user
    });
  };
}
export const updateUserProfile = (name,email) => (dispatch, getState) => {
  const newUser = merge({}, getState().user, { displayName: name , email : email }); 

  jwtAuthService.setUser(newUser)

  return dispatch({
    type: UPDATE_USER_PROFILE,
    data: newUser
  });
}

export function logoutUser() {
  return dispatch => {
    jwtAuthService.logout();

    history.push({
      pathname: "/signin"
    });

    dispatch({
      type: USER_LOGGED_OUT
    });
  };
}
