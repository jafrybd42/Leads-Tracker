export const authRoles = {
  sa: ['SA'], // Only Super Admin has access
  admin: ['SA', 'ADMIN',2], // Only SA & Admin has access
  // admin: [2,'2'], // Only SA & Admin has access
  editor: ['SA', 'ADMIN', 'EDITOR','SUB-ADMIN'], // Only SA & Admin & Editor has access
  guest: ['GUEST','SALES-PERSON'], // Everyone has access
  md : ['Managing Director']


}

// Check out app/views/dashboard/DashboardRoutes.js
// Only SA & Admin has dashboard access

// const dashboardRoutes = [
//   {
//     path: "/dashboard/analytics",
//     component: Analytics,
//     auth: authRoles.admin <----------------
//   }
// ];