import React from "react";

const Brand = ({ children }) => {
  return (
    <div className="flex items-center justify-between brand-area">
      <div className="flex items-center brand">
        <a href = '/' >
        <img src="/assets/logo.svg" alt="company-logo" style={{ height: '40px', marginRight: '-2px', marginTop: '5px' }} />
        </a>
      </div>
      {/* {children} */}
    </div>
  );
};

export default Brand;
