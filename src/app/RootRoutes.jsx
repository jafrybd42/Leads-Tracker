import React from "react";
import { Redirect } from "react-router-dom";
import dashboardRoutes from "./views/dashboard/DashboardRoutes";
import sessionRoutes from "./views/sessions/SessionRoutes";

import superAdminRoutes from "./views/superAdmin/superAdminRoutes";
import subAdminRoutes from "./views/subAdmin/subAdminRoutes"
import SalesPersonRoutes from "./views/salesPerson/salesPersonRoutes";
import ClientTypeRoutes from "./views/clientType/clientTypeRoutes";
import DiscussionTypeRoutes from "./views/discusstionType/discussionTypeRoutes"
import ProspectTypeRoutes from './views/prospectType/prospectTypeRoutes'
import VehicleTypeRoutes from './views/vehicleType/vehicleTypeRoutes'
import CallTypeRoutes from "./views/callType/callTypeRoutes";
import LeadsRoutes from './views/leads/leadsRoutes'
import changePassword from "./views/changPassword/changePassword";
import editProfile from './views/editProfile/editProfile'
import Profile from './views/profile/profile'
import ClientRoutes from './views/client/clientRoutes'
import TaskRoutes from './views/task/taskRoutes'
import AppointmentRoutes from './views/appointment/appointmentRoutes'
import ForgetPassRoutes from './views/forgotPassword/forgetPassRoutes'
import taskExchangeRoutes from './views/taskExchange/taskExchangeRoutes'
import ProfileSales from "./views/salesPersonProfile/profileSales";
import MDRoutes from "./views/md/mdRoutes";
import ReportRoutes from "./views/report/reportRoutes";
import CompanyRoutes from "./views/company/companyRoutes"
import SisterConcernRoutes from "./views/sisterConcern/sisterconcernRoutes";
import clientExchangeRoutes from './views/clientExchange/clientExchangeRoutes'
import ReminderRoutes from "./views/reminder/reminderRoutes";
const redirectRoute =
  [
    {

      path: "/",
      exact: true,
      component: () => <Redirect to="/signin" />

    }
  ]


const errorRoute = [
  {
    component: () => <Redirect to="/session/404" />
  }
];

const routes = [
  ...sessionRoutes,
  ...dashboardRoutes,
  ...redirectRoute,
  ...superAdminRoutes,
  ...subAdminRoutes,
  ...SalesPersonRoutes,
  ...ClientTypeRoutes,
  ...DiscussionTypeRoutes,
  ...ProspectTypeRoutes,
  ...VehicleTypeRoutes,
  ...CallTypeRoutes,
  ...LeadsRoutes,
  ...changePassword,
  ...editProfile,
  ...Profile,
  ...ClientRoutes,
  ...TaskRoutes,
  ...AppointmentRoutes,
  ...ForgetPassRoutes,
  ...taskExchangeRoutes,
  ...ProfileSales,
  ...MDRoutes,
  ...ReportRoutes,
  ...CompanyRoutes,
  ...SisterConcernRoutes,
  ...clientExchangeRoutes,
  ...ReminderRoutes,
  ...errorRoute
];

export default routes;
