import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,
  // Radio,
  // RadioGroup,
  // FormControlLabel,
  // Checkbox
} from "@material-ui/core";
// import {
//   MuiPickersUtilsProvider,
//   KeyboardDatePicker
// } from "@material-ui/pickers";
import "date-fns";
// import DateFnsUtils from "@date-io/date-fns";
// import themeOptions from "app/MatxLayout/MatxTheme/themeOptions";
import myApi from "app/auth/api";

class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      name: "",
      firstName: "",
      email: "",
      date: new Date(),
      creditCard: "",
      phone: "",
      password: "",
      confirmPassword: "",
      gender: "",
      agreement: ""
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    // let { user } = this.props
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {

    axios.post(myApi + '/intraco/lpg/client_type/add', { 'type': this.state.name }, {

      "headers": {
        "x-access-token": localStorageService.getItem("auth_user").token
      }
    })

      .then(res => {

        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

          if (res.data.error === false) {
            this.setState({
              name: '',

            })
          }
        }

        
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    this.setState({ date });
  };

  render() {
    let {
      name,
    
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Client Type (Max length : 25)"
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={[
                  "required",
                  "maxStringLength: 25"
                ]}
                errorMessages={["this field is required"]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
