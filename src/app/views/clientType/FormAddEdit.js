import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
function AddEditForm(props) {
  const [item, setItem] = useState([])
  const [form, setValues] = useState({
    id: 0,
    type: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    active_status: 0,
  })

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

    axios.post(myApi + '/intraco/lpg/client_type/update', {
      'id': form.id, 'type': form.type
    }, {

      "headers": {
        'x-access-token': localStorageService.getItem("auth_user").token
      }
    })

      .then(response => {
        //console.log(response.data)
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }

        props.toggle()
        if (response.data.error === false) {
          props.updateState(form)
        }

      })


      .catch(err => console.log(err))
  }


  useEffect(() => {
    if (props.item) {
      const { id, type, last, email, phone_number, location, hobby, active_status } = props.item
      setValues({ id, type, last, email, phone_number, location, hobby, active_status })
    }
  }, false)

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Full Name</Label>
        <Input type="text" name="type" id="type" onChange={onChange} value={form.type === null ? '' : form.type} minLength='3' required />
      </FormGroup>

      <Button>Submit</Button>
    </Form>
  )
}

export default AddEditForm