import React from 'react'
import { Table, Button } from 'reactstrap';
import ModalForm from './Modal'
import axios from 'axios'
import myApi from '../../auth/api'
function DataTable(props) {
  const deleteItem = id => {
    let confirmDelete = window.confirm('Delete item forever?')
    if (confirmDelete) {
      //   fetch('http://127.0.0.1:3001/intraco/lpg/admin/subAdmin/delete', {
      //   method: 'post',
      //   headers: {
      //     // 'Content-Type': 'application/json'
      //     'x-access-token' :  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZV9udW1iZXIiOiIwMTc3NzY4Nzk3MiIsInVzZXJfaWQiOjE0LCJpZCI6MTQsIm5hbWUiOiJqYWZyeSIsInVzZXJfdHlwZSI6MiwiaWF0IjoxNjAxMjE1NjgzLCJleHAiOjE2MDEzMDIwODN9._NUDJ881gogI0kUi6GhscgVfexgiDb3TVjTlhtZyKpo'

      //   },
      //   body: JSON.stringify({
      //     id
      //   })
      // })
      axios.post(myApi+'/intraco/lpg/call_type/delete', { 'id': id }, {

        "headers": {
          'x-access-token':  localStorage.getItem("jwt_token")
        }
      }) 
          .then(response => {
            // response.json()
            //console.log(response)
          })
          .then(item => {
            props.deleteItemFromState(id)
          })
          .catch(err => console.log(err))
      }
  }

    const items = props.items && props.items.map((item,i) => {
      return (
        <tr key={item.id}>
          <th scope="row">{i+1}</th>
          <td>{item.type}</td>
          {/* <td>{item.phone_number}</td>
          <td>{item.email}</td> */}

          <td>
            <div style={{ width: "150px" }}>
              <ModalForm buttonLabel="Edit" item={item} updateState={props.updateState} />
              {' '}
              <Button color="danger" onClick={() => deleteItem(item.id)}>Delete</Button>
            </div>
          </td>
        </tr>
      )
    })

    return (
      <Table responsive hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Type</th>
            {/* <th>Phone</th>
            <th>Email</th> */}
            <th>Action</th>


          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    )
  }

  export default DataTable