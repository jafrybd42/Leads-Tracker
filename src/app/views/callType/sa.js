import React, { useState, useEffect } from 'react'
import { Container, Row, Col } from 'reactstrap'
import ModalForm from './Modal'
import DataTable from './DataTable'
import { CSVLink } from "react-csv"
import axios from 'axios'
import { Breadcrumb } from "matx";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'

function Sa(props) {

  const [items, setItems] = useState([])

  const getItems = () => {
    axios({
      "method": "GET",
      "url": myApi+"/intraco/lpg/call_time/list",
      "headers": {

        "x-access-token":  localStorageService.getItem("auth_user").token
       }    
    })
    .then((response) => {
      // //console.log(token)
      //console.log(response.data.data)
      setItems(Array.isArray(response.data.data)?response.data.data:[])
    })
    .catch((error) => {
      //console.log(error)
    })
      // .then(response => response.json())
      // .then(items => setItems(items))
      // .catch(err => console.log(err))
  }
//console.log(items)
  const addItemToState = (item) => {
    setItems([...items, item])
  }

  const updateState = (item) => {
    const itemIndex = items.findIndex(data => data.id === item.id)
    const newArray = [...items.slice(0, itemIndex), item, ...items.slice(itemIndex + 1)]
    setItems(newArray)
  }

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter(item => item.id !== id)
    setItems(updatedItems)
  }

  useEffect(() => {
    getItems()
  }, []);

  return (
    <div className="m-sm-30">
        <div  className="mb-sm-30">
    <Container className="App">
      
      <Row>
        <Col>
        <Breadcrumb
        routeSegments={[
          { name: "Call Type", path: "/callType/edit" },
          { name: "Manage Call Type" }
        ]}
      />
          <h1 style={{ margin: "20px 0" }}></h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <DataTable items={items} updateState={updateState} deleteItemFromState={deleteItemFromState} />
        </Col>
      </Row>
      <Row>
        <Col>
          {/* <CSVLink
            filename={"db.csv"}
            color="primary"
            style={{ float: "left", marginRight: "10px" }}
            className="btn btn-primary"
            data={items}>
            Download CSV
            </CSVLink> */}
          {/* <ModalForm buttonLabel="Add Item" addItemToState={addItemToState} /> */}
        </Col>
      </Row>
    </Container>
    </div>
    </div>
  )
}

export default Sa