/* eslint-disable no-dupe-keys */

import React from "react";
import { authRoles } from "../../auth/authRoles";

const CallTypeRoutes = [
  {
    path: "/callType/create",
    component: React.lazy(() => import("./createCallType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/callType/manage",
    component: React.lazy(() => import("./manageCallType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/callType/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }
];

export default CallTypeRoutes;
