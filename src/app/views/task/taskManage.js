/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import ModalForm from "./Modal";
import DataTable from "./DataTable";
import { CSVLink } from "react-csv";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";
import Moment from "moment";
import PDFTable from "./pdf";
import { PDFViewer } from "@react-pdf/renderer";

import { Document, Page } from "react-pdf";
function TaskManage(props) {
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios({
      method: "POST",
      url: myApi + "/intraco/lpg/task/list",
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((response) => {
        console.log(response)
        // let items = response.data.data.filter()
        setItems(Array.isArray(response.data.data)?response.data.data:[]);
      })
      .catch((error) => {
        console.log(error);
      });

  };

  const addItemToState = (item) => {
    setItems([...items, item]);
  };


  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);

    let copyItems = [...items];

    // Add new item
    copyItems.splice(itemIndex, 1, item);

    // Filter for current date
    let filteredArray = copyItems.filter((item) => {
      let itemDate = Moment(item.call_date).format("YYYY-MM-DD");
      let today = Moment(new Date()).format("YYYY-MM-DD");
      return itemDate === today;
    });

    setItems(filteredArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Task", path: "/task/edit" },
                  { name: "Task Manage" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>


          <br></br>
          <Row>
            <Col>
              <DataTable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default TaskManage;
