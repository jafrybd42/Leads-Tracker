import React from 'react'
import { Table, Button } from 'reactstrap';
import ModalForm from './Modal'
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
import Moment from 'moment'
function DataTableReminder(props) {
  const resetPassword = id => {
    let confirmReset = window.confirm('Reset Account Password?')
    if (confirmReset) {
      axios.post(myApi+'/intraco/lpg/salesPerson/profile/resetPassword', { 'id': id }, {

        "headers": {
          'x-access-token': localStorage.getItem("jwt_token")
        }
      }) 
          .then(response => {
            // response.json()
            //console.log(response)
            if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
          })
          // .then(item => {
          //   props.deleteItemFromState(id)
          // })
          .catch(err => console.log(err))
      }
  }
  const deleteItem = id => {
    let confirmDelete = window.confirm('Delete item forever?')
    if (confirmDelete) {
      axios.post(myApi+'/intraco/lpg/client/profile/delete', { 'id': id }, {

        "headers": {
          'x-access-token':  localStorageService.getItem("auth_user").token
        }
      }) 
          .then(response => {
            // response.json()
            //console.log(response)
          })
          .then(item => {
            props.deleteItemFromState(id)
            Swal.fire('Deleted Successfully !')
          })
          .catch(err => console.log(err))
      }
  }

    const items = props.items && props.items.map(item => {
      return (
        <tr key={item.id}>
          {/* <th scope="row">{item.id}</th> */}
          <td>{item.discussion_type}</td>
          <td>{item.vehicle_type}</td>
          {/* <td>{item.client_name}</td> */}
          {/* <td>{Moment(item.call_date).format('YYYY-MM-DD')}</td> */}
          <td>{item.call_at}</td>
          <td>{item.leads}</td>
          {/* <td> 
            <div style={{ width: "120px" }}>

            <Button className="btn btn-light" onClick={() => resetPassword(item.id)}><img src='/assets/reset-password.png' style={{height:'32px'}} /></Button>
          </div>
          </td>  */}

         
        </tr>
      )
    })

    return (
     
      <Table responsive hover>
        <thead>
          <tr>
            {/* <th>ID</th> */}
            <th>Discussion type</th>
            <th>Vehicle type</th>
            {/* <th>Call Time</th> */}
            {/* <th>Date</th> */}
            <th>Call Time</th>
            <th>Lead</th>
            {/* <th>Update</th> */}


          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    
      
    )
  }

  export default DataTableReminder