/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useCallback } from "react";
import { Table } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";
import Moment from "moment";
import ModalTable from "./ModalTable";
import history from "../../../history";
import { Button } from "reactstrap";

function DataTable(props) {
  const [discussionTypes, setDiscussionTypes] = useState([]);
  const [clientTypes, setClientTypes] = useState([]);
  const [vehicleTypes, setVehicleTypes] = useState([]);
  const [callTimeTypes, setCallTimeTypes] = useState([]);
  const [prospectTypes, setProspectTypes] = useState([]);
  const [lead_id, setlead_id] = useState([]);
  const [client_Types, setClientTypesNew] = useState([
    { id: 0, type: "Corporate" },
    { id: 1, type: "Individual" },
  ]);

  useEffect(
    useCallback(() => {
      // getting discussionType
      axios
        .get(myApi + "/intraco/lpg/discussion_type/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((discussionType) => {
            return {
              id: discussionType.id,
              type: discussionType.type,
            };
          });

          setDiscussionTypes(
            [
              {
                id: "",
                type: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
        });

      // getting clientType
      axios
        .get(myApi + "/intraco/lpg/client/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((client) => {
            return {
              id: client.id,
              name: client.name,
            };
          });

          setClientTypes(
            [
              {
                id: "",
                name: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
          console.log(error);
        });
      // getting vehicleType
      axios
        .get(myApi + "/intraco/lpg/vehicle_type/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((vehicleType) => {
            return {
              id: vehicleType.id,
              type: vehicleType.type,
            };
          });

          setVehicleTypes(
            [
              {
                id: "",
                type: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
          //console.log(error);
        });
      // getting callType
      axios
        .get(myApi + "/intraco/lpg/call_time/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((callTime) => {
            return {
              id: callTime.id,
              type: callTime.type,
            };
          });

          setCallTimeTypes(
            [
              {
                id: "",
                type: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
          //console.log(callTimeTypes);
        })
        .catch((error) => {
          //console.log(error);
        });
      // getting prospectType
      axios
        .get(myApi + "/intraco/lpg/prospect_type/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((prospectType) => {
            return {
              id: prospectType.id,
              type: prospectType.type,
            };
          });

          setProspectTypes(
            [
              {
                id: "",
                type: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
        });

      // getting lead_id
      axios
        .get(myApi + "/intraco/lpg/lead/activeList", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((lead) => {
            return {
              id: lead.id,
              name: lead.name,
            };
          });

          setlead_id(
            [
              {
                id: "",
                name: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
          console.log(error);
        });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
      callTimeTypes,
      clientTypes,
      discussionTypes,
      lead_id,
      prospectTypes,
      vehicleTypes,
    ]),
    []
  );

  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };

  const items =
  // typeof props.items === function &&
    props.items &&
    props.items.map((item, i) => {
      return (
        <tr key={item.id}>
          <th scope="row">{i + 1}</th>
          <td>{item.client_type_id === 0 ? "Corporate" : "Individual"}</td>

          <td>{item.client_info.map((e) => e.name || e.company_title + '_' + e.title)}</td>

          <td>
            {item.discussion_type.map(function (item, index) {
              return (
                <span key={`demo_snap_${index}`}>
                  {(index ? ", " : "") + item.type}
                </span>
              );
            })}
          </td>

          <td align="center">
            {item.prospect_type_id === 0
              ? "Default"
              : locateValueById(prospectTypes, item.prospect_type_id)
              ? locateValueById(prospectTypes, item.prospect_type_id).type
              : item.prospect_type}
          </td>
          <td>{Moment(item.call_time, "hh:mm").format("LT")}</td>

          <td>
            {item.status && item.status === "complete" ? (
              <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                Completed
              </small>
            ) : (
              <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                Incomplete
              </small>
            )}
          </td>
          <td>
            <div style={{ width: "40px" }}>
              <ModalTable
                style={{ maxWidth: "700px!important" }}
                buttonLabel="View"
                item={item}
              />


            </div>
          </td>
          <td>
            <div style={{ width: "150px" }}>

              <Button
                onClick={() =>
                  history.push({
                    pathname:
                      "/task/update/" + item.id,
                    state: item,
                  })
                }
                item={item}
                color="btn btn-warning"
                style={{ marginLeft: "5px", marginTop: "0px" }}
              >
                Edit
              </Button>
            </div>
          </td>
        </tr>
      );
    }) ;

  return (
    <Table responsive hover>
      <thead>
        <tr>
          <th>ID</th>
          <th>Client Type</th>
          <th>Client</th>
          <th>Discussion Type</th>
          <th>Prospect Type</th>
          <th>Call Time</th>
          <th>Status</th>
          <th>Details</th>
          <th>Update</th>
        </tr>
      </thead>
      <tbody>{items}</tbody>
    </Table>
  );
}

export default DataTable;
