import React from 'react'
import { Table, Button } from 'reactstrap';
import ModalForm from './Modal'
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
import Moment from 'moment'
function DataTableTwo(props) {
  const resetPassword = id => {
    let confirmReset = window.confirm('Reset Account Password?')
    if (confirmReset) {
      axios.post(myApi+'/intraco/lpg/salesPerson/profile/resetPassword', { 'id': id }, {

        "headers": {
          'x-access-token': localStorage.getItem("jwt_token")
        }
      }) 
          .then(response => {
            // response.json()
            //console.log(response)
            if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
          })
          // .then(item => {
          //   props.deleteItemFromState(id)
          // })
          .catch(err => console.log(err))
      }
  }
  const deleteItem = id => {
    let confirmDelete = window.confirm('Delete item forever?')
    if (confirmDelete) {
      axios.post(myApi+'/intraco/lpg/client/profile/delete', { 'id': id }, {

        "headers": {
          'x-access-token':  localStorageService.getItem("auth_user").token
        }
      }) 
          .then(response => {
            // response.json()
            //console.log(response)
          })
          .then(item => {
            props.deleteItemFromState(id)
            Swal.fire('Deleted Successfully !')
          })
          .catch(err => console.log(err))
      }
  }

    const items = props.items && props.items.map((item,i) => {
      return (
        <tr key={item.id}>
          <td>{i+1}</td>
          <td>{item.sales_person}</td>
          <td>{item.discussion_type}</td>
          <td>{item.vehicle_type}</td>
          <td>{item.call_at}</td>
          <td>{Moment(item.call_date).format('YYYY-MM-DD')}</td>
          <td>{item.leads}</td>
        </tr>
      )
    })

    return (
     
      <Table responsive hover
      // {...getTableProps()}
      style={{marginTop : '10px'}}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Sales Person</th>
            <th>Discussion type</th>
            <th>Vehicle type</th>
            <th>Call Time</th>
            <th>Date</th>
            {/* <th>Call Time</th> */}
            <th>Lead</th>
            {/* <th>Update</th> */}


          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    
      
    )
  }

  export default DataTableTwo