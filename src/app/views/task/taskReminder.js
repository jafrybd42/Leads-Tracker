/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import myApi from "../../auth/api";
import history from "../../../history";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";

import Moment from "moment";
import {
  Card,
  Icon,
  IconButton,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import localStorageService from "app/services/localStorageService";
const TaskReminder = (props) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [client_type_arrayy, setClientTypeArray] = useState([]);
  const [client_type_array, setClientType] = useState([]);
  const [taskReminderList, setTaskReminderList] = React.useState([]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const fetchData = React.useCallback(() => {
    axios({
      method: "POST",
      url: myApi + "/intraco/lpg/task/list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        let responsedData = response.data.data;

        if (responsedData) {
          setTaskReminderList(
            Array.isArray(response.data.data) ? response.data.data : []
          );
        }
        if (
          response.data.message &&
          response.data.message === "Timeout Login Fast"
        ) {
          props.logoutUser();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  React.useEffect(() => {
    fetchData();
    // getting clientType
    axios
      .get(myApi + "/intraco/lpg/client_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user") && localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientType([].concat(clientsFromApi));
        setClientTypeArray(
          [
            {
              value: 0,
              label: "Corporate Client",
              id: 0,
              type: "Corporate Client",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, [fetchData]);
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };
  const handleCompleteTask = (id) => {
    axios
      .post(
        myApi + "/intraco/lpg/task/complete",
        {
          task_id: id,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )
      .then((response) => {
        if (response.data.error === false) {
          Swal.fire({
            icon: "success",
            title: "Completed Successful",
            showConfirmButton: false,
            timer: 1000,
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
        }

        history.push({
          pathname: "/",
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const fs = require("fs");
  // console.log(taskReminderList)
  return (
    <Card elevation={3} className="pt-5 mb-6">
      <div className="card-title px-6 mb-3">Task For Today</div>
      <div className="overflow-auto">
        <Table className="product-table">
          {taskReminderList.length === 0 ? (
            <div
              className="alert alert-info"
              role="alert"
              style={{ width: "100%", background: "white", border: "white" }}
            >
              No more tasks for Today !
            </div>
          ) : (
              <TableHead>
                <TableRow>
                  <TableCell className="px-6" colSpan={3} align="center">
                    Client Type
                </TableCell>
                  <TableCell className="px-0" colSpan={3} align="center">
                    Client
                </TableCell>
                  <TableCell className="px-0" colSpan={2} align="center">
                    Discussion Type
                </TableCell>
                  <TableCell className="px-0" colSpan={2} align="center">
                    Call AT
                </TableCell>
                  <TableCell className="px-0" colSpan={2} align="center">
                    Action
                </TableCell>
                </TableRow>
              </TableHead>
            )}
          {taskReminderList.length !== 0 &&
            taskReminderList
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((user) => (
                <TableBody>
                  <TableRow key={user.id}>
                    <TableCell
                      className="px-0 capitalize"
                      colSpan={3}
                      align="left"
                    >
                      <div className="flex items-center">
                        <img
                          className="circular-image-small"
                          src="/assets/task.png"
                          alt="task"
                        />
                        <p className="m-0 ml-8">
                          {locateValueById(
                            client_type_arrayy,
                            user.client_type_id
                          ) &&
                            locateValueById(
                              client_type_arrayy,
                              user.client_type_id
                            ).type}
                        </p>
                      </div>
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      colSpan={3}
                    >
                      {user.client_info.map((e) =>
                        e.name ? (
                          <a
                            // href=''
                            onClick={() =>
                              history.push({
                                pathname:
                                  "/individual/" +
                                  Number(
                                    user.client_info.map((e) => e.id).toString()
                                  ),
                                state: user,
                              })
                            }
                          >
                            {" "}
                            <font
                              color="blue"
                              style={{
                                cursor: "pointer",
                                fontWeight: "500",
                                textAlign: "center",
                              }}
                            >
                              {e.name}
                            </font>
                          </a>
                        ) : (
                            <a
                              // href=''
                              onClick={() =>
                                history.push({
                                  pathname:
                                    "/client/member/list/" +
                                    Number(
                                      user.client_info.map((e) => e.id).toString()
                                    ),
                                  state: Number(
                                    user.client_info.map((e) => e.id).toString()
                                  ),
                                })
                              }
                            >
                              {" "}
                              <font
                                color="blue"
                                style={{
                                  cursor: "pointer",
                                  fontWeight: "500",
                                  textAlign: "center",
                                }}
                              >
                                {e.company_title + "_" + e.title}{" "}
                              </font>
                            </a>
                          )
                      )}
                    </TableCell>

                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      colSpan={2}
                    >
                      <p className="m-0 ml-8">
                        {user.discussion_type.map(function (item, index) {
                          return (
                            <span key={`demo_snap_${index}`}>
                              {(index ? ", " : "") + item.type}
                            </span>
                          );
                        })}
                      </p>
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      colSpan={2}
                    >
                      {Moment(user.call_time, "hh:mm").format("LT")}
                    </TableCell>

                    <TableCell className="px-0" align="center" colSpan={2}>
                      {user.status && user.status === "complete" ? (
                        user.status === "complete" ? (
                          <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                            Completed
                          </small>
                        ) : (
                            <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                              Incomplete
                            </small>
                          )
                      ) : (
                          <IconButton onClick={() => handleCompleteTask(user.id)}>
                            <Icon color="primary">done</Icon>
                          </IconButton>
                        )}
                    </TableCell>
                  </TableRow>
                </TableBody>
              ))}
        </Table>
        {
          taskReminderList.length !== 0 &&  taskReminderList.length > 5 &&
          <TablePagination
            className="px-4"
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={taskReminderList && taskReminderList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              "aria-label": "Previous Page",
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page",
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        }
      </div>
    </Card>
  );
};

TaskReminder.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(
  connect(mapStateToProps, { logoutUser })(TaskReminder)
);
