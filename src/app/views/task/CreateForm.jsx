import React, { Component } from "react";
import { ValidatorForm } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import Moment from 'moment'
import {

  Button,
  Icon,
  Grid,

} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import Select from 'react-select';

class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      selectedVehicleCorporateNew: [],
      selectedDiscussionCorporateNew: [],
      leads_corporate: [],
      discussionTypes_corporate: [],
      clients_corporate: [],
      prospectTypes_corporate: [],
      vehicleTypes_corporate: [],
      clients: [],
      selectedClient: "",
      propspectTypes: [],
      selectedProspectType: '',
      discussionTypes: [],
      selectedDiscussionType: "",
      CallTimeTypes: [],
      selectedCallTimeType: "",
      VehicleTypes: [],
      selectedVehicleType: "",
      selectedVehicleIndividualNew: [],
      Leads: [],
      selectedLead: "",

      validationError: '',
      date: Moment(new Date()).format('YYYY-MM-DD'),
      time: new Date(),
      newTime: Moment(new Date()).format('HH:mm'),


      client_type_array: [],

      selectedClientType: '',
      selectedClientCorporate: '',
      selectedProspectTypeCorporate: '',
      selectedVehicleTypeCorporate: '',
      selectedLeadCorporate: '',

      selectedDiscussionTypeIndividual: '',
      selectedLeadIndividual: '',
      selectedProspectTypeIndividual: '',
      selectedVehicleTypeIndividual: '',
      selectedClientIndividual: '',
      isSubmitted: false,
      selectedDiscussionIndividualNew: []
    }
  }

  componentDidMount() {
    console.log(this.state.clients)

    console.log(this.state)
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

    // getting clientType
    axios.get(
      myApi + "/intraco/lpg/client_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.type
          };
        });

        this.setState({
          client_type_array: []
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        // let filterClient = res.data.data &&
        //   res.data.data.filter((x) => x.client_type_id === this.state.client_type_id)
        // console.log(filterClient)
        // let clientsFromApi = res.data.data &&
        //   res.data.data.map(client =>
        //     this.state.selectedClientType === Number(client.client_type_id) &&
        //     {
        //       value: client.id,
        //       label: client.name,
        //     });

        // this.setState({
        //   clients: []
        //     .concat(clientsFromApi)
        // });
      })
      .catch(error => {
        console.log(error);
      });

    // getting discussionType
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            id: discussionType.id,
            type: discussionType.type
          };
        });

        this.setState({
          discussionTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(propspectType => {
          return {
            id: propspectType.id,
            type: propspectType.type,
            value: propspectType.id,
            label: propspectType.type,
          };
        });

        this.setState({
          propspectTypes: [

          ].concat(clientsFromApi)
        });

        this.setState({
          prospectTypes_corporate: [

          ].concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting vehicleType
    axios.get(
      myApi + "/intraco/lpg/vehicle_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(VehicleType => {
          return {
            id: VehicleType.id,
            type: VehicleType.type,
            value: VehicleType.id,
            label: VehicleType.type
          };
        });

        this.setState({
          VehicleTypes: [

          ]
            .concat(clientsFromApi)
        });
        this.setState({
          vehicleTypes_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting call type
    axios.get(
      myApi + "/intraco/lpg/call_time/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(CallTimeType => {
          return {
            id: CallTimeType.id,
            type: CallTimeType.type
          };
        });

        this.setState({
          CallTimeTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Leads
    axios.get(
      myApi + "/intraco/lpg/lead/activeList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data.map(Lead => {
          return {
            id: Lead.id,
            name: Lead.name,
            value: Lead.id,
            label: Lead.name
          };
        });

        this.setState({
          Leads: [

          ]
            .concat(clientsFromApi)
        });
        this.setState({
          leads_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/task/create/data",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data && res.data.data.corporateClient.map(cl => {
          return {
            value: cl.id,
            label: cl.company_title + '_' + cl.company_sister_concerns_title
          };
        });

        this.setState({
          clients_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting discussionType corporate
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            value: discussionType.id,
            label: discussionType.type
          };
        });

        this.setState({
          discussionTypes_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    console.log(this.state)

    axios.post(myApi + '/intraco/lpg/task/add', {
      "client_type_id": this.state.selectedClientType,
      'client_id': this.state.selectedClientIndividual,
      'discussion_type': this.state.selectedDiscussionTypeIndividual,
      'vehicle_type': this.state.selectedVehicleTypeIndividual,
      'prospect_type': this.state.selectedProspectTypeIndividual ? this.state.selectedProspectTypeIndividual : 0,
      'call_time': this.state.newTime ? this.state.newTime : Moment(new Date()).format('HH:mm'),
      'call_date': this.state.date,
      'lead': this.state.selectedLeadIndividual
    }, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        //console.log(res)
        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          if (res.data.error === false) {
            this.setState({
              selectedClientType: '',
              selectedClientIndividual: "",
              selectedDiscussionTypeIndividual: "",
              selectedLeadIndividual: "",
              selectedVehicleTypeIndividual: "",
              selectedProspectTypeIndividual: "",
              selectedVehicleIndividualNew: [],
              selectedDiscussionIndividualNew: []

            })
          }
        }
      })
  }

  handleSubmitCorporate = event => {

    axios.post(myApi + '/intraco/lpg/task/add', {
      "client_id": this.state.selectedClientCorporate,
      "discussion_type": this.state.selectedDiscussionType,
      "client_type_id": this.state.selectedClientType,
      "vehicle_type": this.state.selectedVehicleTypeCorporate,
      "prospect_type": this.state.selectedProspectTypeCorporate,
      "call_time": this.state.newTime ? this.state.newTime : Moment(new Date()).format('HH:mm'),
      "call_date": this.state.date,
      "lead": this.state.selectedLeadCorporate
    }, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          if (res.data.error === false) {
            this.setState({
              selectedClientCorporate : '',
              selectedClientType: 1,
              selectedDiscussionType: "",
              selectedVehicleTypeCorporate: "",
              selectedLeadCorporate: "",
              selectedProspectTypeCorporate: '',
              selectedDiscussionCorporateNew: [],
              selectedVehicleCorporateNew: []
            })
          }
        }
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
    // selectedClient: e.target.value,

  };

  handleDateChange = date => {
    date = Moment(date).format('YYYY-MM-DD')
    this.setState({ date });
  };

  handleTimeChange = (time) => {
    console.log({ time })
    this.setState(
      { time }
    );

    this.state.newTime = Moment(time).format('HH:mm')

  };

  onSelectChangeDays = value => {

    if (value !== null) {
      this.setState({ selectedClientType: value.value });

      // if (this.state.selectedClientType !== '') {
        // getting clientInformation
        axios.get(
          myApi + "/intraco/lpg/client/list",
          {
            headers: {
              'x-access-token': localStorageService.getItem('auth_user').token
            }
          }
        )
          .then(res => {
            let clientsFromApi = res.data.data &&

              res.data.data
                .filter(x => Number(x.client_type_id) === this.state.selectedClientType)
                .map(client => {
                  console.log(client)
                  return {
                    value: client.id,
                    label: client.name,
                  }
                })

            this.setState({
              clients: []
                .concat(clientsFromApi)
            });
            console.log(this.state.clients)
          })
          .catch(error => {
            // console.log(error);
          });
      // }
    }
    if (value === null) {
      this.setState({ selectedClientType: "" });
      console.log(this.state.selectedClientType)
    }
  };

  onSelectChangeCorporateClient = value => {
    if (value !== null) {
      this.setState({ selectedClientCorporate: value.value });
      console.log(this.state.selectedClientCorporate)
    }
    if (value === null) {
      this.setState({ selectedClientCorporate: "" });
      console.log(this.state.selectedClientCorporate)
    }
  };

  onSelectChangeCorporateDiscussionType = value => {
    if (value !== null) {
      this.setState({ selectedDiscussionType: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedDiscussionCorporateNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });
    }
    if (value === null) {
      this.setState({ selectedDiscussionType: "" });
      console.log(this.state.selectedDiscussionType)
    }
  };

  onSelectChangeCorporateProspectype = value => {
    if (value !== null) {
      this.setState({ selectedProspectTypeCorporate: value.value });
      console.log(this.state.selectedProspectTypeCorporate)
    }
    if (value === null) {
      this.setState({ selectedProspectTypeCorporate: "" });
      console.log(this.state.selectedProspectTypeCorporate)
    }
  };

  onSelectChangeCorporateVehicleType = value => {
    if (value !== null) {
      this.setState({ selectedVehicleTypeCorporate: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedVehicleCorporateNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });
    }
    if (value === null) {
      this.setState({ selectedVehicleTypeCorporate: "" });
      console.log(this.state.selectedVehicleTypeCorporate)
    }
  };

  onSelectChangeCorporateLeads = value => {
    if (value !== null) {
      this.setState({ selectedLeadCorporate: value.value });
      console.log(this.state.selectedLeadCorporate)
    }
    if (value === null) {
      this.setState({ selectedLeadCorporate: "" });
      console.log(this.state.selectedLeadCorporate)
    }
  };

  //individual
  onSelectChangeIndividualClient = value => {
    if (value !== null) {
      this.setState({ selectedClientIndividual: value.value });


    }
    if (value === null) {
      this.setState({ selectedClientIndividual: "" });
      console.log(this.state.selectedClientIndividual)
    }



  };

  onSelectChangeIndividualDiscussionType = value => {
    if (value !== null) {
      this.setState({ selectedDiscussionTypeIndividual: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedDiscussionIndividualNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });
    }
    if (value === null) {
      this.setState({ selectedDiscussionTypeIndividual: "" });
      console.log(this.state.selectedDiscussionTypeIndividual)
    }
  };

  onSelectChangeIndividualProspectype = value => {
    if (value !== null) {
      this.setState({ selectedProspectTypeIndividual: value.value });
      console.log(this.state.selectedProspectTypeIndividual)
    }
    if (value === null) {
      this.setState({ selectedProspectTypeIndividual: "" });
      console.log(this.state.selectedProspectTypeIndividual)
    }
  };

  onSelectChangeIndividualVehicleType = value => {
    if (value !== null) {
      this.setState({ selectedVehicleTypeIndividual: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedVehicleIndividualNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });
    }
    if (value === null) {
      this.setState({ selectedVehicleTypeIndividual: "" });
      console.log(this.state.selectedVehicleTypeIndividual)
    }
  };

  onSelectChangeIndividualLeads = value => {
    if (value !== null) {
      this.setState({ selectedLeadIndividual: value.value });
      console.log(this.state.selectedLeadIndividual)
    }
    if (value === null) {
      this.setState({ selectedLeadIndividual: "" });
      console.log(this.state.selectedLeadIndividual)
    }
  };


  render() {
    let { user } = this.props
    let {

      date,
      time

    } = this.state;
    return (
      <div>
        <Grid container spacing={6}>
          <Grid item lg={12} md={6} sm={12} xs={12} style={{ maxWidth: '100%' }}>
            <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong> <font color='black'>Select Client Type</font></strong></div>
            <Select
              style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
              value={this.state.client_type_array.filter(option => option.value === this.state.selectedClientType)}

              isClearable="true"
              name="selectedClientType"
              options={this.state.client_type_array}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={this.onSelectChangeDays}
            />

          </Grid>
        </Grid>
        {this.state.selectedClientType === 0 && this.state.selectedClientType !== '' ?
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmitCorporate}
            onError={errors => null}
          >

            <Grid container spacing={6}>
              <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: '0px' }}>
                <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                  <font color='black'>Select Client</font>
                </strong></div>
                {this.state.selectedClientType === '' &&
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    isClearable="true"
                    name="selectedClient"
                    className="basic-multi-select"
                    classNamePrefix="select"
                  />
                }
                {this.state.selectedClientType !== '' &&
                 <Select
                 style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                 value={this.state.clients_corporate.filter(option => option.value === this.state.selectedClientCorporate)}
                 isClearable="true"
                 name="selectedClientIndividual"
                 options={this.state.clients_corporate && this.state.clients_corporate}
                 className="basic-multi-select"
                 classNamePrefix="select"
                 onChange={this.onSelectChangeCorporateClient}
               />
                }
                <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                  <font color='black'>Select Discussion Type</font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.selectedDiscussionCorporateNew.length !== 0 ? this.state.selectedDiscussionCorporateNew.map(e => ({ label: e.label, value: e.value })) : this.state.selectedDiscussionCorporateNew.map(e => ({ label: e.type, value: e.discussion_type_id }))}
                  isMulti
                  isClearable="true"
                  name="selectedDiscussionType"
                  options={this.state.discussionTypes_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeCorporateDiscussionType}
                />

              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                  <font color='black'>Select Prospect Type</font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.prospectTypes_corporate.filter(option => option.value === this.state.selectedProspectTypeCorporate)}
                  isClearable="true"
                  name="selectedProspectTypeCorporate"
                  options={this.state.prospectTypes_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeCorporateProspectype}
                />

                <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                  <font color='black'>Select Vehicle Type</font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.selectedVehicleCorporateNew.length !== 0 ? this.state.selectedVehicleCorporateNew.map(e => ({ label: e.label, value: e.value })) : this.state.selectedVehicleCorporateNew.map(e => ({ label: e.type, value: e.vehicle_type_id }))}
                  isMulti
                  isClearable="true"
                  name="selectedVehicleTypeCorporate"
                  options={this.state.vehicleTypes_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeCorporateVehicleType}
                />


              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                  <font color='black'>Select Lead </font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.leads_corporate.filter(option => option.value === this.state.selectedLeadCorporate)}
                  isClearable="true"
                  name="selectedLeadsCorporate"
                  options={this.state.leads_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeCorporateLeads}
                />

                <div style={{ marginBottom: '-5px', marginTop: '35px' }}><strong>
                  <font color='black'>Select Date</font>
                </strong></div>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>

                  <KeyboardDatePicker
                    style={{ marginTop: '20px' }}
                    className="mb-4 w-full"
                    margin="none"
                    id="mui-pickers-date"
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    value={date}
                    onChange={this.handleDateChange}
                    KeyboardButtonProps={{
                      "aria-label": "change date"
                    }}
                  />
                </MuiPickersUtilsProvider>

              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: '96px' }}>
                <div style={{ marginBottom: '0px', marginTop: '27x' }}><strong>
                  <font color='black'>Select Time </font>
                </strong></div>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    className="mb-4 w-full"
                    margin="normal"
                    id="time-picker"
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    value={time}
                    onChange={this.handleTimeChange}
                    KeyboardButtonProps={{
                      'aria-label': 'change time',
                    }}
                  />
                </MuiPickersUtilsProvider>

              </Grid>

            </Grid>

            <Button style={{ backgroundColor: '#007bff' }} color="primary" variant="contained" type="submit">
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Submit</span>
            </Button>

          </ValidatorForm>
          :

          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={errors => null}
          >
            <Grid container spacing={6}>
              <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: '0px' }}>
                <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                  <font color='black'>Select Client</font>
                </strong></div>

                {this.state.selectedClientType !== '' &&
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    value={this.state.clients.filter(option => option.value === this.state.selectedClientIndividual)}
                    isClearable="true"
                    name="selectedClientIndividual"
                    options={this.state.clients && this.state.clients}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeIndividualClient}
                  />
                }

                {this.state.selectedClientType === '' &&
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    // value={this.state.clients.filter(option => option.value === this.state.selectedClientIndividual)}
                    isClearable="true"
                    name="selectedClientIndividual"
                    // options={this.state.clients}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeIndividualClient}
                  />
                }
                <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                  <font color='black'>Select Discussion Type</font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.selectedDiscussionIndividualNew.length !== 0 ? this.state.selectedDiscussionIndividualNew.map(e => ({ label: e.label, value: e.value })) : this.state.selectedDiscussionIndividualNew.map(e => ({ label: e.type, value: e.discussion_type_id }))}
                  isMulti
                  isClearable="true"
                  name="selectedDiscussionTypeIndividual"
                  options={this.state.discussionTypes_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeIndividualDiscussionType}
                />

              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                  <font color='black'>Select Prospect Type</font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.prospectTypes_corporate.filter(option => option.value === this.state.selectedProspectTypeIndividual)}
                  isClearable="true"
                  name="selectedProspectTypeIndividual"
                  options={this.state.prospectTypes_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeIndividualProspectype}
                />

                <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                  <font color='black'>Select Vehicle Type</font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.selectedVehicleIndividualNew.length !== 0 ? this.state.selectedVehicleIndividualNew.map(e => ({ label: e.label, value: e.value })) : this.state.selectedVehicleIndividualNew.map(e => ({ label: e.type, value: e.vehicle_type_id }))}
                  isMulti
                  isClearable="true"
                  name="selectedVehicleTypeIndividual"
                  options={this.state.vehicleTypes_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeIndividualVehicleType}
                />


              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                  <font color='black'>Select Lead </font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.leads_corporate.filter(option => option.value === this.state.selectedLeadIndividual)}
                  isClearable="true"
                  name="selectedLeadIndividual"
                  options={this.state.leads_corporate}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeIndividualLeads}
                />

                <div style={{ marginBottom: '-5px', marginTop: '35px' }}><strong>
                  <font color='black'>Select Date</font>
                </strong></div>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>

                  <KeyboardDatePicker
                    style={{ marginTop: '20px' }}
                    className="mb-4 w-full"
                    margin="none"
                    id="mui-pickers-date"
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    value={date}
                    onChange={this.handleDateChange}
                    KeyboardButtonProps={{
                      "aria-label": "change date"
                    }}
                  />
                </MuiPickersUtilsProvider>

              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: '96px' }}>
                <div style={{ marginBottom: '0px', marginTop: '27x' }}><strong>
                  <font color='black'>Select Time </font>
                </strong></div>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardTimePicker
                    className="mb-4 w-full"
                    margin="normal"
                    id="time-picker"
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    value={time}
                    onChange={this.handleTimeChange}
                    KeyboardButtonProps={{
                      'aria-label': 'change time',
                    }}
                  />
                </MuiPickersUtilsProvider>

              </Grid>

            </Grid>

            <Button style={{ backgroundColor: '#007bff' }} color="primary" variant="contained" type="submit">
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Submit</span>
            </Button>

          </ValidatorForm>
        }

      </div>
    );
  }
}

export default CreateForm;
