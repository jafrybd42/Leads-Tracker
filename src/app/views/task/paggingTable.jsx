/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import {
  IconButton,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination,
} from "@material-ui/core";
import Moment from "moment";
import ModalTable from "./ModalTable";
import localStorageService from "app/services/localStorageService";
import Axios from "axios";
import history from "../../../history";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Pagination from "@material-ui/lab/Pagination";
import myApi from "app/auth/api";

export default function BasicDatatable(props) {
  const [prospectTypes, setProspectTypes] = useState([]);
  const [client_type_array, setClientType] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [client_type_arrayy, setClientTypeArray] = useState([]);
  const [form, setValues] = useState({
    id: "",
    active_status: "",
    call_date: "",
    call_time: "",
    vehicle_type: [],
    client_info: [],
    client_type_id: "",
    discussion_type: "",
    lead_id: "",
    leads: "",
    prospect_type_id: "",
    sales_person: "",
    sales_person_id: "",
    status: "",
    created_at: "",
  });

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const items = props.items && props.items;

  useEffect(() => {
    // getting clientType
    Axios.get(myApi + "/intraco/lpg/client_type/list", {
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientType([].concat(clientsFromApi));
        setClientTypeArray(
          [
            {
              value: 0,
              label: "Corporate Client",
              id: 0,
              type: "Corporate Client",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
    // getting prospectType
    Axios.get(myApi + "/intraco/lpg/prospect_type/list", {
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((res) => {
        let clientsFromApi = res.data.data.map((prospectType) => {
          return {
            id: prospectType.id,
            type: prospectType.type,
          };
        });

        setProspectTypes(
          [
            {
              id: "",
              type: "Select if you want to change",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };
  const handleCompleteTask = (item) => {
    Axios.post(
      myApi + "/intraco/lpg/task/complete",
      {
        task_id: item.id,
      },
      {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      }
    )
      .then((response) => {
        Swal.fire({
          icon: "success",
          title: 'Task completed successful',
          showConfirmButton: false,
          timer: 1000
        });

        if (response.data.error === false) {
          props.updateState({
            id: item.id,
            active_status: item.active_status,
            call_date: item.call_date,
            call_time: item.call_time,
            vehicle_type: item.vehicle_type,
            client_info: item.client_info,
            client_type_id: item.client_type_id,
            discussion_type: item.discussion_type,
            lead_id: item.lead_id,
            leads: item.leads,
            prospect_type_id: item.prospect_type_id,
            sales_person: item.sales_person,
            sales_person_id: item.sales_person_id,
            status: "complete",
            created_at: item.created_at,
          });
        }
      })
      .catch((error) => {
        // console.log(error);
      });
  };
  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();
  return (
    <>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fafafa",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "5%" }}
              >
                SL.
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Client Type
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Client
              </TableCell>
              {localStorageService.getItem("auth_user").role !==
                "SALES-PERSON" && (
                <TableCell
                  align="center"
                  className="px-0"
                  style={{ width: "10%" }}
                >
                  Sales Person
                </TableCell>
              )}

              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Call Date
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Time
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Details
              </TableCell>
              {localStorageService.getItem("auth_user") &&
                localStorageService.getItem("auth_user").role ===
                  "SALES-PERSON" && (
                  <TableCell
                    align="center"
                    className="px-0"
                    style={{ width: "10%" }}
                  >
                    Action
                  </TableCell>
                )}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "5%" }}
                    >
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "12%" }}
                    >
                      {locateValueById(
                        client_type_arrayy,
                        item.client_type_id
                      ) &&
                        locateValueById(client_type_arrayy, item.client_type_id)
                          .type}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "12%" }}
                    >
                      {item.client_info.map((e) =>
                        e.name ? (
                          <a
                            onClick={() =>
                              history.push({
                                pathname:
                                  "/individual/" +
                                  Number(
                                    item.client_info.map((e) => e.id).toString()
                                  ),
                                state: item,
                              })
                            }
                          >
                            {" "}
                            <font
                              color="blue"
                              style={{
                                cursor: "pointer",
                                fontWeight: "500",
                                textAlign: "center",
                              }}
                            >
                              {e.name}
                            </font>
                          </a>
                        ) : (
                          <a
                            onClick={() =>
                              history.push({
                                pathname:
                                  "/client/member/list/" +
                                  Number(
                                    item.client_info.map((e) => e.id).toString()
                                  ),
                                state: Number(
                                  item.client_info.map((e) => e.id).toString()
                                ),
                              })
                            }
                          >
                            {" "}
                            <font
                              color="blue"
                              style={{
                                cursor: "pointer",
                                fontWeight: "500",
                                textAlign: "center",
                              }}
                            >
                              {e.company_title + "_" + e.title}{" "}
                            </font>
                          </a>
                        )
                      )}
                    </TableCell>
                    {localStorageService.getItem("auth_user").role !==
                      "SALES-PERSON" && (
                      <TableCell className="px-0 capitalize" align="center">
                        {item.sales_person}
                      </TableCell>
                    )}

                    <TableCell className="px-0 capitalize" align="center">
                      {Moment(item.call_date).format("YYYY-MM-DD")}
                    </TableCell>

                    <TableCell className="px-0 capitalize" align="center">
                      {Moment(item.call_time, "hh:mm").format("LT")}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      {item.status && item.status === "complete" ? (
                        <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                          Completed
                        </small>
                      ) : (
                        <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                          Incomplete
                        </small>
                      )}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      <ModalTable
                        style={{ maxWidth: "700px!important" }}
                        buttonLabel="View"
                        item={item}
                      />
                    </TableCell>
                    {localStorageService.getItem("auth_user") &&
                      localStorageService.getItem("auth_user").role ===
                        "SALES-PERSON" && (
                        <TableCell className="px-0 capitalize" align="center">
                          {item.status && item.status === "complete" ? (
                            <font color="grey">Not available</font>
                          ) : (
                            Moment(item.call_date).format("YYYY-MM-DD") ===
                              Moment(new Date()).format("YYYY-MM-DD") && (
                              <IconButton
                                onClick={() => handleCompleteTask(item)}
                              >
                                <Icon color="primary">done</Icon>
                              </IconButton>
                            )
                          )}
                          {item.status && item.status !== "complete" && (
                            <Button
                              onClick={() =>
                                history.push({
                                  pathname: "/task/update/" + item.id,
                                  state: item,
                                })
                              }
                              item={item}
                              color="btn"
                              style={{ marginLeft: "-5px", marginTop: "0px" }}
                            >
                              <span
                                class="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                                aria-hidden="true"
                                title="mode_edit"
                              >
                                mode_edit
                              </span>
                            </Button>
                          )}
                        </TableCell>
                      )}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={items && items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
}
