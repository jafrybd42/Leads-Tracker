
// import React, { useState, useEffect } from 'react';
// import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
// import axios from 'axios'
// import myApi from '../../auth/api'
// import history from '../../../history'
// import Swal from 'sweetalert2'
// import localStorageService from 'app/services/localStorageService';
// import Moment from 'moment'

// // state = {

// // };


// function AddEditForm(props) {
//   // const [discussionTypes, setDiscussionTypes] = useState([])


//   const [item, setItem] = useState([])
//   const [discussionTypes, setDiscussionTypes] = useState([])
//   const [clientTypes, setClientTypes] = useState([])
//   const [vehicleTypes, setVehicleTypes] = useState([])
//   const [callTimeTypes, setCallTimeTypes] = useState([])
//   const [leads, setLeads] = useState([])
//   const [prospectTypes, setProspectTypes] = useState([])

//   const [form, setValues] = useState({
//     id: 0,
//     call_date: '',
//     prospect_type: '',
//     vehicle_type: '',
//     call_at: '',
//     leads: '',
//     discussion_type: '',
//     client_id: '',
//     call_time_type : '',
//     clients: [],
//     selectedClient: "",
//     // discussionTypes: [],
//     selectedDiscussionType: "",
//     CallTimeTypes: [],
//     selectedCallTimeType: "",
//     VehicleTypes: [],
//     selectedVehicleType: "",
//     Leads: [],

//     selectedClientType: '',
//     selectedProspectType: '',
//     selectedLead: '',
//     selectedCallTimeType: '',
//     validationError: '',
//     date: ''
//   })

//   const onChange = e => {
//     setValues({
//       ...form,
//       [e.target.name]: e.target.value
//     })
//   }

//   const submitFormAdd = e => {
//     e.preventDefault()
//     fetch('http://localhost:3000/crud', {
//       method: 'post',
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         name: form.name,
//         last: form.last,
//         email: form.email,
//         phone_number: form.phone_number,
//         location: form.location,
//         hobby: form.hobby
//       })
//     })
//       .then(response => response.json())
//       .then(item => {
//         if (Array.isArray(item)) {
//           props.addItemToState(item[0])
//           props.toggle()
//         } else {
//           //console.log('failure')
//         }
//       })
//       .catch(err => console.log(err))
//   }

//   const submitFormEdit = e => {
//     e.preventDefault()
//     //console.log(`details`, form.id, discussionTypes, form.selectedDiscussionType, form.secondary_phone_no, form.email, form.address)
//     // //console.log(this.form.state.id)
//     axios.post(myApi + '/intraco/lpg/task/update', {
//       'id': form.id, 'client_id': form.client_id,
//       'discussion_type_id': form.discussion_type, 'call_time_type_id': form.call_time_type,
//       'vehicle_type_id': form.vehicle_type, 'call_date': Moment(form.call_date).format('YYYY-MM-DD'),
//       'lead_id': form.leads, 'prospect_type': form.prospect_type_id
//     }, {

//       "headers": {
//         'x-access-token': localStorageService.getItem("auth_user").token
//       }
//     })

//       .then(response => {
//         //console.log(response.data)
//         props.toggle()
//         // form.discussion_type,form.vehicle_type,form.leads, form.call_at,form.call_date
//         props.updateState(form)
//         // props.getItem()
//         if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
//         // Swal.fire('Updated Successfully')

//       })

//       .catch(err => console.log(err))
//   }


//   useEffect(() => {
//     // getting discussionType
//     axios.get(
//       myApi + "/intraco/lpg/discussion_type/list",
//       {
//         headers: {
//           'x-access-token': localStorageService.getItem('auth_user').token
//         }
//       }
//     )
//       .then(res => {

//         let clientsFromApi = res.data.data.map(discussionType => {
//           return {
//             id: discussionType.id,
//             type: discussionType.type
//           };
//         });

//         setDiscussionTypes([
//           {
//             id: '',
//             type:
//               "Select if you want to change"
//           }
//         ].concat(
//           clientsFromApi)
//         )
//         //console.log(discussionTypes)
//       })
//       .catch(error => {
//         //console.log(error);

//       });

//     // getting clientType
//     axios.get(
//       myApi + "/intraco/lpg/client/list",
//       {
//         headers: {
//           'x-access-token': localStorageService.getItem('auth_user').token
//         }
//       }
//     )
//       .then(res => {

//         let clientsFromApi = res.data.data.map(client => {
//           return {
//             id: client.id,
//             name: client.name
//           };
//         });

//         setClientTypes([
//           {
//             id: '',
//             name:
//               "Select if you want to change"
//           }
//         ].concat(
//           clientsFromApi)
//         )
//         //console.log(clientTypes)
//       })
//       .catch(error => {
//         //console.log(error);

//       });
//     // getting vehicleType
//     axios.get(
//       myApi + "/intraco/lpg/vehicle_type/list",
//       {
//         headers: {
//           'x-access-token': localStorageService.getItem('auth_user').token
//         }
//       }
//     )
//       .then(res => {

//         let clientsFromApi = res.data.data.map(vehicleType => {
//           return {
//             id: vehicleType.id,
//             type: vehicleType.type
//           };
//         });

//         setVehicleTypes([
//           {
//             id: '',
//             type:
//               "Select if you want to change"
//           }
//         ].concat(
//           clientsFromApi)
//         )
//         //console.log(vehicleTypes)
//       })
//       .catch(error => {
//         //console.log(error);

//       });
//     // getting callType
//     axios.get(
//       myApi + "/intraco/lpg/call_time/list",
//       {
//         headers: {
//           'x-access-token': localStorageService.getItem('auth_user').token
//         }
//       }
//     )
//       .then(res => {

//         let clientsFromApi = res.data.data.map(callTime => {
//           return {
//             id: callTime.id,
//             type: callTime.type
//           };
//         });

//         setCallTimeTypes([
//           {
//             id: '',
//             type:
//               "Select if you want to change"
//           }
//         ].concat(
//           clientsFromApi)
//         )
//         //console.log(callTimeTypes)
//       })
//       .catch(error => {
//         //console.log(error);

//       });
//     // getting prospectType
//     axios.get(
//       myApi + "/intraco/lpg/prospect_type/list",
//       {
//         headers: {
//           'x-access-token': localStorageService.getItem('auth_user').token
//         }
//       }
//     )
//       .then(res => {

//         let clientsFromApi = res.data.data.map(prospectType => {
//           return {
//             id: prospectType.id,
//             type: prospectType.type
//           };
//         });

//         setProspectTypes([
//           {
//             id: '',
//             type:
//               "Select if you want to change"
//           }
//         ].concat(
//           clientsFromApi)
//         )
//         //console.log(prospectTypes)
//       })
//       .catch(error => {
//         //console.log(error);

//       });

//     // getting leads
//     axios.get(
//       myApi + "/intraco/lpg/lead/activeList",
//       {
//         headers: {
//           'x-access-token': localStorageService.getItem('auth_user').token
//         }
//       }
//     )
//       .then(res => {

//         let clientsFromApi = res.data.data.map(lead => {
//           return {
//             id: lead.id,
//             name: lead.name
//           };
//         });

//         setLeads([
//           {
//             id: '',
//             name:
//               "Select if you want to change"
//           }
//         ].concat(
//           clientsFromApi)
//         )
//         //console.log(leads)
//       })
//       .catch(error => {
//         //console.log(error);

//       });

//     if (props.item) {
//       const { id, discussion_type, vehicle_type, client_id, call_date, call_at, leads, selectedDiscussionType } = props.item
//       setValues({ id, discussion_type, vehicle_type, client_id, call_date, call_at, leads, selectedDiscussionType })
//     }
//   }, false)

//   return (
//     <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
//       <FormGroup>
//         <Label for="discussion_type">Discussion Type</Label>
//         <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
//           className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
//           name="discussion_type"
//           // value={form.selectedDiscussionType}
//           value={form.discussion_type === null ? '' : form.discussion_type}
//           onChange={onChange}
//         >
//           {discussionTypes.map(discussionType => (
//             <option
//               key={discussionType.id}
//               value={discussionType.id}
//             >
//               {discussionType.type}
//             </option>
//           ))}
//         </select>
//       </FormGroup>
//       <FormGroup>
//         <Label for="clien_id">Client</Label>
//         <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
//           className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
//           name="clien_id"
//           // value={form.selectedDiscussionType}
//           value={form.clien_id === null ? '' : form.clien_id}
//           onChange={onChange}
//         >
//           {clientTypes.map(client => (
//             <option
//               key={client.id}
//               value={client.id}
//             >
//               {client.name}
//             </option>
//           ))}
//         </select>
//       </FormGroup>
//       <FormGroup>
//         <Label for="vehicle_type">Vehicle Type</Label>
//         <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
//           className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
//           name="vehicle_type"
//           // value={form.selectedDiscussionType}
//           value={form.vehicle_type === null ? '' : form.vehicle_type}
//           onChange={onChange}
//         >
//           {vehicleTypes.map(vehicleType => (
//             <option
//               key={vehicleType.id}
//               value={vehicleType.id}
//             >
//               {vehicleType.type}
//             </option>
//           ))}
//         </select>
//       </FormGroup>
//       <FormGroup>
//         <Label for="call_at">Call Time</Label>
//         <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
//           className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
//           name="call_at"
//           // value={form.selectedDiscussionType}
//           value={form.call_at === null ? '' : form.call_at}
//           onChange={onChange}
//         >
//           {callTimeTypes.map(callTime => (
//             <option
//               key={callTime.id}
//               value={callTime.id}
//             >
//               {callTime.type}
//             </option>
//           ))}
//         </select>
//       </FormGroup>
//       <FormGroup>
//         <Label for="prospect_type">Prospect type</Label>
//         <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
//           className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
//           name="prospect_type"
//           // value={form.selectedDiscussionType}
//           value={form.prospect_type === null ? '' : form.prospect_type}
//           onChange={onChange}
//         >
//           {prospectTypes.map(prospectType => (
//             <option
//               key={prospectType.id}
//               value={prospectType.id}
//             >
//               {prospectType.type}
//             </option>
//           ))}
//         </select>
//       </FormGroup><FormGroup>
//         <Label for="leads">Lead</Label>
//         <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
//           className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
//           name="leads"
//           // value={form.selectedDiscussionType}
//           value={form.leads === null ? '' : form.leads}
//           onChange={onChange}
//         >
//           {leads.map(lead => (
//             <option
//               key={lead.id}
//               value={lead.id}
//             >
//               {lead.name}
//             </option>
//           ))}
//         </select>
//       </FormGroup>

//       <FormGroup>
//         <Label for="call_date">Date</Label>
//         <Input type="date" name="call_date" id="call_date" onChange={onChange} value={form.call_date === null ? 'Select Date' : form.call_date} />
//       </FormGroup>
//       <Button>Update</Button>
//     </Form>
//   )
// }

// export default AddEditForm
