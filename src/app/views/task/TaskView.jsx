/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';

import axios from 'axios'
import Axios from 'axios'
import myApi from '../../auth/api'

import localStorageService from 'app/services/localStorageService';
import Moment from 'moment'

import { FlexibleWidthXYPlot } from 'react-vis';
function TaskView(props) {

  const [taskList, setTaskList] = useState([])
  const [prospectTypes, setProspectTypes] = useState([])
  const [client_type_array, setClientType] = useState([]);
  const [client_type_arrayy, setClientTypeArray] = useState([]);

  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    image: '',

  })
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

  }


  useEffect(() => {
    // getting clientType
    Axios.get(myApi + "/intraco/lpg/client_type/list", {
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientType(
          [
            // {
            //   id: 0,
            //   type: "Corporate Client",
            // },
          ].concat(clientsFromApi)
        );
        setClientTypeArray(
          [
            {
              value: 0,
              label: "Corporate Client",
              id: 0,
              type: "Corporate Client",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });

    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(prospectType => {
          return {
            id: prospectType.id,
            type: prospectType.type
          };
        });

        setProspectTypes([
          {
            id: '',
            type:
              "Select if you want to change"
          }
        ].concat(
          clientsFromApi)
        )
      })
      .catch(error => {
        console.log(error);

      });

    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, image } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, image })


      axios.get(myApi + '/intraco/lpg/task/details/' + props.item.id, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {


        if (response.data.data) {
          setTaskList(response.data.data)
        }
      })

    }
  }, false)

  const locateValueById = (types, id) => {
    let item = types.find(it => it.id === Number(id))
    return item
  }

  return (
    <div>
      <div className="modal-dialog">
        <div className="modal-content" style={{marginTop : '-35px'}}>

          <div className="modal-body">
            <div style={{ display: FlexibleWidthXYPlot }}>
              <div>
                <span><strong>Date : </strong></span>
                <span className="label label-warning">{Moment(props.item.call_date).format('YYYY-MM-DD')}</span>

              </div>

              <div style={{
                textAlign: "right",
                marginTop: "-20px"
              }}>
                <span><strong>Call Time : </strong></span>
                <span className="label label-warning">{Moment(props.item.call_time, 'hh:mm').format('LT')}</span>

              </div>
            </div>


            <hr />
            <center>
              <span><strong>Client Type: </strong></span>
              <span className="label label-warning"> {locateValueById(client_type_arrayy, props.item.client_type_id) &&
                locateValueById(client_type_arrayy, props.item.client_type_id)
                  .type}</span>

            </center>
            <hr />
            <center>
              <span><strong>Client : </strong></span>
              <span className="label label-warning">{props.item.client_info.map((e) => e.name || e.company_title + '_' + e.title)}</span>

            </center>
            <hr />
            <center>
              <span><strong>Discussion Type : </strong></span>
              <span className="label label-warning">{props.item.discussion_type.map(function (item, index) {
                return (
                  <span key={`demo_snap_${index}`}>
                    {(index ? ", " : "") + item.type}
                  </span>
                );
              })}</span>

            </center>
            <hr />
            <center>
              <span><strong>Vehicle Type : </strong></span>
              <span className="label label-warning">{props.item.vehicle_type.map(function (item, index) {
                return (
                  <span key={`demo_snap_${index}`}>
                    {(index ? ", " : "") + item.type}
                  </span>
                );
              })}</span>

            </center>
            <hr />
            <center>
              <span><strong>prospect Type : </strong></span>
              <span className="label label-warning">{props.item.prospect_type_id === 0
                ? "Default"
                : locateValueById(prospectTypes, props.item.prospect_type_id)
                  ? locateValueById(prospectTypes, props.item.prospect_type_id).type
                  : props.item.prospect_type}</span>

            </center>
            <hr />
            <center>
              <span><strong>Lead : </strong></span>
              <span className="label label-warning">{props.item.leads}</span>

            </center>
            <hr />
            <center>
              <span><strong>Status : </strong></span>
              <span className="label label-warning"> {props.item.status && props.item.status == "complete" ? (
                <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                  Completed
                </small>
              ) : (
                  <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                    Incomplete
                  </small>
                )}</span>

            </center>
          </div>

        </div>
      </div>
    </div>
  )
}

export default TaskView
