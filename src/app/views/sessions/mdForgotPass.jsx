/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { Card, Grid, Button } from "@material-ui/core";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import localStorageService from 'app/services/localStorageService'
import Swal from 'sweetalert2'
import { resetPassword } from "../../redux/actions/LoginActions";
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'

class MDForgotPassword extends Component {
  constructor() {
    super();
    this.state = {
      phone: "",
      questions: [],
      selectedQuestion: '',
      primary_phone_no: "",
      email: "",
      address: "",
      secondary_phone_no: "",
      quesAns: '',
      newPass: ''
    };
  }

  componentDidMount() {

    // getting questions
    axios.get(
      myApi + "/intraco/lpg/question/list"

    )
      .then(res => {
        //console.log(res.data.data)
        let questionsFromApi = res.data.data.map(question => {
          return {
            id: question.id,
            question_details: question.question_details
          };
        });

        this.setState({
          questions: [
            {
              id: 0,
              question_details:
                "Select question"
            }
          ]
            .concat(questionsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

  }
  handleChange = event => {
    event.persist();
    this.setState({
      [event.target.name]: event.target.value
    });
  };
  handleFormSubmit = () => {
    // this.props.resetPassword({ ...this.state });
    axios.post(myApi + '/intraco/lpg/salesPerson/profile/forgetPassword', {
      "phone": this.state.phone,
      "password_recovery_questions_id": parseInt(this.state.selectedQuestion),
      "question_ans": this.state.quesAns,
      "new_password": this.state.newPass
    })
      .then(res => {
        //console.log(res.data)
        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

          if (res.data.error === false) {
            this.setState({
              phone: "",
              // questions: [],
              selectedQuestion: '',
              primary_phone_no: "",
              email: "",
              address: "",
              secondary_phone_no: "",
              quesAns: '',
              newPass: ''
            })
          }
        }
      })
  };
  render() {
    // let { phone, selectedQuestion, quesAns } = this.state;
    let {
      phone,
      // primary_phone_no,
      // secondary_phone_no,
      // email,
      // address,
      // question_details,
      // selectedQuestion,
      quesAns,
      newPass
    } = this.state;
    return (
      <div className="signup flex justify-center w-full h-full-screen">
        <div className="p-8">
          <Card className="signup-card position-relative y-center">
            <Grid container>
              <Grid item lg={5} md={5} sm={5} xs={12}>
                <div className="p-8 flex justify-center items-center h-full">
                  <img src="/assets/lpg.svg" alt="" style={{ width: '140px' }} />
                </div>
              </Grid>
              <Grid item lg={7} md={7} sm={7} xs={12}>
                <div className="p-9 h-full bg-light-gray position-relative">
                  <ValidatorForm ref="form" onSubmit={this.handleFormSubmit}>
                    <TextValidator
                autoComplete='off'
                      className="mb-6 w-full"
                      variant="outlined"
                      label="Phone Number"
                      onChange={this.handleChange}
                      type="phone"
                      name="phone"
                      value={phone}
                      validators={["required"]}
                      errorMessages={[
                        "this field is required",
                        "Phone number is not valid"
                      ]}
                    />
                    <select style={{ width: '100%', padding: '5px', backgroundColor: '#ebebeb', color: '#444', borderColor: '#b5b5b5', borderBottomColor: '#b5b5b5', textAlign: 'left' }}
                      className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                      name="selectedQuestion"
                      value={this.state.selectedQuestion ? this.state.selectedQuestion : 0}
                      onChange={this.handleChange}
                    // {e =>
                    //   this.setState({
                    //     selectedQuestion:  this.state.selectedQuestion ? parseInt(this.state.selectedQuestion) : (e.target.value),
                    //     validationError:
                    //       e.target.value === ""
                    //         ? "You must select Question"
                    //         : 'select'
                    //   })
                    // }
                    >
                      {this.state.questions.map(question => (
                        <option
                          key={question.id}
                          value={question.id}
                        >
                          {question.question_details}
                        </option>
                      ))}
                    </select>
                    <TextValidator
                autoComplete='off'
                      className="mb-4 w-full"
                      label="Your Answer"
                      onChange={this.handleChange}
                      type="text"
                      name="quesAns"
                      value={quesAns}
                      // validators={["required", "isemail"]}
                      errorMessages={["this field is required"]}
                    />
                    <TextValidator
                autoComplete='off'
                      className="mb-4 w-full"
                      label="Give a New Password"
                      onChange={this.handleChange}
                      type="text"
                      name="newPass"
                      value={newPass}
                      // validators={["required", "isemail"]}
                      errorMessages={["this field is required"]}
                    />
                    <div className="flex items-center">
                      <Button variant="contained" color="primary" type="submit">
                        Reset Password
                      </Button>
                      <span className="ml-4 mr-2">or</span>
                      <Button
                        className="capitalize"
                        onClick={() =>
                          this.props.history.push("/signin")
                        }
                      >
                        Sign in
                      </Button>
                    </div>
                  </ValidatorForm>
                </div>
              </Grid>
            </Grid>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  resetPassword: PropTypes.func.isRequired,
  login: state.login
});
export default withRouter(
  connect(mapStateToProps, { resetPassword })(MDForgotPassword)
);
