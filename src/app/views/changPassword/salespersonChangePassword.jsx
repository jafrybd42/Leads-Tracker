/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'

import {
  Button,
  Icon,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import themeOptions from "app/MatxLayout/MatxTheme/themeOptions";

class SalespersonChangePassword extends Component {

  constructor() {
    super();
    this.state = {
      oldPass: "",
      firstName: "",
      email: "",
      date: new Date(),
      creditCard: "",
      phone: "",
      password: "",
      confirmPassword: "",
      gender: "",
      agreement: ""
    };
  }
  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    let { user } = this.props
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    axios.post(myApi + '/intraco/lpg/salesPerson/profile/passwordChange',
      { 'id': JSON.parse(localStorage.getItem("auth_user")).userId, 'old_password': this.state.oldPass, 'new_password': this.state.password },
      {
        headers: {

          'x-access-token': localStorage.getItem("jwt_token")
        },
      }
    )
      .then(res => {
        if (res.data.error === false) {
          this.setState({
            oldPass: "",
            firstName: "",
            email: "",
            date: new Date(),
            creditCard: "",
            phone: "",
            password: "",
            confirmPassword: "",
            gender: "",
            agreement: ""
          })
        }
        if (res.data.error) {
         Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
          // throw new Error(res.data.message)
        }
        else {
         Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
        }
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    this.setState({ date });
  };

  render() {
    let { user } = this.props
    let {
      oldPass,
      firstName,
      creditCard,
      phone,
      password,
      confirmPassword,
      gender,
      date,
      email
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Current Password"
                onChange={this.handleChange}
                type="password"
                name="oldPass"
                value={oldPass}
              />


              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="New Password (Min Length : 6)"
                onChange={this.handleChange}
                name="password"
                type="password"
                value={password}

                validators={["required", "isPasswordMatch"]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Confirm Password"
                onChange={this.handleChange}
                name="confirmPassword"
                type="password"
                value={confirmPassword}

                validators={["required", "isPasswordMatch"]}
                errorMessages={[
                  "this field is required",
                  "password didn't match"
                ]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default SalespersonChangePassword;
