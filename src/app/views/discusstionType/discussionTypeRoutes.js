/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const DiscussionTypeRoutes = [
  {
    path: "/discussionType/create",
    component: React.lazy(() => import("./createDiscussionType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/discussionType/manage",
    component: React.lazy(() => import("./manageDiscussionType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/discussionType/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/car_service/edit/:id",
    component: React.lazy(() => import("./car_service/createClient")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }
];

export default DiscussionTypeRoutes;
