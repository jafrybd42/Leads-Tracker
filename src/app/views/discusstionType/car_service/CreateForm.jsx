/* eslint-disable no-dupe-keys */
/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../../services/localStorageService"
import axios from 'axios'
import myApi from '../../../auth/api'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import Select from 'react-select';

import "date-fns";


class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      id: 0,
      name: '',
      type: '',
      active_status: "",
      service_time_period: "",
      reminder_time_period: "",

      primary_phone_no: "",
      email: "",
      rank: "",
      secondary_phone_no: "",
      clients: [],
      selectedClient: "",
      selectedServiceYear: '',
      selectedServiceDays: '',
      selectedServiceMonth: '',
      selectedReminderYear: '',
      selectedReminderMonth: '',
      selectedReminderDays: '',
      years: [
        { value: 0, label: "Select Year" },
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
        { value: 4, label: "4" },
        { value: 5, label: "5" },
        { value: 6, label: "6" },
        { value: 7, label: "7" },
      ],
      months: [
        { value: 0, label: "Select Month" },
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
        { value: 4, label: "4" },
        { value: 5, label: "5" },
        { value: 6, label: "6" },
        { value: 7, label: "7" },
        { value: 8, label: "8" },
        { value: 9, label: "9" },
        { value: 10, label: "10" },
        { value: 11, label: "11" },
      ],
      days: [
        { value: 0, label: "Select Day" },
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
        { value: 4, label: "4" },
        { value: 5, label: "5" },
        { value: 6, label: "6" },
        { value: 7, label: "7" },
        { value: 8, label: "8" },
        { value: 9, label: "9" },
        { value: 10, label: "10" },
        { value: 11, label: "11" },
        { value: 12, label: "12" },
        { value: 13, label: "13" },
        { value: 14, label: "14" },
        { value: 15, label: "15" },
        { value: 16, label: "16" },
        { value: 17, label: "17" },
        { value: 18, label: "18" },
        { value: 19, label: "19" },
        { value: 20, label: "20" },
        { value: 21, label: "21" },
        { value: 22, label: "22" },
        { value: 23, label: "23" },
        { value: 24, label: "24" },
        { value: 25, label: "25" },
        { value: 26, label: "26" },
        { value: 27, label: "27" },
        { value: 28, label: "28" },
        { value: 29, label: "29" },
        { value: 30, label: "30" },
      ]
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

    let yearr, monthh, dayy

    ([yearr, monthh, dayy] =
      this.props.location.state.service_time_period.split("_"))
    this.setState({
      selectedServiceYear: yearr,
      selectedServiceMonth: monthh,
      selectedServiceDays: dayy,
    })

    let rD,rM,rY
    ([rY, rM, rD] =
      this.props.location.state.reminder_time_period.split("_"))
    this.setState({
      selectedReminderYear: rY,
      selectedReminderMonth: rM,
      selectedReminderDays: rD,
    })

    this.props.location.state.type &&
    this.setState({
      type : this.props.location.state.type
    })
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {

    axios.post(myApi + '/intraco/lpg/discussion_type/update', {
      "id": this.props.location.state.id,
      "type": this.state.type,
      "service_year": Number(this.state.selectedServiceYear),
      "service_month": Number(this.state.selectedServiceMonth),
      "service_day": Number(this.state.selectedServiceDays),
      "reminder_year": Number(this.state.selectedReminderYear),
      "reminder_month":Number(this.state.selectedReminderMonth),
      "reminder_day": Number(this.state.selectedReminderDays)
  }, {

      "headers": {
        "x-access-token": localStorage.getItem("jwt_token")
      }
    })

      .then(res => {
        //console.log(res.data.error)

        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

          if (res.data.error === false) {
            this.setState({
              name: this.state.type,
              selectedServiceYear:  this.state.selectedServiceYear,
              selectedServiceDays: this.state.selectedServiceDays,
              selectedServiceMonth: this.state.selectedServiceMonth,
              selectedReminderYear: this.state.selectedReminderYear,
              selectedReminderMonth: this.state.selectedReminderMonth,
              selectedReminderDays: this.state.selectedReminderDays,
            })
          }
        }
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {

    this.setState({ date });
  };



  render() {
    let {
      name,
    } = this.state;

    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={12} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Discussion Type (Max length : 25)"
                onChange={this.handleChange}
                type="text"
                name="type"
                value={this.state.type || this.props.location.state.type}
                validators={[
                  "required",
                  "maxStringLength: 25"
                ]}
                errorMessages={["this field is required"]}
              />

            </Grid>
          </Grid>
          <div style={{ fontWeight: '500', fontSize: '15px', marginTop: '40px', marginBottom: '15px' }}>
            <font color="black">
              Service Period
            </font>
          </div>
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '25%' }}>
              <font color='grey'>Day</font>

              <select
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedServiceDays"
                value={this.state.selectedServiceDays || this.props.location.state.service_time_period}
                onChange={this.handleChange}
              >
                {this.state.days.map((client) => (
                  <option key={client.value} value={client.value}>
                    {client.label}
                  </option>
                ))}
              </select>

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '35%' }}>
              <font color='grey'>Month</font>

              <select
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedServiceMonth"
                value={this.state.selectedServiceMonth || this.props.location.state.service_time_period}
                onChange={this.handleChange}
              >
                {this.state.months.map((client) => (
                  <option key={client.value} value={client.value}>
                    {client.label}
                  </option>
                ))}
              </select>
            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '40%' }}>
              <font color='grey'>Year</font>

              <select
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedServiceYear"
                value={this.state.selectedServiceYear || this.props.location.state.service_time_period}
                onChange={this.handleChange}
              >
                {this.state.years.map((client) => (
                  <option key={client.value} value={client.value}>
                    {client.label}
                  </option>
                ))}
              </select>

            </Grid>
          </Grid>

          <div style={{ fontWeight: '500', fontSize: '15px', marginTop: '40px', marginBottom: '15px' }}>
            <font color="black">
              Reminder Period
            </font>
          </div>

          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '25%' }}>
              <font color='grey'>Day</font>

              <select
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedReminderDays"
                value={this.state.selectedReminderDays || this.props.location.state.service_time_period}
                onChange={this.handleChange}
              >
                {this.state.days.map((client) => (
                  <option key={client.value} value={client.value}>
                    {client.label}
                  </option>
                ))}
              </select>

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '35%' }}>
              <font color='grey'>Month</font>

              <select
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedReminderMonth"
                value={this.state.selectedReminderMonth || this.props.location.state.service_time_period}
                onChange={this.handleChange}
              >
                {this.state.months.map((client) => (
                  <option key={client.value} value={client.value}>
                    {client.label}
                  </option>
                ))}
              </select>
            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '40%' }}>
              <font color='grey'>Year</font>

              <select
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedReminderYear"
                value={this.state.selectedReminderYear || this.props.location.state.service_time_period}
                onChange={this.handleChange}
              >
                {this.state.years.map((client) => (
                  <option key={client.value} value={client.value}>
                    {client.label}
                  </option>
                ))}
              </select>

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit" style={{ marginTop: '30px' }}>
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>

    );
  }

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

}



export default withRouter(CreateForm);
