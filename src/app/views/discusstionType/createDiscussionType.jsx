import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import CreateForm from "./CreateForm";

class CreateDiscussionType extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Discussion Type", path: "/discussionType/create" },
              { name: "Create Discussion Type" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><CreateForm /></Card>
      </div>
    );
  }
}

export default CreateDiscussionType;
