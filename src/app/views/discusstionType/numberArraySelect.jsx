import React, { Fragment } from "react";
import { TextField } from "@material-ui/core";
import { Autocomplete, createFilterOptions } from "@material-ui/lab";

const suggestions = [
  { label: "1" },
  { label: "2" },
  { label: "3" },
  { label: "4" },
  { label: "5" },
  { label: "6" },
  { label: "7" },
  { label: "8" },
  { label: "9" },
  { label: "10" },
  { label: "11" },
  { label: "12" },
  { label: "13" },
  { label: "14" },
  { label: "15" },
  { label: "16" },
  { label: "17" },
  { label: "18" },
  { label: "19" },
  { label: "20" },
  { label: "21" },
  { label: "22" },
  { label: "23" },
  { label: "24" },
  { label: "25" },
  { label: "26" },
  { label: "27" },
  { label: "28" },
  { label: "29" },
  { label: "30" },
];

const filter = createFilterOptions();

const NumberArraySelect = () => {
  const [value, setValue] = React.useState(null);

  const handleChange = (event, newValue) => {
    if (newValue && newValue.inputValue) {
      setValue({
        label: newValue.inputValue
      });
      return;
    }
    setValue(newValue);
  };

  const filterOptions = (options, params) => {
    const filtered = filter(options, params);
    if (params.inputValue !== "") {
      filtered.push({
        inputValue: params.inputValue,
        label: `Add "${params.inputValue}"`
      });
    }
    return filtered;
  };

  return (
    <Fragment>
      <Autocomplete
        className="mb-4 w-300"
        options={suggestions}
        getOptionLabel={option => option.label}
        renderInput={params => (
          <TextField
            {...params}
            label="Select Year"
            variant="outlined"
            fullWidth
          />
        )}
      />

      {/* <Autocomplete
        className="mb-4 w-300"
        value={value}
        onChange={handleChange}
        filterOptions={filterOptions}
        options={suggestions}
        getOptionLabel={option => {
          // e.g value selected with enter, right from the input
          if (typeof option === "string") {
            return option;
          }
          if (option.inputValue) {
            return option.inputValue;
          }
          return option.label;
        }}
        renderOption={option => option.label}
        style={{ width: 300 }}
        freeSolo
        renderInput={params => (
          <TextField
            {...params}
            label="Free solo with text demo"
            variant="outlined"
            fullWidth
          />
        )}
      />

      <Autocomplete
        className="mb-4 w-300"
        options={suggestions}
        getOptionLabel={option => option.label}
        getOptionDisabled={option =>
          option === suggestions[0] || option === suggestions[2]
        }
        renderInput={params => (
          <TextField
            {...params}
            label="Disabled option"
            variant="outlined"
            fullWidth
          />
        )}
      /> */}
    </Fragment>
  );
};

export default NumberArraySelect;
