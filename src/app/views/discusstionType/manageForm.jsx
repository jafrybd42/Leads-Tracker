import React from "react";
import {
  Card,
  Icon,
  IconButton,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import axios from 'axios'
const ManageForm = () => {
  let [discussionTypeList, setDiscussionTypeList] = React.useState('');
  // let {user} = this.props


  const fetchData = React.useCallback(() => {
    axios({
      "method": "GET",
      "url": "http://127.0.0.1:3001/intraco/lpg/discussion_type/list",
      "headers": {

        "x-access-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwaG9uZV9udW1iZXIiOiIwMTc3NzY4Nzk3MiIsInVzZXJfaWQiOjE0LCJpZCI6MTQsIm5hbWUiOiJqYWZyeSIsInVzZXJfdHlwZSI6MiwiaWF0IjoxNjAwOTI3OTc4LCJleHAiOjE2MDEwMTQzNzh9.5P4iPxAKd0kx4Il1nZNw0HoM0mi8sOqbj-q5lLM4U84"
      }
    })
      .then((response) => {
        // //console.log(token)
        //console.log(response.data.data)
        setDiscussionTypeList(response.data.data)
      })
      .catch((error) => {
        //console.log(error)
      })
  }, [])
  React.useEffect(() => {
    fetchData()
  }, [fetchData])



return (

  <Card elevation={3} className="pt-5 mb-6">
    <div className="card-title px-6 mb-3">Discussion Type List</div>
    <div className="overflow-auto">


      <Table className="product-table">
        <TableHead>
          <TableRow>
            <TableCell className="px-6" colSpan={1}>
              ID
              </TableCell>
            <TableCell className="px-0" colSpan={3}>
              Name
              </TableCell>
            {/* <TableCell className="px-0" colSpan={2}>
                Email
              </TableCell> */}
            <TableCell className="px-0" colSpan={1}>
              Action
              </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {/* {ManageForm.map((product, index) => ( */}
          {discussionTypeList.length === 0
            ? 'Loading users...'
            : discussionTypeList.map(user => (
              <TableRow key={user.id}>
                <TableCell className="px-0 capitalize" colSpan={1} align="left">
                  <div className="flex items-center">
                    {/* <p className="m-0 ml-8"></p> */}
                    {'#' + user.id}
                  </div>
                </TableCell>
                <TableCell className="px-0 capitalize" align="left" colSpan={3}>


                  {user.type}

                </TableCell>

                {/* <TableCell className="px-0 capitalize" align="left" colSpan={2}>

                  {user.email}
                </TableCell> */}
                <TableCell className="px-0" colSpan={1}>
                {/* onClick={() => updateClientType(user.id)} */}
                  <IconButton>
                    <Icon color="primary">edit</Icon>
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>

    </div>
  </Card>
);
};
export default ManageForm;
