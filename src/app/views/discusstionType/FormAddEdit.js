/* eslint-disable no-dupe-keys */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import history from "../../../history";
import Swal from "sweetalert2";
function AddEditForm(props) {
  // eslint-disable-next-line no-unused-vars
  const [item, setItem] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    name: "",
    type: "",
    active_status: "",
    service_time_period: "",
    reminder_time_period: "",

    serviceYear: "",
    serviceMonth: "",
    serviceDay: "",
    reminderYear: "",
    remindeMonth: "",
    reminderDay: "",

    years: [
      { value: 0, label: "Select Year" },
      { value: 1, label: "1" },
      { value: 2, label: "2" },
      { value: 3, label: "3" },
      { value: 4, label: "4" },
      { value: 5, label: "5" },
      { value: 6, label: "6" },
      { value: 7, label: "7" },
    ],
    months: [
      { value: 0, label: "Select Month" },
      { value: 1, label: "1" },
      { value: 2, label: "2" },
      { value: 3, label: "3" },
      { value: 4, label: "4" },
      { value: 5, label: "5" },
      { value: 6, label: "6" },
      { value: 7, label: "7" },
      { value: 8, label: "8" },
      { value: 9, label: "9" },
      { value: 10, label: "10" },
      { value: 11, label: "11" },
    ],
    days: [
      { value: 0, label: "Select Day" },
      { value: 1, label: "1" },
      { value: 2, label: "2" },
      { value: 3, label: "3" },
      { value: 4, label: "4" },
      { value: 5, label: "5" },
      { value: 6, label: "6" },
      { value: 7, label: "7" },
      { value: 8, label: "8" },
      { value: 9, label: "9" },
      { value: 10, label: "10" },
      { value: 11, label: "11" },
      { value: 12, label: "12" },
      { value: 13, label: "13" },
      { value: 14, label: "14" },
      { value: 15, label: "15" },
      { value: 16, label: "16" },
      { value: 17, label: "17" },
      { value: 18, label: "18" },
      { value: 19, label: "19" },
      { value: 20, label: "20" },
      { value: 21, label: "21" },
      { value: 22, label: "22" },
      { value: 23, label: "23" },
      { value: 24, label: "24" },
      { value: 25, label: "25" },
      { value: 26, label: "26" },
      { value: 27, label: "27" },
      { value: 28, label: "28" },
      { value: 29, label: "29" },
      { value: 30, label: "30" },
    ],

    active_status: "",
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });

    console.log(form.serviceDay);
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    axios
      .post(
        myApi + "/intraco/lpg/discussion_type/update",
        {
          id: form.id,
          type: form.type,
          service_year: form.serviceYear,
          service_month: form.serviceMonth,
          service_day: form.serviceDay,
          reminder_year: form.reminderYear,
          reminder_month: form.remindeMonth,
          reminder_day: form.reminderDay,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )

      .then((response) => {
        Swal.fire({
          icon: "success",
          title: response.data.message,
          showConfirmButton: false,
          timer: 1500,
        });
        if (response.data.error === false) {
          props.updateState(form);
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (props.item) {
      const {
        id,
        type,
        service_time_period,
        reminder_time_period,
        active_status,
        serviceDay,
        serviceMonth,
        serviceYear,
        reminderDay,
        remindeMonth,
        reminderYear,
      } = props.item;
      setValues({
        id,
        type,
        service_time_period,
        reminder_time_period,
        active_status,
        serviceDay,
        serviceMonth,
        serviceYear,
        reminderDay,
        remindeMonth,
        reminderYear,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false);

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Type</Label>
        <Input
          type="text"
          name="type"
          id="type"
          onChange={onChange}
          value={form.type === null ? "" : form.type}
          minLength="3"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="serviceDay">Day</Label>
        {/* <Input
          type="text"
          name="serviceDay"
          id="serviceDay"
          onChange={onChange}
          value={
            ([
              form.serviceYear,
              form.serviceMonth,
              form.serviceDay,
            ] = props.item.service_time_period.split("_")) &&
            form.serviceDay === null
              ? ""
              : form.serviceDay
          }
        /> */}
        {/* {([
          form.serviceYear,
          form.serviceMonth,
          form.serviceDay,
        ] = props.item.service_time_period.split("_")) &&
        form.serviceDay === null
          ? ""
          : form.serviceDay} */}
        {/* <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="client_id"
          value={([
            form.serviceYear,
            form.serviceMonth,
            form.serviceDay,
          ] = props.item.service_time_period.split("_")) && form.serviceDay || props.item.service_time_period}
          onChange={onChange}
        >
          {form.days.map((client) => (
            <option key={client.value} value={client.value}>
              {client.label}
            </option>
          ))}
        </select> */}
        <Label for="serviceMonth">Month</Label>
        <Input
          type="text"
          name="serviceMonth"
          id="serviceMonth"
          onChange={onChange}
          // value={
          //   ([
          //     serviceYear,
          //     serviceMonth,
          //     serviceDay,
          //   ] = props.item.service_time_period.split("_")) &&
          //   serviceMonth === null
          //     ? ""
          //     : serviceMonth
          // }
        />
        <Label for="serviceYear">Year</Label>
        <Input
          type="text"
          name="serviceYear"
          id="serviceYear"
          onChange={onChange}
          // value={
          //   ([
          //     serviceYear,
          //     serviceMonth,
          //     serviceDay,
          //   ] = props.item.service_time_period.split("_")) &&
          //   serviceYear === null
          //     ? ""
          //     : serviceYear
          // }
        />
      </FormGroup>
      <FormGroup>
        <Label for="reminderDay">Day</Label>
        <Input
          type="text"
          name="reminderDay"
          id="reminderDay"
          onChange={onChange}
          // value={
          //   ([
          //     reminderYear,
          //     remindeMonth,
          //     reminderDay,
          //   ] = props.item.reminder_time_period.split("_")) &&
          //   reminderDay === null
          //     ? ""
          //     : reminderDay
          // }
        />

        <Label for="remindeMonth">Month</Label>
        <Input
          type="text"
          name="remindeMonth"
          id="remindeMonth"
          onChange={onChange}
          // value={
          //   ([
          //     reminderYear,
          //     remindeMonth,
          //     reminderDay,
          //   ] = props.item.reminder_time_period.split("_")) &&
          //   remindeMonth === null
          //     ? ""
          //     : remindeMonth
          // }
        />

        <Label for="reminderYear">Year</Label>
        <Input
          type="text"
          name="reminderYear"
          id="reminderYear"
          onChange={onChange}
          // value={
          //   ([
          //     reminderYear,
          //     remindeMonth,
          //     reminderDay,
          //   ] = props.item.reminder_time_period.split("_")) &&
          //   reminderYear === null
          //     ? ""
          //     : reminderYear
          // }
        />
      </FormGroup>
      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
