/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import history from "../../../history";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

export default function DataTable(props) {
  const [form, setValues] = useState({
    name: "",
    type: "",
    active_status: "",
    service_time_period: "",
    reminder_time_period: "",
  });
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  var [yearr, monthh, dayy] = useState([]);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const deleteItem = (
    id,
    type,
    service_time_period,
    reminder_time_period,
    active_status
  ) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, deactive it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/discussion_type/delete",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Activated",
              showConfirmButton: false,
              timer: 1000,
            });
          })
          .then((item) => {
            props.updateState({
              id,
              type,
              service_time_period,
              reminder_time_period,
              active_status: 1,
            });
          })
          .catch((err) => console.log(err));
      }
    });
  };
  const activeItem = (
    id,
    type,
    service_time_period,
    reminder_time_period,
    active_status
  ) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1a71b5",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, active it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/discussion_type/active",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Activated",
              showConfirmButton: false,
              timer: 1000,
            });
          })
          .then((item) => {
            props.updateState({
              id,
              type,
              service_time_period,
              reminder_time_period,
              active_status: 0,
            });
          });
      }
    });
  };
  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  return (
    <>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fafafa",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "5%" }}
              >
                SL.
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Type
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Service Period
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Reminder Period
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "12.5%" }}
              >
                Edit
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "12.5%" }}
              >
                Delete
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "5%" }}
                    >
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {item.type}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {([yearr, monthh, dayy] =
                        item.service_time_period !== ("0_0_0" || "undefined")
                          ? item.service_time_period.split("_")
                          : "No data") &&
                      yearr === "0" &&
                      monthh === "0" &&
                      dayy !== 0 ? (
                        dayy + " day"
                      ) : yearr === "0" && monthh !== "0" && dayy === 0 ? (
                        monthh + " month"
                      ) : yearr === "0" && monthh !== "0" && dayy !== 0 ? (
                        monthh + " month " + dayy + " day"
                      ) : yearr !== "0" && monthh === "0" && dayy === 0 ? (
                        yearr + " year"
                      ) : yearr !== "0" && monthh === "0" && dayy !== 0 ? (
                        yearr + " year " + dayy + " day"
                      ) : yearr !== "0" && monthh !== "0" && dayy === 0 ? (
                        yearr + " year " + monthh + " month "
                      ) : yearr !== "0" && monthh !== "0" && dayy !== 0 ? (
                        yearr + " year " + monthh + " month " + dayy + " day"
                      ) : (
                        <font color="green">not available</font>
                      )}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "20%" }}
                    >
                      {([yearr, monthh, dayy] =
                        item.reminder_time_period !== ("0_0_0" || "undefined")
                          ? item.reminder_time_period.split("_")
                          : "No data") &&
                      yearr === "0" &&
                      monthh === "0" &&
                      dayy !== 0 ? (
                        dayy + " day"
                      ) : yearr === "0" && monthh !== "0" && dayy === 0 ? (
                        monthh + " month"
                      ) : yearr === "0" && monthh !== "0" && dayy !== 0 ? (
                        monthh + " month " + dayy + " day"
                      ) : yearr !== "0" && monthh === "0" && dayy === 0 ? (
                        yearr + " year"
                      ) : yearr !== "0" && monthh === "0" && dayy !== 0 ? (
                        yearr + " year " + dayy + " day"
                      ) : yearr !== "0" && monthh !== "0" && dayy === 0 ? (
                        yearr + " year " + monthh + " month "
                      ) : yearr !== "0" && monthh !== "0" && dayy !== 0 ? (
                        yearr + " year " + monthh + " month " + dayy + " day"
                      ) : (
                        <font color="green">not available</font>
                      )}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "10%" }}
                    >
                      {item.active_status === 0 ? (
                        <font color="green">Activated</font>
                      ) : (
                        <font color="red">Deactivated</font>
                      )}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "12.5%", margin: "auto" }}
                    >
                      {item.active_status === 0 ? (
                        <Button
                          onClick={() =>
                            history.push({
                              pathname: "/car_service/edit/" + item.id,
                              state: {
                                id: item.id,
                                type: item.type,
                                active_status: item.active_status,
                                reminder_time_period: item.reminder_time_period,
                                service_time_period: item.service_time_period,
                              },
                            })
                          }
                          item={item}
                          color="btn btn-warning"
                          style={{
                            margin: "auto",
                            // marginLeft: "-10px",
                            marginTop: "7px",
                            // marginRight: "5px",
                            height: "37px",
                          }}
                        >
                          <span
                            class="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                            aria-hidden="true"
                            title="mode_edit"
                          >
                            mode_edit
                          </span>
                        </Button>
                      ) : (
                        <Button
                          disabled
                          onClick={() =>
                            history.push({
                              pathname: "/car_service/edit/" + item.id,
                              state: {
                                id: item.id,
                                type: item.type,
                                active_status: item.active_status,
                                reminder_time_period: item.reminder_time_period,
                                service_time_period: item.service_time_period,
                              },
                            })
                          }
                          item={item}
                          color="btn btn-warning"
                          style={{
                            margin: "auto",
                            // marginLeft: "-10px",
                            marginTop: "7px",
                            // marginRight: "5px",
                            height: "37px",
                          }}
                        >
                          <span
                            class="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                            aria-hidden="true"
                            title="mode_edit"
                          >
                            mode_edit
                          </span>
                        </Button>
                      )}
                    </TableCell>
                    {item.active_status === 0 ? (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "12.5%" }}
                      >
                        <Button
                          color="danger"
                          style={{
                            marginLeft: "-10px",
                            backgroundColor: "#f41b35",
                            borderColor: "#f41b35",
                            width: "100px",
                          }}
                          onClick={() =>
                            deleteItem(
                              item.id,
                              item.type,
                              item.service_time_period,
                              item.reminder_time_period,
                              item.active_status
                            )
                          }
                        >
                          Deactive
                        </Button>
                      </TableCell>
                    ) : (
                      <TableCell
                        className="px-0 capitalize"
                        align="center"
                        style={{ width: "12.5%" }}
                      >
                        <Button
                          name="active_status"
                          color="success"
                          style={{
                            marginLeft: "-10px",
                            backgroundColor: "#00aa33",
                            borderColor: "#00aa33",
                            width: "100px",
                          }}
                          onClick={() =>
                            activeItem(
                              item.id,
                              item.type,
                              item.service_time_period,
                              item.reminder_time_period,
                              item.active_status
                            )
                          }
                        >
                          Active
                        </Button>
                      </TableCell>
                    )}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
}
