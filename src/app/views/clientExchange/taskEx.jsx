import React from 'react';
import { PickList } from '@bit/primefaces.primereact.picklist';
import PrimereactStyle from '@bit/primefaces.primereact.internal.stylelinks';
import myApi from 'app/auth/api';
import localStorageService from 'app/services/localStorageService';
import Axios from 'axios';
import { ValidatorForm } from "react-material-ui-form-validator";
import {

	Button,
	Icon,
	Grid,
} from "@material-ui/core";
import moment from 'moment'
import Swal from 'sweetalert2';
class TaskEX extends React.Component {
	constructor() {
		super();
		this.state = ({
			clientType: [],
			source: [],
			target: [],
			selectedClientType: 1,
			salesPersonsOne: [],
			salesPersonsTwo: [],
			selectedSalesPersonOne: '',
			selectedSalesPersonTwo: '',
			isSubmitted: false

		});
		this.taskservice = this.state.salesPersonsOne;
		this.taskAssign = this.taskAssign.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	componentDidMount() {

		this.setState({
			source: []
		});
		// console.log(this.state.selectedSalesPersonOne)
		// getting salesPersonInformation
		Axios.get(
			myApi + "/intraco/lpg/salesPerson/allList",
			{
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token
				}
			}
		)
			.then(res => {
				let salesPersonsFromApi = res.data.data.map(salesPerson => {
					return {
						id: salesPerson.id,
						name: salesPerson.name,
						status: salesPerson.active_status === 0 ? 'Active' : 'Deactive'
					};
				});

				this.setState({
					salesPersonsOne: [
						{
							id: '',
							name: "Change Client From",
							status: 'Active Status'
						}
					]
						.concat(salesPersonsFromApi),

				});
			})
			.catch(error => {
				console.log(error);
			});


		// getting salesPersonInformation
		Axios.get(
			myApi + "/intraco/lpg/salesPerson/list",
			{
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token
				}
			}
		)
			.then(res => {
				let salesPersonsFromApi = res.data.data.map(salesPerson => {
					return {
						id: salesPerson.id,
						name: salesPerson.name,
					};
				});

				this.setState({
					salesPersonsTwo: [
						{
							id: '',
							name:
								"Change Client To"
						}
					]
						.concat(salesPersonsFromApi)
				});
			})
			.catch(error => {
				console.log(error);
			});

		// getting clientType
		Axios
			.get(myApi + "/intraco/lpg/client_type/list", {
				headers: {
					"x-access-token": localStorageService.getItem("auth_user").token,
				},
			})
			.then((res) => {
				let clientsFromApi = res.data.data.map((client) => {
					return {
						id: client.id,
						type: client.type,
					};
				});

				this.setState({
					clientType:
						[
							{
								id: "",
								type: "Select if you want to change",
							},
						].concat(clientsFromApi)
				});
			})
			.catch((error) => {
				console.log(error);
			});

	}

	onChange(event) {
		this.setState({
			source: event.source,
			target: event.target
		});
		// console.log(this.state.target.map(x => x.client_id))

	}

	taskAssign(task) {
		return (
			<div className='p-clearfix'>
				{
					task.sales_person_id === parseInt(this.state.selectedSalesPersonOne) &&
					<div style={{ fontSize: '14px', margin: '15px 5px 0 0', textAlign: 'center' }}>
						{task.client_name || task.company_title + '_' + task.sister_concern_title}
						{/* {task.id}) {task.client_info.map((e) => e.name || e.company_title + '_' + e.title)} - {moment(task.call_time, "hh:mm").format("LT")}- {moment(task.call_date).format('Y/MM/DD')} */}
					</div>
				}
			</div>
		);
	}
	handleSubmitSalesPersonTwo = e => {
		e.persist()
		if (this.state.selectedSalesPersonTwo === '') {
			Swal.fire({
				icon: "error",
				title: "You need to select Salesperson",
				showConfirmButton: false,
			});
			this.setState({
				target: [],
			})
			Axios.get(myApi + '/intraco/lpg/company_sister_concerns/' + Number(this.state.selectedClientType) + '/' + Number(this.state.selectedSalesPersonOne), {
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
				},
			})
				.then(res => {
					if (res.data.count === 0) {
						Swal.fire({
							icon: "error",
							title: "No Client Available",
							showConfirmButton: false,
						});
					}
					if (res.data.data) {
						// console.log(res.data.data)
						if (res.data.data.length !== 0) {
							this.setState({
								isSubmitted: true,
								source: res.data.data,
								selectedSalesPersonOne: this.state.selectedSalesPersonOne
							})
						}
						else {
							this.setState({
								isSubmitted: false,
								source: res.data.data,
								selectedSalesPersonOne: this.state.selectedSalesPersonOne
							})
						}
					}
				})
		} else {
			Axios.post(myApi + '/intraco/lpg/client/reassign',
				{
					"present_saler_id": Number(this.state.selectedSalesPersonOne),
					"new_saler_id": Number(this.state.selectedSalesPersonTwo),
					"is_assign_all": false,
					"client_type": Number(this.state.selectedClientType),
					"client_id": this.state.target.map(x => (x.company_sister_concern_id && x.company_sister_concern_id) || (x.client_id && x.client_id))
				}, {
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token
				}
			}
			).then(res => {

				if (!res.data.error) {
					Swal.fire({
					  icon: "success",
					  title: res.data.message,
					  showConfirmButton: false,
					});
				  } else {
					Swal.fire({
					  icon: "error",
					  title: res.data.message,
					  showConfirmButton: false,
					});
				  }

				this.setState({
					target: [],
				})

			})
		}
	}

	handleSubmitSalesPersonOne = event => {
		event.persist();
		console.log(this.state.selectedClientType, this.state.selectedSalesPersonOne)
		Axios.get(myApi + '/intraco/lpg/company_sister_concerns/' + Number(this.state.selectedClientType) + '/' + Number(this.state.selectedSalesPersonOne), {
			headers: {
				'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
			},
		})
			.then(res => {
				if (res.data.count === 0) {
					Swal.fire({
						icon: "error",
						title: "No Client Available",
						showConfirmButton: false,
					});
				}
				if (res.data.data) {
					// console.log(res.data.data)
					if (res.data.data.length !== 0) {
						this.setState({
							isSubmitted: true,
							source: res.data.data,
							selectedSalesPersonOne: this.state.selectedSalesPersonOne
						})
					}
					else {
						this.setState({
							isSubmitted: false,
							source: res.data.data,
							selectedSalesPersonOne: this.state.selectedSalesPersonOne
						})
					}
				}
			})
	}

	handleChange = event => {
		event.persist();
	};
	render() {

		return (
			<>
				<ValidatorForm
					ref="form"
					onSubmit={this.handleSubmitSalesPersonOne}
					onError={errors => null}
				>
					<Grid container spacing={6}>
						<Grid item lg={6} md={6} sm={12} xs={12}>
							<select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
								className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
								name="selectedClientType"
								value={this.state.selectedClientType}
								onChange={e =>
									this.setState({
										source: [],
										target: [],
										selectedClientType: e.target.value,
										validationError:
											e.target.value === ""
												? "You must select Client Type"
												: ''
									})
								}
							>

								{this.state.clientType.map(c => (
									<option
										key={c.id}
										value={c.id}
									>
										{c.type}
									</option>
								))}
							</select>

							<Button color="primary" variant="contained" type="submit">
								<Icon>send</Icon>
								<span className="pl-2 capitalize">Get Client List</span>
							</Button>
						</Grid>
						<Grid item lg={6} md={6} sm={12} xs={12}>
							<select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
								className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
								name="selectedSalesPersonOne"
								value={this.state.selectedSalesPersonOne}
								onChange={e =>
									this.setState({
										source: [],
										target: [],
										selectedSalesPersonOne: e.target.value,
										validationError:
											e.target.value === ""
												? "You must select Sales Person"
												: ''
									})
								}
							>
								{
									this.state.salesPersonsOne.map(salesPerson => (

										<option
											key={salesPerson.id}
											value={salesPerson.id}
										>
											{salesPerson.name + ' -> ' + salesPerson.status}
										</option>
									))}
							</select>



						</Grid>
					</Grid>
				</ValidatorForm>
				{ this.state.isSubmitted &&
					<ValidatorForm
						ref="form"
						onSubmit={this.handleSubmitSalesPersonTwo}
						onError={errors => null}
					>
						<Grid container spacing={6}>
							<Grid item lg={6} md={6} sm={12} xs={12}>
								<select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
									className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
									name="selectedSalesPersonTwo"
									value={this.state.selectedSalesPersonTwo}
									onChange={e =>
										this.setState({
											selectedSalesPersonTwo: e.target.value,
											validationError:
												e.target.value === ""
													? "You must select Sales Person"
													: ''
										})
									}
								>
									{this.state.salesPersonsTwo.map(salesPerson => (
										salesPerson.id !== Number(this.state.selectedSalesPersonOne) ?
											<option
												key={salesPerson.id}
												value={salesPerson.id}
											>
												{salesPerson.name}
											</option> : ""
									))}
								</select>

								<Button color="primary" variant="contained" type="submit">
									<Icon>send</Icon>
									<span className="pl-2 capitalize">Save Client List</span>
								</Button>

							</Grid>
						</Grid>
					</ValidatorForm>}


				<div style={{ width: 800, marginTop: '15px' }}>
					<PrimereactStyle />

					{this.state.isSubmitted &&
						<div className='content-section implementation'>
							<PickList
								source={this.state.source}
								target={this.state.target}
								itemTemplate={this.taskAssign}
								sourceHeader='Remaining Client'
								targetHeader='Assigned Client'
								responsive={true}
								sourceStyle={{ height: '300px' }}
								targetStyle={{ height: '300px' }}
								onChange={this.onChange}
							/>
						</div>
					}

				</div>
			</>
		);
	}
}

export default TaskEX