import React, { Component } from "react";
import { Breadcrumb } from "matx";

import TaskEX from './taskEx'
import { SimpleCard } from 'matx'

class taskExchange extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Client", path: "/client/exchange" },
              { name: "Client Exchange" }
            ]}
          />
        </div>


        <SimpleCard title="">
          <TaskEX />
        </SimpleCard>
      </div>
    );
  }
}

export default taskExchange;
