import React from "react";
import { authRoles } from "../../auth/authRoles";

const clientExchangeRoutes = [
  {
    path: "/client/exchange",
    component: React.lazy(() => import("./clientExchange")),
    auth: authRoles.admin,
    // eslint-disable-next-line no-dupe-keys
    auth: authRoles.editor
  }
];

export default clientExchangeRoutes;
