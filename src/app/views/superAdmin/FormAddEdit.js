import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
function AddEditForm(props) {
  const [item, setItem] = useState([])
  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: ''
  })

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()
    fetch('http://localhost:3000/crud', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: form.name,
        last: form.last,
        email: form.email,
        phone_number: form.phone_number,
        location: form.location,
        hobby: form.hobby
      })
    })
      .then(response => response.json())
      .then(item => {
        if (Array.isArray(item)) {
          props.addItemToState(item[0])
          props.toggle()
        } else {
          //console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  const submitFormEdit = e => {
    e.preventDefault()
    //console.log(`details`, form.id, form.name, form.phone_number, form.email)
    // //console.log(this.form.state.id)
    axios.post(myApi + '/intraco/lpg/Admin/superAdmin/profile/update', {
      'id': form.id, 'name': form.name,
      'email': form.email, 'phone_number': form.phone_number
    }, {

      "headers": {
        'x-access-token': localStorageService.getItem("auth_user").token
      }
    })

      .then(response => {
        //console.log(response.data)
        // window.location.reload(false);
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
        if (response.data.error === false) {
          props.updateState(form)
        }
        props.toggle()
        // history.push({
        //   pathname: "/superAdmin/create"
        // });
        // .then(item => {
        //   if (Array.isArray(item)) {
        //     props.addItemToState(item[0])
        //     props.toggle()
        //   } else {
        //     //console.log('failure')
        //   }
        // })
      })

      .catch(err => console.log(err))
  }


  useEffect(() => {
    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby })
    }
  }, false)

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Full Name</Label>
        <Input type="text" name="name" minLength='3' id="name" onChange={onChange} value={form.name === null ? '' : form.name}  required/>
      </FormGroup>
      {/* <FormGroup>
        <Label for="last">Last Name</Label>
        <Input type="text" name="last" id="last" onChange={onChange} value={form.last === null ? '' : form.last}  />
      </FormGroup> */}
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" onChange={onChange} value={form.email === null ? '' : form.email} required/>
      </FormGroup>
      <FormGroup>
        <Label for="phone_number">Phone number</Label>
        <Input type="text" name="phone_number" id="phone_number" onChange={onChange}  value={form.phone_number === null ? '' : form.phone_number}  placeholder="ex. 555-555-5555"  readOnly/>
      </FormGroup>
      {/* <FormGroup>
        <Label for="location">Location</Label>
        <Input type="text" name="location" id="location" onChange={onChange} value={form.location === null ? '' : form.location}  placeholder="City, State" />
      </FormGroup>
      <FormGroup>
        <Label for="hobby">Hobby</Label>
        <Input type="text" name="hobby" id="hobby" onChange={onChange} value={form.hobby}  />
      </FormGroup> */}
      <Button>Submit</Button>
    </Form>
  )
}

export default AddEditForm