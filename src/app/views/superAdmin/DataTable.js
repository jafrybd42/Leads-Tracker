import React from "react";
import axios from "axios";
import myApi from "../../auth/api";
import localStorageService from "../../services/localStorageService";
import Swal from "sweetalert2";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

export default function DataTable(props) {

  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const resetPassword = (id) => {
    let confirmReset = window.confirm("Reset Account Password?");
    if (confirmReset) {
      axios
        .post(
          myApi + "/intraco/lpg/admin/superAdmin/profile/resetPassword",
          { id: id },
          {
            headers: {
              "x-access-token": localStorage.getItem("jwt_token"),
            },
          }
        )
        .then((response) => {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        })

        .catch((err) => console.log(err));
    }
  };

  const deleteItem = (id) => {
    axios
      .post(
        myApi + "/intraco/lpg/admin/superAdmin/delete",
        { id: id },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((response) => {
        Swal.fire({
          icon: "success",
          title: response.data.message,
          showConfirmButton: false,
          timer: 1500
        });
      })
      .then((item) => {
        props.deleteItemFromState(id);
      })
      .catch((err) => console.log(err));
  };

  const items =
    props.items &&
    props.items.map((item, i) => {
      return (
        <tr key={item.id}>
          <th scope="row" style={{ textAlign: "center" }}>
            {i + 1}
          </th>
          <td style={{ textAlign: "center" }}>{item.name}</td>
          <td style={{ textAlign: "center" }}>{item.used_phone_no}</td>
          <td style={{ textAlign: "center" }}>{item.email}</td>
        </tr>
      );
    });
  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  return (
    <TableContainer component={Paper} style={{ boxShadow: '0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)', backgroundColor: '#fafafa' }}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell
              className="px-0"
              style={{ width: "60px", textAlign: "center" }}
            >
              SL.
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center" }}>
              Name
            </TableCell>
            <TableCell
              className="px-0"
              style={{ width: "300px", textAlign: "center" }}
            >
              Email
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center" }}>
              Used Phone Number
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "60px" }}
                  >
                    {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.name}
                  </TableCell>
                  <TableCell
                    className="px-0 "
                    align="center"
                    style={{ width: "300px" }}
                  >
                    {item.email}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.used_phone_no}
                  </TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </TableContainer>
  );
}
