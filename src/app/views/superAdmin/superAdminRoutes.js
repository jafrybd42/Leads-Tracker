import React from "react";
import { authRoles } from "../../auth/authRoles";

const SuperAdminRoutes = [
  {
    path: "/superAdmin/create",
    component: React.lazy(() => import("./createSuperAdmin")),
    auth: authRoles.admin
  },
  {
    path: "/superAdmin/manage",
    component: React.lazy(() => import("./manageSuperAdmin")),
    auth: authRoles.admin
  },
  {
    path: "/superAdmin/edit",
    component: React.lazy(() => import("../../views/sa/sa")),
    auth: authRoles.admin
  },
  {
    path: "/superAdmin/deactiveList",
    component: React.lazy(() => import("./deactive")),
    auth: authRoles.admin
  },
];

export default SuperAdminRoutes;
