import React, { Component, Fragment } from "react";
import { Grid, Card } from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";


import StatCards from "./shared/AdminStatCard";
import AdminDashboard from "./shared/Admindashboard";

import UpgradeCard from "./shared/UpgradeCard";
import Campaigns from "./shared/Campaigns";
import { withStyles } from "@material-ui/styles";

class Dashboard1 extends Component {
  state = {};

  render() {
    let { theme } = this.props;
    let { user } = this.props;
    return (

      <Fragment>


        <div className="analytics m-sm-30 " style={{ marginTop: '35px !important' }}>
          <Grid container spacing={3}>
            <Grid item lg={8} md={8} sm={12} xs={12}>
              <StatCards />

              <AdminDashboard />


            </Grid>


            <Grid item lg={4} md={4} sm={12} xs={12}>
              <Card className="px-6 py-4 mb-6">
                <div className="card-subtitle" style={{ textAlign: 'center', color: '#1a71b5', margin: '5px' }}><b><strong>{user.role}</strong></b></div>
                <div className="card-title" style={{ textAlign: 'center' }}>{user.displayName}</div>
                <br></br>
                <img src={user.photoURL} alt="user" style={{ height: '300px', display: 'block', margin: 'auto' }} />
              </Card>
              <Campaigns />

            </Grid>


          </Grid>


        </div>
      </Fragment>
    );
  }
}



Dashboard1.propTypes = {

  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  logoutUser: PropTypes.func.isRequired,
  user: state.user,
});


export default withStyles({}, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, {
    })(Dashboard1)
  )
);