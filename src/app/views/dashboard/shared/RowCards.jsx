import React, { Fragment } from "react";
import { format } from "date-fns";
import {
  Grid,
  Card,
  Icon,
  IconButton,
  Checkbox,
  Fab,
  Avatar,
  Hidden
} from "@material-ui/core";
import Axios from "axios";
import localStorageService from "app/services/localStorageService";
import Moment from 'moment'
import myApi from "app/auth/api";
const RowCards = () => {
  let [appointmentList, setAppointmentList] = React.useState('');

  const fetchData = React.useCallback(() => {
    Axios.get( myApi + "/intraco/lpg/appointment/list",{
      headers: {
        'x-access-token': localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').token
      }
    })
      .then((response) => {
        if (response.data.data) {
          setAppointmentList(response.data.data)
        }
      })
      .catch((error) => {
        //console.log(error)
      })
  }, [])
  React.useEffect(() => {
    fetchData()
  }, [fetchData])


  return (
    <Card elevation={3} className="pt-5 mb-6">
      <div className="card-title px-6 mb-3">Appointments </div>
      <div className="overflow-auto">
        {
          appointmentList.length === 0
            ? (<div className="alert alert-info" role="alert" style={{ width: '100%', background: 'white', border: 'white' }}>
              No More Appointments For Today !
        </div>)
            : (appointmentList.map(ap => (
              <Fragment key={ap.id}>
                <Card className="py-2 px-4 project-card">

                  <Grid container alignItems="center">
                    <Grid item md={5} xs={7}>
                      <div className="flex items-center">

                        <Hidden smDown>
                          {
                            <Fab
                              className="ml-4 bg-green box-shadow-none text-white"
                              size="small"
                            >
                              <Icon>date_range</Icon>
                            </Fab>
                          }
                        </Hidden>
                        <span className="card__roject-name font-medium">
                          {ap.service_details}                  </span>
                      </div>
                    </Grid>

                    <Grid item md={3} xs={4}>
                      <div className="flex justify-end">


                        {Moment(ap.appointment_date).format('YYYY-MM-DD')}

                      </div>
                    </Grid>
                  </Grid>

                </Card>
              </Fragment>
            )))}
      </div>
    </Card>

  )




};

export default RowCards;
