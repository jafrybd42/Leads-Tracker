import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DeactiveTable from "./deactiveTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";

function DeactiveList(props) {
  const [form, setValues] = useState({
    isSubmitted: false,
  });
  const [items, setItems] = useState([]);
  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/admin/subAdmin/delete_list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        if (response.data.error) {
          setValues({ ...form, isSubmitted: false })
          setItems(Array.isArray(response.data.data) ? response.data.data : []);
        } else {
          setValues({ ...form, isSubmitted: true })
          setItems(Array.isArray(response.data.data) ? response.data.data : []);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Sub Admin", path: "/subAdmin/deactiveList" },
                  { name: "Deactive List" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          {form.isSubmitted ?
          <Row>
            <Col>
              <DeactiveTable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
           :
           <div class="alert alert-primary" role="alert">
             No Data Available !
           </div>
         }
        </Container>
      </div>
    </div>
  );
}

export default DeactiveList;
