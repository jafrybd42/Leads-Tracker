import React from "react";
import {
  Card,
  Icon,
  IconButton,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import axios from 'axios'
const ManageForm = () => {
  let [subAdminList, setSubAdminList] = React.useState('');
  // let {user} = this.props
  const fetchData = React.useCallback(() => {
    axios({
      "method": "GET",
      "url": "http://192.168.1.141:3001/intraco/lpg/admin/subAdmin/list",
      "headers": {
       
        "x-access-token": localStorage.getItem("jwt_token")
      }
    })
    .then((response) => {
      // //console.log(token)
      //console.log(response.data.data)
      setSubAdminList(response.data.data)
    })
    .catch((error) => {
      //console.log(error)
    })
  }, [])
  React.useEffect(() => {
    fetchData()
  }, [fetchData])

  
  return (

    <Card elevation={3} className="pt-5 mb-6">
      <div className="card-title px-6 mb-3">Sub Admin List</div>
      <div className="overflow-auto">

        
        <Table className="product-table">
          <TableHead>
            <TableRow>
              <TableCell className="px-6" colSpan={4}>
                Name
              </TableCell>
              <TableCell className="px-0" colSpan={2}>
                Phone
              </TableCell>
              <TableCell className="px-0" colSpan={2}>
                Email
              </TableCell>
              <TableCell className="px-0" colSpan={1}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {/* {ManageForm.map((product, index) => ( */}
            { subAdminList.length == 0
            ? 'Loading users...'
            : subAdminList.map(user => (
              <TableRow key={user.id}>
                <TableCell className="px-0 capitalize" colSpan={4} align="left">
                  <div className="flex items-center">
                    <img
                      className="circular-image-small"
                      src={'/profile/'+user.image}
                      alt="user"
                    />
                    <p className="m-0 ml-8">{user.name}</p>
                  </div>
                </TableCell>
                <TableCell className="px-0 capitalize" align="left" colSpan={2}>
                  
                    
                    {user.phone_number}

                </TableCell>

                <TableCell className="px-0 capitalize" align="left" colSpan={2}>
                 
                  {user.email}
                </TableCell>
                <TableCell className="px-0" colSpan={1}>
                  <IconButton>
                    <Icon color="primary">edit</Icon>
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        
      </div>
    </Card>
  );
};
export default ManageForm;
