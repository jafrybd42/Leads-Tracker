import React from "react";
import { authRoles } from "../../auth/authRoles";
const SubAdminRoutes = [
  {
    path: "/subAdmin/create",
    component: React.lazy(() => import("./createSubAdmin")),
    auth: authRoles.admin,
  },

  {
    path: "/subAdmin/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
  },
  {
    path: "/subAdmin/deactiveList",
    component: React.lazy(() => import("./deactiveList")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
];

export default SubAdminRoutes;
