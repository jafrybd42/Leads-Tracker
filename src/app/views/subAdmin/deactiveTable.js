import React from "react";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
function DeactiveTable(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const resetPassword = (id) => {
    let confirmReset = window.confirm("Reset Account Password?");
    if (confirmReset) {
      axios
        .post(
          myApi + "/intraco/lpg/admin/superAdmin/profile/resetPassword",
          { id: id },
          {
            headers: {
              "x-access-token": localStorage.getItem("jwt_token"),
            },
          }
        )
        .then((response) => {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        })

        .catch((err) => console.log(err));
    }
  };

  const deleteItem = (id) => {
    axios
      .post(
        myApi + "/intraco/lpg/admin/superAdmin/delete",
        { id: id },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((response) => {
        Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
      })
      .then((item) => {
        props.deleteItemFromState(id);
      })
      .catch((err) => console.log(err));
  };
  const items =
    props.items &&
    props.items.map((item, i) => {
      return (
        <tr key={item.id}>
          <th scope="row" style={{ textAlign: "center" }}>
            {i + 1}
          </th>
          <td style={{ textAlign: "center" }}>{item.name}</td>
          <td>{item.used_phone_no}</td>
          <td>{item.email}</td>
        </tr>
      );
    });

  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell
              className="px-0"
              style={{ width: "60px", textAlign: "center" }}
            >
              SL.
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center" }}>
              Name
            </TableCell>
            <TableCell
              className="px-0"
              style={{ width: "300px", textAlign: "center" }}
            >
              Email
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center" }}>
              Used Phone Number
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "60px" }}
                  >
                   {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.name}
                  </TableCell>
                  <TableCell
                    className="px-0 "
                    align="center"
                    style={{ width: "300px" }}
                  >
                    {item.email}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.used_phone_no}
                  </TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
}

export default DeactiveTable;
