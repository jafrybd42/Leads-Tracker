import React from 'react';
import { PickList } from '@bit/primefaces.primereact.picklist';
import PrimereactStyle from '@bit/primefaces.primereact.internal.stylelinks';
import myApi from 'app/auth/api';
import localStorageService from 'app/services/localStorageService';
import Axios from 'axios';
import { ValidatorForm } from "react-material-ui-form-validator";
import {

	Button,
	Icon,
	Grid,
} from "@material-ui/core";
import moment from 'moment'
import Swal from 'sweetalert2';
class TaskEX extends React.Component {
	constructor() {
		super();
		this.state = ({
			source: [],
			target: [],
			salesPersonsOne: [],
			salesPersonsTwo: [],
			selectedSalesPersonOne: '',
			selectedSalesPersonTwo: '',
			isSubmitted: false

		});
		this.taskservice = this.state.salesPersonsOne;
		this.taskAssign = this.taskAssign.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	componentDidMount() {

		this.setState({
			source: []
		});

		// getting salesPersonInformation
		Axios.get(
			myApi + "/intraco/lpg/salesPerson/allList",
			{
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token
				}
			}
		)
			.then(res => {
				let salesPersonsFromApi = res.data.data.map(salesPerson => {
					return {
						id: salesPerson.id,
						name: salesPerson.name,
						status: salesPerson.active_status === 0 ? 'Active' : 'Deactive'
					};
				});

				this.setState({
					salesPersonsOne: [
						{
							id: 0,
							name: "Change Task From",
							status: 'Active Status'
						}
					]
						.concat(salesPersonsFromApi),

				});
			})
			.catch(error => {
				console.log(error);
			});


		// getting salesPersonInformation
		Axios.get(
			myApi + "/intraco/lpg/salesPerson/list",
			{
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token
				}
			}
		)
			.then(res => {
				let salesPersonsFromApi = res.data.data.map(salesPerson => {
					return {
						id: salesPerson.id,
						name: salesPerson.name,
					};
				});

				this.setState({
					salesPersonsTwo: [
						{
							id: 0,
							name:
								"Change Task To"
						}
					]
						.concat(salesPersonsFromApi)
				});
			})
			.catch(error => {
				console.log(error);
			});
	}

	onChange(event) {
		this.setState({
			source: event.source,
			target: event.target
		});
	}

	taskAssign(task) {
		return (
			<div className='p-clearfix'>
				{
					task.sales_person_id === parseInt(this.state.selectedSalesPersonOne) &&
					<div style={{ fontSize: '14px', margin: '15px 5px 0 0', textAlign: 'left' }}>
						{task.client_info.map((e) => e.name || e.company_title + '_' + e.title)} - {moment(task.call_time, "hh:mm").format("LT")}- {moment(task.call_date).format('Y/MM/DD')}
					</div>
				}
			</div>
		);
	}

	handleSubmitSalesPersonTwo = e => {
		e.persist()
		if (this.state.selectedSalesPersonTwo === '') {
			Swal.fire({
				icon: "error",
				title: 'You need to select a Salesperson',
				showConfirmButton: false,
			});
			this.setState({
				target: [],
			})
			Axios.post(myApi + '/intraco/lpg/task/incompletTaskList', {
				'sales_person_id': parseInt(this.state.selectedSalesPersonOne),

			}, {
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
				},
			})
				.then(res => {
					if (res.data.data) {


						this.setState({
							isSubmitted: true,
							source: res.data.data,
							selectedSalesPersonOne: this.state.selectedSalesPersonOne
						})

					}
				})
		} else {
			Axios.post(myApi + '/intraco/lpg/task/reassign',
				{
					"present_saler_id": this.state.selectedSalesPersonOne,
					"new_saler_id": this.state.selectedSalesPersonTwo,
					"is_assign_all": false,
					"task_id": this.state.target.map(function (x) { return x.id; })
				}, {
				headers: {
					'x-access-token': localStorageService.getItem('auth_user').token
				}
			}
			).then(res => {
				if (res.data.error !== true) {
					Swal.fire({
						icon: "success",
						title: res.data.message,
						showConfirmButton: false,
					});

					this.setState({
						target: [],
					})
				} else {
					Swal.fire({
						icon: "error",
						title: res.data.message,
						showConfirmButton: false,
					});

				}
			})
		}
	}

	handleSubmitSalesPersonOne = event => {
		event.persist();
		Axios.post(myApi + '/intraco/lpg/task/incompletTaskList', {
			'sales_person_id': parseInt(this.state.selectedSalesPersonOne),

		}, {
			headers: {
				'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
			},
		})
			.then(res => {
				if (res.data.data) {


					this.setState({
						isSubmitted: true,
						source: res.data.data,
						selectedSalesPersonOne: this.state.selectedSalesPersonOne
					})

				}
			})
	}

	handleChange = event => {
		event.persist();
	};
	render() {

		return (
			<>
				<ValidatorForm
					ref="form"
					onSubmit={this.handleSubmitSalesPersonOne}
					onError={errors => null}
				>
					<Grid container spacing={6}>
						<Grid item lg={6} md={6} sm={12} xs={12}>
							<select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
								className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
								name="selectedSalesPersonOne"
								value={this.state.selectedSalesPersonOne}
								onChange={e =>
									this.setState({
										source: [],
										target: [],
										selectedSalesPersonOne: e.target.value,
										validationError:
											e.target.value === ""
												? "You must select Sales Person"
												: ''
									})
								}
							>
								{this.state.salesPersonsOne.map(salesPerson => (
									<option
										key={salesPerson.id}
										value={salesPerson.id}
									>
										{salesPerson.name + ' -> ' + salesPerson.status}
									</option>
								))}
							</select>

							<Button color="primary" variant="contained" type="submit">
								<Icon>send</Icon>
								<span className="pl-2 capitalize">Get Task List</span>
							</Button>

						</Grid>
					</Grid>
				</ValidatorForm>
				{ this.state.isSubmitted &&
					<ValidatorForm
						ref="form"
						onSubmit={this.handleSubmitSalesPersonTwo}
						onError={errors => null}
					>
						<Grid container spacing={6}>
							<Grid item lg={6} md={6} sm={12} xs={12}>
								<select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
									className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
									name="selectedSalesPersonTwo"
									value={this.state.selectedSalesPersonTwo}
									onChange={e =>
										this.setState({
											selectedSalesPersonTwo: e.target.value,
											validationError:
												e.target.value === ""
													? "You must select Sales Person"
													: ''
										})
									}
								>
									{this.state.salesPersonsTwo.map(salesPerson => (
										salesPerson.id !== Number(this.state.selectedSalesPersonOne) ?
											<option
												key={salesPerson.id}
												value={salesPerson.id}
											>
												{salesPerson.name}
											</option> : ""
									))}
								</select>

								<Button color="primary" variant="contained" type="submit">
									<Icon>send</Icon>
									<span className="pl-2 capitalize">Save Task List</span>
								</Button>

							</Grid>
						</Grid>
					</ValidatorForm>}


				<div style={{ width: 800, marginTop: '15px' }}>
					<PrimereactStyle />

					{this.state.isSubmitted &&
						<div className='content-section implementation'>
							<PickList
								source={this.state.source}
								target={this.state.target}
								itemTemplate={this.taskAssign}
								sourceHeader='Remaining Tasks'
								targetHeader='Assigned'
								responsive={true}
								sourceStyle={{ height: '300px' }}
								targetStyle={{ height: '300px' }}
								onChange={this.onChange}
							/>
						</div>
					}

				</div>
			</>
		);
	}
}

export default TaskEX