import React from "react";
import { authRoles } from "../../auth/authRoles";

const taskExchangeRoutes = [
  {
    path: "/task/exchange",
    component: React.lazy(() => import("./taskExchange")),
    auth: authRoles.admin,
    // eslint-disable-next-line no-dupe-keys
    auth: authRoles.editor
  }
];

export default taskExchangeRoutes;
