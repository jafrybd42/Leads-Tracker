import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
// import history from '../../../history'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,
  // Radio,
  // RadioGroup,
  // FormControlLabel,
  // Checkbox
} from "@material-ui/core";
// import {
//   MuiPickersUtilsProvider,
//   KeyboardDatePicker
// } from "@material-ui/pickers";
import "date-fns";
// import DateFnsUtils from "@date-io/date-fns";
// import themeOptions from "app/MatxLayout/MatxTheme/themeOptions";
import myApi from "app/auth/api";
import taskEx from "./taskEx";

class CreateForm extends Component {


  state = {
    name: "",
    firstName: "",
    email: "",
    date: new Date(),
    creditCard: "",
    phone: "",
    password: "",
    confirmPassword: "",
    gender: "",
    agreement: ""
  };

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    // let { user } = this.props
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    // //console.log(this.state.phone)
    // let baseURL;
    //console.log('type', this.state.name)
    axios.post(myApi+'/intraco/lpg/client_type/add', {'type': this.state.name},{
  
      "headers": {
        "x-access-token":  localStorageService.getItem("auth_user").token
      }
    })

      .then(res => {
        //console.log(res.data.error)
        // responseData = res.data
        // if (res.data.error == 'false') {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          this.setState({
            "name" : ''
          })

       
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    // //console.log(date);

    this.setState({ date });
  };

  render() {
    // let { user } = this.props
    let {
      name,
      // firstName,
      // creditCard,
      // phone,
      // password,
      // confirmPassword,
      // gender,
      // date,
      // email
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Full Name (Max length : 25)"
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={[
                  "required",
                  "maxStringLength: 25"
                ]}
                errorMessages={["this field is required"]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
        <taskEx />
      </div>
    );
  }
}

export default CreateForm;
