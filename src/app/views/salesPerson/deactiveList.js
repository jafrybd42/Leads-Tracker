/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DeactiveTable from "./deactiveTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";

function Sa() {
  const [form, setValues] = useState({
    isSubmitted: false,
  });
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/salesPerson/delete_list",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        if (response.data.error) {
          setValues({ ...form, isSubmitted: false })
          setItems(Array.isArray(response.data.data) ? response.data.data : []);
        } else {
          setValues({ ...form, isSubmitted: true })
          setItems(Array.isArray(response.data.data) ? response.data.data : []);
        }
      })
      .catch(() => {
        //console.log(error)
      });
  };


  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <h1 style={{ margin: "20px 0" }}></h1>
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Sales Person", path: "/salesPerson/deactiveList" },
                  { name: "Deactive List" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          {form.isSubmitted ?
            <Row>
              <Col>
                <DeactiveTable
                  items={items}
                  updateState={updateState}
                  deleteItemFromState={deleteItemFromState}
                />
              </Col>
            </Row>
            :
            <div class="alert alert-primary" role="alert">
              No Data Available !
           </div>
          }
        </Container>
      </div>
    </div>
  );
}

export default Sa;
