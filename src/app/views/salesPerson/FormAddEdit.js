/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
// import { Input } from 'react-input-component';
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'
import Swal from 'sweetalert2'
function AddEditForm(props) {
  const [item, setItem] = useState([])
  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: ''
  })

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

    axios.post(myApi + '/intraco/lpg/salesPerson/profile/update', {
      'id': form.id, 'name': form.name,
      'email': form.email, 'phone_number': form.phone_number
    }, {

      "headers": {
        'x-access-token':  localStorage.getItem("jwt_token")
      }
    })

      .then(response => {

        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
        if (response.data.error === false) {
          props.updateState(form)
        }
        props.toggle()

      })

      .catch(err => console.log(err))
  }


  useEffect(() => {
    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Full Name</Label>
        <Input type="text" minLength='3' name="name" id="name" onChange={onChange} value={form.name === null ? '' : form.name} required />
      </FormGroup>

      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" onChange={onChange} value={form.email === null ? '' : form.email} />
      </FormGroup>
      <FormGroup>
        <Label for="phone_number">Phone number</Label>
        <Input type="text" name="phone_number" id="phone_number" onChange={onChange} value={form.phone_number === null ? '' : form.phone_number} placeholder="ex. 555-555-5555"
        maxlength="11" minLength='11' readOnly/>
      </FormGroup>

      <Button>Submit</Button>
    </Form>
  )
}

export default AddEditForm