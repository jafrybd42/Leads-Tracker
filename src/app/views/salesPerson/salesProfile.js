import React from 'react'
import { Table, Button } from 'reactstrap';
import ModalForm from './Modal'
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'

function SalesProfile(props) {
  const resetPassword = id => {
    let confirmReset = window.confirm('Reset Account Password?')
    if (confirmReset) {
      axios.post(myApi+'/intraco/lpg/salesPerson/profile/resetPassword', { 'id': id }, {

        "headers": {
          'x-access-token': localStorage.getItem("jwt_token")
        }
      }) 
          .then(response => {
           
            if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
          })
          
          .catch(err => console.log(err))
      }
  }
  const deleteItem = id => {
    let confirmDelete = window.confirm('Delete item forever?')
    if (confirmDelete) {
      axios.post(myApi+'/intraco/lpg/salesPerson/delete', { 'id': id }, {

        "headers": {
          'x-access-token':  localStorage.getItem("jwt_token")
        }
      }) 
          .then(response => {
           
          })
          .then(item => {
            props.deleteItemFromState(id)
          })
          .catch(err => console.log(err))
      }
  }

    const items = props.items && props.items.map((item,i) => {
      return (
        <tr key={item.id}>
          <th scope="row">{i+1}</th>
          <td><a href={"/account/profile/"+item.id}>{item.name}</a></td>
          <td>{item.phone_number}</td>
          <td>{item.email}</td>
          <td> 
            <div style={{ width: "120px" }}>

            <Button className="btn btn-light" onClick={() => resetPassword(item.id)}><img src='/assets/reset-password.png' style={{height:'32px'}} /></Button>
          </div>
          </td> 

          <td>
            <div style={{ width: "150px" }}>

              <ModalForm buttonLabel="Edit" item={item} updateState={props.updateState} />
              {' '}
              <Button color="danger" onClick={() => deleteItem(item.id)}>Delete</Button>
            </div>
          </td>
        </tr>
      )
    })

    return (
      <Table responsive hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Reset Password</th>
            <th>Action</th>


          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    )
  }

  export default SalesProfile