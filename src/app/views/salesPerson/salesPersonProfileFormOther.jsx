/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';

import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
function SalesPersonProfileFormOther(props) {
  const [item, setItem] = useState([])
  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    image: ''
  })

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

    axios.post(myApi + '/intraco/lpg/salesPerson/profile/update', {
      'id': form.id, 'name': form.name,
      'email': form.email, 'phone_number': form.phone_number
    }, {

      "headers": {
        'x-access-token': localStorage.getItem("jwt_token")
      }
    })

      .then(response => {

        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
        if (response.data.error === false) {
          props.updateState(form)
        }
        props.toggle()

      })

      .catch(err => console.log(err))
  }


  useEffect(() => {

    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, image } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, image })



      axios.get(myApi + '/intraco/lpg/salesPerson/profile/view/' + props.item.id, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {


        setValues({

          name: response.data.data.name,
          phone: response.data.data.phone_number,
          email: response.data.data.email,
          photo: response.data.data.image,

        })
      })

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  return (


    <div>
      <div className="modal-dialog" style={{maxWidth : '350px!important'}}>
         <div className="modal-content" style={{marginTop : '-35px'}}>
          {/* <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" />
            <h4 className="modal-title" id="myModalLabel">More About <font color = 'green'>{props.item.name}</font></h4>
          </div> */}
          <div className="modal-body">
            <center>
              <img src='/profile/sales_person.jpg' name="aboutme" className="circular-image-medium" width={140} height={140} border={0} alt="profile_image" />
              <h3 className="media-heading">{props.item.name}</h3><br />
              <span><strong>Email : </strong></span>
              <span className="label label-warning">{props.item.email}</span>

            </center>
            <hr />
            <center>
              <span><strong>Phone Number : </strong></span>
              <span className="label label-warning">{props.item.phone_number}</span>

            </center>
          </div>

        </div>
      </div>
    </div>


  )
}

export default SalesPersonProfileFormOther
