import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import CreateForm from "./CreateForm";

class CreateSalesPerson extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Sales Person", path: "/salesPerson/create" },
              { name: "Create Sales Person" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><CreateForm /></Card>
      </div>
    );
  }
}

export default CreateSalesPerson;
