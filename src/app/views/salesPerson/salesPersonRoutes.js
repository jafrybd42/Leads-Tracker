/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const SalesPersonRoutes = [
  {
    path: "/salesPerson/create",
    component: React.lazy(() => import("./createSalesPerson")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
  {
    path: "/salesPerson/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  }
  ,
  {
    path: "/salesPerson/deactiveList",
    component: React.lazy(() => import("./deactiveList")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
  {
    path: "/account/profile/:item.id",
    component: React.lazy(() => import("./salesPersonProfileOther")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
];

export default SalesPersonRoutes;
