import React from "react";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import ModalOther from "./ModalOther";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

export default function DataTable(props) {
  const tableRef = React.createRef();
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
    tableRef.current && tableRef.current.scrollIntoView();
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const resetPassword = (id) => {
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, reset password!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/salesPerson/profile/resetPassword",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((response) => {
            Swal.fire(response.data.message, "", "success");
          })

          .catch((err) => console.log(err));
      }
    });
  };
  const deleteItem = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/salesPerson/delete",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((response) => {
            Swal.fire(response.data.message, "", "success");
          })
          .then((item) => {
            props.deleteItemFromState(id);
          })
          .catch((err) => console.log(err));
      }
    });
  };
  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  return (
    <TableContainer component={Paper} style={{ boxShadow: '0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)', backgroundColor: '#fafafa' }}>
      <Table
      ref={tableRef}
      className={classes.table}
      aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell
              className="px-0"
              style={{ textAlign: "center", width: "5%" }}
            >
              SL.
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center" , width: "20%"}}>
              Name
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center",width: "15%" }}>
              Phone
            </TableCell>
            <TableCell
              className="px-0"
              style={{ textAlign: "center", width: "15%" }}
            >
              Email
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center",width: "10%" }}>
              Reset Password
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center",width: "15%" }}>
              View
            </TableCell>
            <TableCell className="px-0" style={{ textAlign: "center",width: "15%" }}>
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell className="px-0 capitalize" align="center">
                    {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.name}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.phone_number}
                  </TableCell>
                  <TableCell
                    className="px-0 "
                    align="center"
                    style={{ width: "250px" }}
                  >
                    {item.email}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ margin: "auto" }}
                  >
                    <Button
                      className="btn btn-light"
                      onClick={() => resetPassword(item.id)}
                    >
                      <img
                        src="/assets/reset-password.png"
                        style={{ height: "32px" }}
                        alt="reset"
                      />
                    </Button>
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ margin: "auto" }}
                  >
                    <ModalOther buttonLabel="View" item={item} />
                  </TableCell>
                  <TableCell className="px-0 capitalize">
                    <ModalForm
                      buttonLabel="Edit"
                      item={item}
                      updateState={props.updateState}
                    />{" "}
                    <Button color="danger" onClick={() => deleteItem(item.id)}>
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </TableContainer>
  );
}

