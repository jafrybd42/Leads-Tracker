import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SubAdminProfileNew from "./subAdminProfileNew";

class SubAdminProfile extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/subadmin" },
              { name: "SUB-ADMIN" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SubAdminProfileNew /></Card>
      </div>
    );
  }
}

export default SubAdminProfile;
