import React from "react";
import { authRoles } from "../../auth/authRoles";

const Profile = [
  {
    path: "/profile/admin",
    component: React.lazy(() => import("./adminProfile")),
    auth: authRoles.admin
    // auth: authRoles.editor
  },
  {
    path: "/profile/subadmin",
    component: React.lazy(() => import("./subAdminProfile")),
    // auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/profile/sales_person",
    component: React.lazy(() => import("./salesPersonProfile")),
    // auth: authRoles.admin,
    auth: authRoles.guest
  }
];

export default Profile;
