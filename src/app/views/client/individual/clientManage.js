/* eslint-disable no-unused-vars */
import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import myApi from "../../../auth/api";
import localStorageService from "app/services/localStorageService";
import { Grid } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import Swal from "sweetalert2";
import { Table } from "reactstrap";
import { TableHead, TableBody, TableRow, TableCell } from "@material-ui/core";
function ClientManage(props) {
  const [selectedClientTitle, setSelectedClientTitle] = useState("");
  const [selectedClientId, setSelectedClientId] = useState("");
  const [client_type_array, setClientType] = useState([]);
  const [individualClientList, setIndividualClientList] = useState([]);
  const [items, setItems] = useState([]);
  const location = useLocation();
  const getItems = () => {
    // console.log(Number(location.state.client_type_id.toString()))
    // console.log(Number(location.state.map(e => e.client_type_id).toString()))
    // getting clientType
    axios
      .get(myApi + "/intraco/lpg/client_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user") && localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientType([].concat(clientsFromApi));
      })
      .catch((error) => {});
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/client/list",
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((response) => {
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/client/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        // console.log(res);
        let client = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.name,
          };
        });

        setIndividualClientList(
          [
            {
              value: 0,
              label: "Select All Individual Client",
            },
          ].concat(client)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const addItemToState = (item) => {
    setItems([...items, item]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);
  const onSelectClientChange = (label, value) => {
    if (label !== null) {
      setSelectedClientTitle(label.label);
      console.log(label.value);
      setSelectedClientId(label.value);
    }
    if (label === null) {
      setSelectedClientTitle("");
    }
  };
  const handleSubmit = () => {
    if (selectedClientTitle === "Select All Individual Client") {
      axios
        .get(myApi + "/intraco/lpg/client/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
          },
        })
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    } else {
      axios
        .post(
          myApi +
            "/intraco/lpg/client/" +
            selectedClientId +
            "/searchIndividualClient",
          {
            name: selectedClientTitle,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
                icon: "error",
                title: "No Data Found",
                showConfirmButton: false,
                timer: 1000,
              });
          }
        });
    }
  };
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };
  return (
    <Fragment>
      <div
        className="analytics m-sm-30 "
        style={{ marginTop: "35px !important" }}
      >
        <Grid container spacing={3}>
          <Grid item lg={12} md={8} sm={12} xs={12}>
            <div className="modal-body">
              <Table responsive hover style={{ textAlign: "center" }}>
                <TableHead>
                  <TableRow style={{ textAlign: "center" }}>
                    <TableCell className="px-0">Client Name</TableCell>
                    <TableCell className="px-0 capitalize">
                      <strong>
                        {location.state.client_info
                          ? location.state.client_info
                              .map((e) => e.name)
                              .toString()
                          : location.state
                              .map((e) => e.client_info.map((e) => e.name))
                              .toString()}
                      </strong>
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ textAlign: "center" }}>
                    <TableCell className="px-0">Client type</TableCell>
                    <TableCell className="px-0 capitalize">
                      {location.state.client_info
                        ? locateValueById(
                          client_type_array,
                          Number(location.state.client_type_id.toString())
                        ) && locateValueById(
                            client_type_array,
                            Number(location.state.client_type_id.toString())
                          ).type
                        : locateValueById(
                          client_type_array,
                          Number(
                            location.state
                              .map((e) => e.client_type_id)
                              .toString()
                          )) && locateValueById(
                            client_type_array,
                            Number(
                              location.state
                                .map((e) => e.client_type_id)
                                .toString()
                            )
                          ).type}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ textAlign: "center" }}>
                    <TableCell className="px-0">Email</TableCell>
                    <TableCell className="px-0 capitalize">
                      {location.state.client_info
                        ? location.state.client_info
                            .map((e) => e.email)
                            .toString()
                        : location.state
                            .map((e) => e.client_info.map((e) => e.email))
                            .toString()}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ textAlign: "center" }}>
                    <TableCell className="px-0">Primary Phone Number</TableCell>
                    <TableCell className="px-0 capitalize">
                      {location.state.client_info
                        ? location.state.client_info
                            .map((e) => e.primary_phone_no)
                            .toString()
                        : location.state
                            .map((e) =>
                              e.client_info.map((e) => e.primary_phone_no)
                            )
                            .toString()}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ textAlign: "center" }}>
                    <TableCell className="px-0">
                      Secondary Phone Number
                    </TableCell>
                    <TableCell className="px-0 capitalize">
                      {location.state.client_info
                        ? location.state.client_info
                            .map((e) => e.secondary_phone_no)
                            .toString()
                        : location.state
                            .map((e) =>
                              e.client_info.map((e) => e.secondary_phone_no)
                            )
                            .toString()}
                    </TableCell>
                  </TableRow>
                  <TableRow style={{ textAlign: "center" }}>
                    <TableCell className="px-0">Location</TableCell>
                    <TableCell className="px-0 capitalize">
                      {location.state.client_info
                        ? location.state.client_info
                            .map((e) => e.address)
                            .toString()
                        : location.state
                            .map((e) => e.client_info.map((e) => e.address))
                            .toString()}
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody></TableBody>
              </Table>
            </div>
          </Grid>
        </Grid>
      </div>
    </Fragment>
  );
}

export default ClientManage;
