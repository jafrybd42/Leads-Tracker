import React, { useState } from "react";
import { Button } from "reactstrap";
// import ModalForm from "./Modal";
// import axios from "axios";
// import myApi from "../../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import history from "../../../../history";
import myApi from "app/auth/api";
import Swal from "sweetalert2";
import Axios from "axios";
import ModalForm from "./ModalEdit";

const DeactiveDataTable = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const deleteItem = (id) => {
    Swal.fire({
      title: "Are you sure?",
      // text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        Axios.post(
          myApi + "/intraco/lpg/company_sister_concern_contacts/delete",
          { id: id },
          {
            headers: {
              "x-access-token": localStorage.getItem("jwt_token"),
            },
          }
        )
          .then((response) => {
            Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
          })
          .then((item) => {
            props.deleteItemFromState(id);
          })
          .catch((err) => console.log(err));
      }
    });
  };
  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell align="center" className="px-0" style={{ width: "5%" }}>
              SL.
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "15%" }}>
              Name
            </TableCell>

            <TableCell align="center" className="px-0" style={{ width: "17%" }}>
              Primary Phone
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "17%" }}>
              Secondary Phone
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "21%" }}>
              Rank
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "25%" }}>
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "5%" }}
                  >
                   {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "15%" }}
                  >
                    {item.name}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "17%" }}
                  >
                    {item.phone_no_primary}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "17%" }}
                  >
                    {item.phone_no_secondary}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "21%" }}
                  >
                    {item.rank}
                  </TableCell>

                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "25%" }}
                  >
                    <ModalForm
                      buttonLabel="Edit"
                      item={item}
                      updateState={props.updateState}
                    />{" "}
                    <Button color="danger" onClick={() => deleteItem(item.id)}>
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items && props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default DeactiveDataTable;
