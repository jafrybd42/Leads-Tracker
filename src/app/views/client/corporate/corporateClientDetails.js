/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DeactiveDataTable from "./newTableMemberList";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../../auth/api";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

import localStorageService from "app/services/localStorageService";
import { useLocation } from "react-router-dom";
import history from "../../../../history";

function Sa(props) {
  const [name, setNames] = useState("");
  const [selectedCompany, setSelectedCompany] = useState("");
  const [selectedSisterConcern, setSelectedSisterConcern] = useState("");
  const [company, setCompany] = useState([]);
  const [sisterConcernList, setSisterConcernList] = useState([]);
  const [items, setItems] = useState([]);
  const location = useLocation();

  const getItems = () => {
    axios({
      method: "GET",
      url:
        myApi +
        "/intraco/lpg/company_sister_concern_contacts/" +
        location.state +
        "/contactList",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/company/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        console.log(res);
        let companies = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.title,
          };
        });

        setCompany(
          [
            {
              value: 0,
              label: "Select All Company",
            },
          ].concat(companies)
        );
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/company_sister_concerns/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        console.log(res);
        let companies = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.sister_concern,
          };
        });

        setSisterConcernList(
          [
            {
              value: 0,
              label: "Select All Sister Concern",
            },
          ].concat(companies)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
    setNames([...name, name]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectChangeCompany = (label) => {
    if (label !== null) {
      setSelectedCompany(label.label);
    }
    if (label === null) {
      setSelectedCompany("");
    }
  };

  const onSelectChangeSisterConcern = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }
  };

  const handleSubmit = () => {
    console.log(selectedSisterConcern, selectedCompany);
    if (selectedCompany === "Select All Company") {
      axios
        .get(
          myApi + "/intraco/lpg/company/allList",

          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    } else {
      axios
        .post(
          myApi + "/intraco/lpg/company/search",
          {
            title: selectedCompany,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    }
  };

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Corporate Client", path: "/client/corporate" },
                  { name: "Member List" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          <br></br>
          <br></br>
          {localStorageService.getItem("auth_user") &&
            localStorageService.getItem("auth_user").role ===
              "SALES-PERSON" && (
              <Row>
                <Col>
                  <button
                    className="btn btn-primary"
                    style={{ marginLeft: "87%", marginTop: "-25px" }}
                    onClick={() =>
                      history.push({
                        pathname: "/client/member/add/" + location.state,
                        state: location.state,
                      })
                    }
                  >
                    Add Member
                  </button>
                </Col>
              </Row>
            )}
          <Row>
            <Col>
              <DeactiveDataTable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
// }

export default Sa;
