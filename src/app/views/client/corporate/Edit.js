/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
// import { Input } from 'react-input-component';
import axios from "axios";
import myApi from "../../../auth/api";
// import history from '../../../history'
import Swal from "sweetalert2";
function AddEditForm(props) {
  const [item, setItem] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    name: "",
    phone_no_primary: "",
    phone_no_secondary: "",
    rank: "",
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    axios
      .post(
        myApi +
          "/intraco/lpg/company_sister_concern_contacts/editContactPerson",
        {
          'id': form.id,
          'name': form.name,
          'phone_no_primary': form.phone_no_primary,
          'phone_no_secondary': form.phone_no_secondary,
          'rank': form.rank,
        },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )

      .then((response) => {
        Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        if (response.data.error === false) {
          props.updateState(form);
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (props.item) {
      const {
        id,
        name,
        phone_no_primary,
        phone_no_secondary,
        rank,
      } = props.item;
      setValues({ id, name, phone_no_primary, phone_no_secondary, rank });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false);

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Full Name</Label>
        <Input
          type="text"
          minLength="3"
          name="name"
          id="name"
          onChange={onChange}
          value={form.name === null ? "" : form.name}
          required
        />
      </FormGroup>

      <FormGroup>
        <Label for="phone_no_primary">Primary Phone Number</Label>
        <Input
          type="text"
          name="phone_no_primary"
          id="phone_no_primary"
          onChange={onChange}
          value={form.phone_no_primary === null ? "" : form.phone_no_primary}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="phone_no_secondary">Secondary Phone number</Label>
        <Input
          type="text"
          name="phone_no_secondary"
          id="phone_no_secondary"
          onChange={onChange}
          value={
            form.phone_no_secondary === null ? "" : form.phone_no_secondary
          }
          placeholder="ex. 555-555-5555"
          maxlength="11"
          minLength="11"
          // readOnly
        />
      </FormGroup>
      <FormGroup>
        <Label for="rank">Rank</Label>
        <Input
          type="text"
          name="rank"
          id="email"
          onChange={onChange}
          value={form.rank === null ? "" : form.rank}
          required
        />
      </FormGroup>

      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
