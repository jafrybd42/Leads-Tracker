/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const ClientRoutes = [
  {
    path: "/client/create",
    component: React.lazy(() => import("./createClient")),
    auth: authRoles.guest,
  },
  {
    path: "/client/edit",
    component: React.lazy(() => import("./clientManage")),
    auth: authRoles.guest,
  },
  {
    path: "/individual/:id",
    component: React.lazy(() => import("./individual/mdProfile")),
    auth: authRoles.guest,
  },
  {
    path: "/client/corporate",
    component: React.lazy(() => import("./corporate/sa")),
    auth: authRoles.guest,
  },
  {
    path: "/corporate/:id",
    component: React.lazy(() => import("./corporate/companyView")),
    auth: authRoles.guest,
  },
  {
    path: "/client/member/add/:id",
    component: React.lazy(() => import("./corporate/createClient")),
    auth: authRoles.guest,
  },
  {
    path: "/client/member/list/:id",
    component: React.lazy(() => import("./corporate/corporateClientDetails")),
    auth: authRoles.guest,
  },
  {
    path: "/client/list",
    component: React.lazy(() => import("./Filter")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
];

export default ClientRoutes;
