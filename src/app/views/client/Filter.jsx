import React, { Component } from "react";
import { Breadcrumb } from "matx";
import TaskFilter from "./taskFilter";

class Filter extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Client", path: "/client/list" },
              { name: "Client List" }
            ]}
          />
        </div>
        <TaskFilter />
      </div>
    );
  }
}

export default Filter;
