import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DataTable from "./DataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";
import Select from "react-select";
import { Button, Icon, Grid } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import { useLocation } from "react-router-dom";
import Swal from "sweetalert2";

function ClientManage(props) {
  const [name, setNames] = useState("");
  const [selectedClientTitle, setSelectedClientTitle] = useState("");
  const [selectedClientId, setSelectedClientId] = useState("");

  const [individualClientList, setIndividualClientList] = useState([]);
  const [items, setItems] = useState([]);
  const location = useLocation();
  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/client/list",
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((response) => {
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/client/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        // console.log(res);
        let client = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.name,
          };
        });

        setIndividualClientList(
          [
            {
              value: '',
              label: "Select All Individual Client",
            },
          ].concat(client)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const addItemToState = (item) => {
    setItems([...items, item]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);
  const onSelectClientChange = (label, value) => {
    if (label !== null) {
      setSelectedClientTitle(label.label);
      console.log(label.value)
      setSelectedClientId(label.value)
    }
    if (label === null) {
      setSelectedClientTitle("");
    }
  };
  const handleSubmit = () => {
    if (selectedClientTitle === "Select All Individual Client") {
      axios
        .get(myApi + "/intraco/lpg/client/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
          },
        })
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    } else {
      axios
        .post(
          myApi +
          "/intraco/lpg/client/" + selectedClientId + "/searchIndividualClient",
          {
            'name': selectedClientTitle,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
                icon: "error",
                title: "No Data Found",
                showConfirmButton: false,
                timer: 1000,
              });
          }
        });
    }
  };
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Individual Client", path: "/client/edit" },
                  { name: "Manage" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          <br></br>
          <Row>
            <Col>
              <div>
                <ValidatorForm onSubmit={handleSubmit}>
                  <Grid container spacing={6}>
                    <Grid
                      item
                      lg={6}
                      md={6}
                      sm={12}
                      xs={12}
                      style={{ maxWidth: "50%" }}
                    >
                      {
                        <Select
                          style={{
                            width: "100%",
                            padding: "5px",
                            backgroundColor: "white",
                            color: "#444",
                            borderBottomColor: "#000000",
                            textAlign: "left",
                            marginTop: "40px",
                          }}
                          isClearable="true"
                          name="selectedClientTitle"
                          options={individualClientList}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={onSelectClientChange}
                        />
                      }

                      <Button
                        onSubmit={handleSubmit}
                        color="primary"
                        variant="contained"
                        type="submit"
                        style={{ height: "30px", marginTop: "20px" }}
                      >
                        <Icon>send</Icon>
                        <span className="pl-2 capitalize">Search</span>
                      </Button>
                    </Grid>
                  </Grid>
                </ValidatorForm>
              </div>
            </Col>
          </Row>
          <br></br>
          <Row>
            <Col>
              {items && (
                <DataTable
                  items={items}
                  updateState={updateState}
                  deleteItemFromState={deleteItemFromState}
                />
              )}
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default ClientManage;
