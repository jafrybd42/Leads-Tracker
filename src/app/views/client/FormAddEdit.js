/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
function AddEditForm(props) {
  const [clientTypes, setClientTypes] = useState([]);

  const [item, setItem] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    name: "",
    primary_phone_no: "",
    secondary_phone_no: "",
    email: "",
    address: "",
    clientType: "",
    client_type_id: "",
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    let reqData = {
      id: form.id,
      name: form.name,
      primary_phone_no: form.primary_phone_no,
      secondary_phone_no: form.secondary_phone_no,
      address: form.address,
      client_type_id:
        form.client_type_id === 0 ? "" : Number(form.client_type_id),
      email: form.email,
    };

    if (reqData.email === "") {
      delete reqData.email;
    }

    axios
      .post(myApi + "/intraco/lpg/client/profile/update", reqData, {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })

      .then((response) => {
        props.toggle();

        if (response.data.error === false) {
          props.updateState(form);

        }
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
      })
      .catch((err) => console.log(err));

    if (form.primary_phone_no === form.secondary_phone_no) {
      Swal.fire("Primary and Secondary number can't be same !");
    }
  };

  useEffect(() => {
    // getting clientType
    axios
      .get(myApi + "/intraco/lpg/client_type/clientTypeListWithoutCorporate", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientTypes(
          [
            {
              id: "",
              type: "Select type",
            },
          ].concat(clientsFromApi)
        );
        //console.log(clientTypes)
      })
      .catch((error) => {
        //console.log(error);
      });

    if (props.item) {
      const {
        clientType,
        id,
        name,
        primary_phone_no,
        email,
        secondary_phone_no,
        address,
        client_type_id,
      } = props.item;
      setValues({
        clientType,
        id,
        name,
        primary_phone_no,
        email,
        secondary_phone_no,
        address,
        client_type_id,
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false);

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Full Name</Label>
        <Input
          type="text"
          name="name"
          id="name"
          onChange={onChange}
          value={form.name === null ? "" : form.name}
          minLength="3"
          required
        />
      </FormGroup>

      <FormGroup>
        <Label for="email">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          onChange={onChange}
          value={form.email === null ? "" : form.email}
        />
      </FormGroup>
      <FormGroup>
        <Label for="primary_phone_no">Primary Phone Number</Label>
        <Input
          type="text"
          name="primary_phone_no"
          id="primary_phone_no"
          onChange={onChange}
          value={form.primary_phone_no === null ? "" : form.primary_phone_no}
          placeholder="ex. 555-555-5555"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="secondary_phone_no">Secondary Phone Number</Label>
        <Input
          type="text"
          name="secondary_phone_no"
          id="secondary_phone_no"
          onChange={onChange}
          value={
            form.secondary_phone_no === null ? "" : form.secondary_phone_no
          }
          placeholder="Secondary Number"
        />
      </FormGroup>
      <FormGroup>
        <Label for="address">Address</Label>
        <Input
          type="text"
          name="address"
          id="address"
          onChange={onChange}
          value={form.address}
        />
      </FormGroup>
      <FormGroup>
        <Label for="clientType"> Type</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="client_type_id"
          value={form.client_type_id === null ? "" : form.client_type_id}
          onChange={onChange}
        >
          {clientTypes.map((ct) => (
            <option key={ct.id} value={ct.id}>
              {ct.type}
            </option>
          ))}
        </select>
      </FormGroup>
      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
