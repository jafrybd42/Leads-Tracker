import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";

import "date-fns";



class CreateForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      primary_phone_no: '',
      email: "",
      address: "",
      secondary_phone_no: "",
      clients: [],
      selectedClient: "",
    };
  }


  componentDidMount() {
    // getting clientTypeInformation
    axios.get(
      myApi + "/intraco/lpg/client_type/clientTypeListWithoutCorporate",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            type: client.type
          };
        });

        this.setState({
          clients: [
            {
              id: 0,
              type:
                "Select Client Type"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
      });
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
  }

  componentWillUnmount() {
    // remove rule when it is not needed

    ValidatorForm.addValidationRule("isNumberMatch", value => {
      if (value === this.state.primary_phone_no) {
        return false;
      }
      return true;
    });
  }

  handleSubmit = event => {

    if (this.state.primary_phone_no !== this.state.secondary_phone_no) {
      let reqdata = {
        'name': this.state.name,
        'primary_phone_no': this.state.primary_phone_no,
        'secondary_phone_no': this.state.secondary_phone_no,
        'email': this.state.email,
        'address': this.state.address,
        'sales_person_id': localStorageService.getItem('auth_user').userId,
        'client_type_id': parseInt(this.state.selectedClient)
      };

      if (reqdata.secondary_phone_no === "") {
        delete reqdata.secondary_phone_no;
      }
      axios.post(myApi + '/intraco/lpg/client/add', reqdata, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
        },
      })
        .then(res => {
          if (res.data.message) {
            if (!res.data.error) {
              Swal.fire({
                icon: "success",
                title: res.data.message,
                showConfirmButton: false,
              });
            } else {
              Swal.fire({
                icon: "error",
                title: res.data.message,
                showConfirmButton: false,
              });
            }

            if (res.data.error === false) {
              this.setState({
                name: '',
                primary_phone_no: '',
                email: '',
                address: '',
                secondary_phone_no: '',
                selectedClient: ''
              })
            }
          }
        })
    }

    if (this.state.primary_phone_no === this.state.secondary_phone_no) {
      Swal.fire("Primary & Secondery Number can't be Same")
    }
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {

    this.setState({ date });
  };

  render() {
    let {
      name,
      primary_phone_no,
      secondary_phone_no,
      email,
      address,

    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Full Name (Max length : 25)"
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={[
                  "required",
                  "maxStringLength: 25"
                ]}
                errorMessages={["this field is required"]}
              />

              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Address"
                onChange={this.handleChange}
                type="text"
                name="address"
                value={address}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Email"
                onChange={this.handleChange}
                type="email"
                name="email"
                value={email}
                errorMessages={["this field is required", "email is not valid"]}
              />
              <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedClient"
                value={this.state.selectedClient}
                onChange={this.handleChange}

              >
                {this.state.clients.map(client => (
                  <option
                    key={client.id}
                    value={client.id}
                  >
                    {client.type}
                  </option>
                ))}
              </select>



            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Primary Phone Number (11 Digit)"
                onChange={this.handleChange}
                type="Number"
                name="primary_phone_no"
                value={primary_phone_no}
                validators={[
                  "required",
                  'minNumber:0',
                  "minStringLength: 11",
                  "maxStringLength: 11"
                ]}
                errorMessages={["this field is required"]}
              />

              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Secondary Phone Number (11 Digit)"
                onChange={this.handleChange}
                type="Number"
                name="secondary_phone_no"
                value={secondary_phone_no}
                validators={[
                  'minNumber:0',

                ]}
              // // validators={["required", "isNumberMatch"]}
              // errorMessages={["this field is required"]}
              />
            </Grid>
          </Grid>
          <Button style={{ backgroundColor: '#007bff' }} color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div >
    );
  }
}



export default CreateForm;
