/* eslint-disable no-dupe-keys */

import React from "react";
import { authRoles } from "../../auth/authRoles";

const LeadsRoutes = [
  {
    path: "/lead/create",
    component: React.lazy(() => import("./createLeads")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/lead/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }
];

export default LeadsRoutes;
