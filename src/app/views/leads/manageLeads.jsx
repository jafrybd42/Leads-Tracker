import React, { Component } from "react";
import { Breadcrumb } from "matx";
import ManageForm from "./manageForm";
import { Card } from "@material-ui/core";

class ManageLeads extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Leads", path: "/lead/manage" },
              { name: "Manage Lead" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><ManageForm /></Card>
      </div>
    );
  }
}

export default ManageLeads;
