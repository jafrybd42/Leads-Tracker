/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'
import Swal from 'sweetalert2'
function AddEditForm(props) {

  const [modal, setModal] = useState(false)

  const toggle = () => {
    setModal(!modal)
  }

  // const [modal, setModal] = useState(true)
  const [item, setItem] = useState([])
  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    active_status: ''
  })

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()
    fetch('http://localhost:3000/crud', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: form.name,
        last: form.last,
        email: form.email,
        phone_number: form.phone_number,
        location: form.location,
        hobby: form.hobby
      })
    })
      .then(response => response.json())
      .then(item => {
        if (Array.isArray(item)) {
          props.addItemToState(item[0])
          props.toggle()
        } else {
          //console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  const submitFormEdit = e => {
    e.preventDefault()

    //console.log(`details`, form)
    axios.post(myApi + '/intraco/lpg/lead/update', {
      'id': form.id, 'name': form.name
    }, {

      "headers": {
        'x-access-token': localStorage.getItem("jwt_token")
      }
    })
      .then(response => {
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
             timer: 1500
          });
        }

        props.toggle()
        if (response.data.error === false) {
          props.updateState(form)
        }

      })

      .catch(err => console.log(err))
  }



  useEffect(() => {
    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, active_status } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, active_status })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Lead Title</Label>
        <Input type="text" name="name" id="name" onChange={onChange} value={form.name === null ? '' : form.name} minLength='3' required />
      </FormGroup>

      <Button>Submit</Button>
    </Form>
  )
}

export default AddEditForm