import React from "react";
import {
  Card,
  Icon,
  IconButton,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import axios from 'axios'
import myApi from "app/auth/api";
import localStorageService from "app/services/localStorageService";
const ManageForm = () => {
  let [leadsList, setLeadsList] = React.useState('');


  const fetchData = React.useCallback(() => {
    axios({
      "method": "GET",
      "url": myApi+"/intraco/lpg/lead/list",
      "headers": {

        "x-access-token": localStorageService.getItem('auth_user').token
      }
    })
      .then((response) => {
        
        setLeadsList(response.data.data)
      })
      .catch((error) => {
        //console.log(error)
      })
  }, [])
  React.useEffect(() => {
    fetchData()
  }, [fetchData])


return (

  <Card elevation={3} className="pt-5 mb-6">
    <div className="card-title px-6 mb-3">Client Type List</div>
    <div className="overflow-auto">


      <Table className="product-table">
        <TableHead>
          <TableRow>
            <TableCell className="px-6" colSpan={1}>
              ID
              </TableCell>
            <TableCell className="px-0" colSpan={3}>
              Name
              </TableCell>
           
            <TableCell className="px-0" colSpan={1}>
              Action
              </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
     
          {leadsList.length == 0
            ? 'Loading users...'
            : leadsList.map(user => (
              <TableRow key={user.id}>
                <TableCell className="px-0 capitalize" colSpan={1} align="left">
                  <div className="flex items-center">
                
                    {'#' + user.id}
                  </div>
                </TableCell>
                <TableCell className="px-0 capitalize" align="left" colSpan={3}>


                  {user.name}

                </TableCell>

              
                <TableCell className="px-0" colSpan={1}>
               
                  <IconButton>
                    <Icon color="primary">edit</Icon>
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>

    </div>
  </Card>
);
};
export default ManageForm;
