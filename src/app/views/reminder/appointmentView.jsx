import React from 'react';
import Moment from 'moment'

import { FlexibleWidthXYPlot } from 'react-vis';
function AppointmentView(props) {
  return (
    <div>
      <div className="modal-dialog">
         <div className="modal-content" style={{marginTop : '-35px'}}>
         

          <div className="modal-body">
            <div style={{ display: FlexibleWidthXYPlot }}>
              <div>
                <span><strong>Date : </strong></span>
                <span className="label label-warning">{Moment(props.item.appointmentDetails.map(e => e.appointment_date).toString()).format('YYYY-MM-DD')}</span>

              </div>

              <div style={{
                textAlign: "right",
                marginTop: "-20px"
              }}>
                <span><strong>Client : </strong></span>
                <span className="label label-warning">{props.item && props.item.appointmentDetails.map(e => e.client_info.map(d => d.name || d.company_title+'_'+d.title).toString())}</span>

              </div>
            </div>

            <hr />
            <center>
              <span><strong>Car number : </strong></span>
              <span className="label label-warning">{props.item && props.item.appointmentDetails.map(e => e.car_number).toString()}</span>

            </center>
            <hr />
            <center>
              <span><strong>Driver name : </strong></span>
              <span className="label label-warning">{props.item && props.item.appointmentDetails.map(e => e.driver_name).toString()}</span>

            </center>
            <hr />
            <center>
              <span><strong>Service Details: </strong></span>
              <span className="label label-warning">{props.item && props.item.appointmentDetails.map(e => e.service_details).toString()}</span>

            </center>
           
            <hr />
            <center>
              <span><strong>Status : </strong></span>
              <span className="label label-warning"> {props.item.appointmentDetails.map(e => e.status).toString() === "complete" ? (
                <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                  Completed
                </small>
              ) : (
                  <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                    Incomplete
                  </small>
                )}</span>

            </center>
          </div>

        </div>
      </div>
    </div>
  )
}

export default AppointmentView
