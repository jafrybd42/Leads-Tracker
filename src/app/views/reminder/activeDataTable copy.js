/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import {
  IconButton,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination,
  Tooltip,
} from "@material-ui/core";
import { Button } from "reactstrap";
import Moment from "moment";
import ModalForm from "./Modal";
import localStorageService from "app/services/localStorageService";
import history from "../../../history";
import axios from "axios";
import Axios from "axios";
import myApi from "app/auth/api";
import TaskModal from "./taskModal";

const ActiveDataTable = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [client_type_array, setClientType] = useState([]);
  // const [client_type_arrayy, setClientTypeArray] = useState([]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const items = props.items && props.items;

  useEffect(() => {
    // getting clientType
    Axios.get(myApi + "/intraco/lpg/client_type/list", {
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientType(
          [
            // {
            //   id: 0,
            //   type: "Corporate Client",
            // },
          ].concat(clientsFromApi)
        );
        // setClientTypeArray(
        //   [
        //     {
        //       value: 0,
        //       label: "Corporate Client",
        //       id: 0,
        //       type: "Corporate Client",
        //     },
        //   ].concat(clientsFromApi)
        // );
      })
      .catch((error) => {
        // console.log(error);
      });
  }, false);
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };

  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell
              align="center"
              className="px-0"
              style={{ width: "5%", textAlign: "center" }}
            >
              #
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "15%" }}>
              Start Date
            </TableCell>

            <TableCell align="center" className="px-0" style={{ width: "16%" }}>
              End Date
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "15%" }}>
              Client Type
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "20%" }}>
              Client
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "10%" }}>
              Details
            </TableCell>

            <TableCell align="center" className="px-0" style={{ width: "10%" }}>
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell className="px-0 capitalize" align="center">
                   {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {Moment(item.reminder_start_date).format("YYYY-MM-DD")}
                  </TableCell>

                  <TableCell className="px-0 capitalize" align="center">
                    {Moment(item.reminder_end_date).format("YYYY-MM-DD")}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {locateValueById(
                      client_type_array,
                      Number(
                        item.appointmentDetails
                          .map((e) => e.client_type_id)
                          .toString()
                      )
                    ) &&
                      locateValueById(
                        client_type_array,
                        Number(
                          item.appointmentDetails
                            .map((e) => e.client_type_id)
                            .toString()
                        )
                      ).type}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {Number(
                      item.appointmentDetails.map((d) => d.client_type_id)
                    ).toString() !== 0 && (
                      <a
                        onClick={() =>
                          history.push({
                            pathname:
                              "/individual/" +
                              Number(
                                item.appointmentDetails
                                  .map((d) => d.client_info.map((e) => e.id))
                                  .toString()
                              ),
                            state: item.appointmentDetails,
                          })
                        }
                      >
                        {" "}
                        <font
                          color="blue"
                          style={{ cursor: "pointer", fontWeight: "500" }}
                        >
                          {item.appointmentDetails
                            .map((d) => d.client_info.map((e) => e.name))
                            .toString()}
                        </font>
                      </a>
                    )}
                    {/* // : */}
                    {Number(
                      item.appointmentDetails
                        .map((d) => d.client_type_id)
                        .toString()
                    ) === 0 && (
                      <a
                        onClick={() =>
                          history.push({
                            pathname:
                              "/client/member/list/" +
                              Number(
                                item.appointmentDetails
                                  .map((d) => d.client_info.map((e) => e.id))
                                  .toString()
                              ),
                            state: Number(
                              item.appointmentDetails
                                .map((d) => d.client_info.map((e) => e.id))
                                .toString()
                            ),
                          })
                        }
                      >
                        {" "}
                        <font
                          color="blue"
                          style={{ cursor: "pointer", fontWeight: "500" }}
                        >
                          {item.appointmentDetails
                            .map((d) =>
                              d.client_info.map(
                                (e) => e.company_title + "_" + e.title
                              )
                            )
                            .toString()}{" "}
                        </font>
                      </a>
                    )}
                  </TableCell>
                  <TableCell
                    className="px-0"
                    style={{ width: "10%" }}
                    align="center"
                  >
                    <TaskModal
                      style={{ marginLeft: "-15px" }}
                      buttonLabel="View"
                      item={item}
                      updateState={props.updateState}
                    />
                  </TableCell>

                  {item.is_created_task === 0 && (
                    <TableCell
                      className="px-0"
                      style={{ width: "10%" }}
                      align="center"
                    >
                      {Moment(new Date()).format("YYYY-MM-DD") >
                        Moment(item.reminder_start_date).format("YYYY-MM-DD") &&
                      Moment(new Date()).format("YYYY-MM-DD") <
                        Moment(item.reminder_end_date).format("YYYY-MM-DD") ? (
                        <Tooltip title="Create Task" placement="right">
                          <Button
                            onClick={() =>
                              history.push({
                                pathname: "/reminder/edit/" + item.id,
                                state: item,
                              })
                            }
                            item={item}
                            color="btn"
                            style={{ marginTop: "5px", margin: "auto" }}
                          >
                            <span
                              class="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                              aria-hidden="true"
                            >
                              create
                            </span>
                          </Button>
                        </Tooltip>
                      ) : (
                        ""
                      )}
                    </TableCell>
                  )}
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items && props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default ActiveDataTable;
