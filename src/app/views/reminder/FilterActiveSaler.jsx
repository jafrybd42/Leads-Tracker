import { Breadcrumb } from "matx";

import AppointmentFilterSalesperson from "./Reminder";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import React, { useState, useEffect } from "react";
import localStorageService from "app/services/localStorageService";

function Sa(props) {
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios
      .get(myApi + "/intraco/lpg/reminders/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((response) => {
        console.log(response.data.data);
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Reminder", path: "/reminder/manage" },
                  { name: "Check Reminder" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>

          <br></br>
          <Row>
            {items?.length !== 0 ? (
              <Col>
                <AppointmentFilterSalesperson
                  items={items}
                  updateState={updateState}
                  deleteItemFromState={deleteItemFromState}
                />
              </Col>
            ) : (
              <Col style={{minHeight: '400px'}}>
                <AppointmentFilterSalesperson
                  items={items}
                  updateState={updateState}
                  deleteItemFromState={deleteItemFromState}
                />
              </Col>
            )}
          </Row>
          <br></br>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
