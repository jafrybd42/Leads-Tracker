/* eslint-disable no-unused-vars */
import React from "react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import { Container, Row, Col } from 'reactstrap'
import {

  Button,
  Icon,
  Grid,

} from "@material-ui/core";
import axios from "axios";
import Axios from "axios";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'
import { ValidatorForm } from "react-material-ui-form-validator";
import ActiveDataTable from "./activeDataTable";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Moment from 'moment'
import Select from 'react-select';
import Swal from "sweetalert2";
import { Fragment } from "react";

class AppointmentFilterSalesperson extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tableData: [],
      isSubmitted: false
    }

    const deleteItemFromState = id => {
      const updatedItems = this.state.tableData.filter(item => item.id !== id)
      this.setState({ tableData: updatedItems })
    }

  }

  state = {
    clients_corporate: [],

    selectedClientIndividual: '',
    selectedClientCorporate: '',
    selectedClientType: 1,
    client_type_array: [],
    client_type_arrayy: [],
    selectedVehicleIndividualNew: [],
    selectedDiscussionCorporateNew: [],
    prospectTypes: [],
    selectedProspectType: '',
    clients: [],
    selectedClient: "",
    sales: [],
    selectedSales: '',
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",

    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",

    },
    tableData: [],
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false

  };

  componentDidMount() {
    console.log(this.state.tableData)

    if (this.state.selectedClientType !== 1 || this.state.selectedClientType !== 0) {
      this.setState({
        selectedClientType: 1
      })
    }
    // getting clientType
    axios.get(
      myApi + "/intraco/lpg/client_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.type,
            id: client.id,
            type: client.type
          };
        });

        this.setState({
          client_type_array: []
            .concat(clientsFromApi)
        });
        this.setState({
          client_type_arrayy: [
            {
              value: 0,
              label: 'Corporate Client',
              id: 0,
              type: 'Corporate Client'
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    //Reminder List

    axios.get(myApi + '/intraco/lpg/reminders/list',
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token

        }
      })
      .then((response) => {
        // console.log(response.data.data)
        if (response.data.data.length !== 0) {
          this.setState({
            isSubmitted: true,
            tableData: Array.isArray(response.data.data) ? response.data.data : []
          })
        }

      })
      .catch((error) => {
        console.log(error)
      })

    // getting clientType
    Axios.get(myApi + "/intraco/lpg/client_type/list", {
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        this.setState({
          client_type_array: [
            {
              id: 0,
              type: "Corporate Client",
            },
          ].concat(clientsFromApi)
        });
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.name
          };
        });

        this.setState({
          clients: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/company_sister_concerns/allList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        console.log(res)
        let clientsFromApi = res.data.data && res.data.data.map(cl => {
          return {
            value: cl.id,
            label: cl.company + '_' + cl.sister_concern
          };
        });

        this.setState({
          clients_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });
    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(propspectType => {
          return {
            value: propspectType.id,
            label: propspectType.type
          };
        });

        this.setState({
          prospectTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/salesPerson/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.name
          };
        });

        this.setState({
          sales: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting discussionType
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            value: discussionType.id,
            label: discussionType.type
          };
        });

        this.setState({
          discussionTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting vehicleType
    axios.get(
      myApi + "/intraco/lpg/vehicle_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(VehicleType => {
          return {
            value: VehicleType.id,
            label: VehicleType.type
          };
        });

        this.setState({
          VehicleTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting call type
    axios.get(
      myApi + "/intraco/lpg/call_time/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(CallTimeType => {
          return {
            id: CallTimeType.id,
            type: CallTimeType.type
          };
        });

        this.setState({
          CallTimeTypes: [
            {
              id: '',
              type:
                "Select Call Time Type"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Leads
    axios.get(
      myApi + "/intraco/lpg/lead/activeList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data.map(Lead => {
          return {
            value: Lead.id,
            label: Lead.name
          };
        });

        this.setState({
          Leads: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

  }
  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  hundleDateChange(startDate, endDate) {
    this.setState(() => ({

      endDate,
      startDate,
    }));
    if (startDate != null) {
      this.setState(() => ({
        startDateFormatted: startDate.format("YYYY-MM-DD"),
      }));
    }
    if (startDate == null) {
      this.setState(() => ({
        startDateFormatted: '',
      }));
    }
    if (endDate != null) {
      this.setState(() => ({
        endDateFormatted: endDate.format("YYYY-MM-DD"),
      }));
    }
    if (endDate == null) {
      this.setState(() => ({
        endDateFormatted: '',
      }));
    }
  }

  handleSubmit = () => {
    let reqData = {
      "from_date": this.state.startDateFormatted || Moment(new Date()).format('YYYY-MM-DD'),
      "to_date": this.state.endDateFormatted || Moment(new Date()).format('YYYY-MM-DD')
    }
    if (reqData.to_date === '' || reqData.to_date === null || reqData.to_date === 'null') {
      delete reqData.to_date
    }
    if (reqData.from_date === '' || reqData.from_date === null || reqData.from_date === 'null') {
      delete reqData.from_date
    }
    axios.post(myApi + '/intraco/lpg/reminders/search', reqData, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        if (res.data.data) {
          console.log(res.data.data)
          if (res.data.data.length === 0) {
            this.setState({ isSubmitted: false })
            Swal.fire({
              icon: "error",
              title: "No Data Found",
              showConfirmButton: false,
            });
          }
          if (res.data.data.length !== 0) {
            this.setState({
              isSubmitted: true,
              tableData: Array.isArray(res.data.data) ? res.data.data : []

            })
          }

        }
      })
  }
  exportPDF = () => {
    const locateValueById = (types, id) => {
      let item = types.find((it) => it.id === Number(id));
      return item;
    };
    const addFooters = doc => {
      const pageCount = doc.internal.getNumberOfPages()
      var footer = new Image();
      footer.src = '/assets/footerPdf.png';

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i)
        doc.addImage(image, 'JPEG', pageWidth - 110, 0, 100, 100)
        doc.addImage(leftBar, 'JPEG', 0, 0, 16, 270)
        doc.addImage(footer, 'JPEG', 0, pageHeight - 60, pageWidth, 60)
      }
    }

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    doc.setFontSize(15);

    var leftBar = new Image();
    leftBar.src = '/assets/leftBar.png';
    var image = new Image();
    image.src = '/assets/logoNew.png';
    const title = "Appointment Report";
    const headers = [["SL", "Date", "Client Type", "Client", "Car Number", "Driver Name",
      "Service Details", "Status"]];

    const data = this.state.tableData.map((ap, i) => [i + 1, ap.appointmentDetails.map((e) => Moment(e.appointment_date).format("YYYY-MM-DD")), ap.appointmentDetails.map((e) => locateValueById(this.state.client_type_arrayy, e.client_type_id) &&
      locateValueById(this.state.client_type_arrayy, e.client_type_id).type), ap.appointmentDetails.map((e) => e.client_info.map(ef => ef.name || ef.company_title + "_" + ef.title)),
    ap.appointmentDetails.map((e) => e.car_number), ap.appointmentDetails.map((e) => e.driver_name), ap.appointmentDetails.map((e) => e.service_details), ap.appointmentDetails.map((e) => e.status)]);

    let content = {
      startY: 150,
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 150
      }
    };


    doc.setFont('helvetica', 'bold')
    doc.setFontSize(15)
    doc.text(title, marginLeft, 50);
    doc.setFontSize(10)
    const filterData = this.state.startDateFormatted && this.state.endDateFormatted && 'Date : ' + this.state.startDateFormatted + " > " + this.state.endDateFormatted
    this.state.startDateFormatted && this.state.endDateFormatted && doc.text(filterData, marginLeft, 70)


    doc.autoTable(content);

    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc)

    doc.save("appointment_report(" + Moment(new Date()).format('YYYY-MM-DD') + ").pdf")
  }

  onSelectChangeIndividualClient = value => {
    if (value !== null) {
      this.setState({ selectedClientIndividual: value.value });
      console.log(this.state.selectedClientIndividual)
    }
    if (value === null) {
      this.setState({ selectedClientIndividual: "" });
      console.log(this.state.selectedClientIndividual)
    }
  };
  onSelectChange = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedSalesNew: value.value });
      console.log(this.state.selectedSalesNew)
    }
    if (value === null) {
      this.setState({ selectedSalesNew: "" });
      console.log(this.state.selectedSalesNew)
    }
  };


  onSelectChangeDiscussionType = value => {
    if (value !== null) {
      this.setState({ selectedDiscussionType: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedDiscussionCorporateNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });

    }
    if (value === null) {
      this.setState({ selectedDiscussionType: "" });
      console.log(this.state.selectedDiscussionType)
    }
  };

  onSelectChangeVehicleType = value => {
    if (value !== null) {
      this.setState({ selectedVehicleType: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedVehicleIndividualNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });

      console.log(Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [])
      console.log(this.state.selectedVehicleIndividualNew)
    }
    if (value === null) {
      this.setState({ selectedVehicleType: "" });
      console.log(this.state.selectedVehicleTypeIndividual)
    }
  };

  onSelectChangeIndividualLeads = value => {
    if (value !== null) {
      this.setState({ selectedLead: value.value });
      console.log(this.state.selectedLead)
    }
    if (value === null) {
      this.setState({ selectedLead: "" });
      console.log(this.state.selectedLead)
    }
  };
  onSelectChangeIndividualProspectype = value => {
    if (value !== null) {
      this.setState({ selectedProspectType: value.value });
      console.log(this.state.selectedProspectType)
    }
    if (value === null) {
      this.setState({ selectedProspectType: "" });
      console.log(this.state.selectedProspectType)
    }
  };
  onSelectChangeClientType = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedClientType: value.value });
      console.log(this.state.selectedClientType)
    }
    if (value === null) {
      this.setState({ selectedClientType: "" });
      console.log(this.state.selectedClientType)
    }
  };
  onSelectChangeSaler = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedSales: value.value });
      console.log(this.state.selectedSales)
    }
    if (value === null) {
      this.setState({ selectedSales: "" });
      console.log(this.state.selectedSales)
    }
  };
  onSelectChangeCorporateClient = value => {
    if (value !== null) {
      this.setState({ selectedClientCorporate: value.value });
      console.log(this.state.selectedClientCorporate)
    }
    if (value === null) {
      this.setState({ selectedClientCorporate: "" });
      console.log(this.state.selectedClientCorporate)
    }
  };


  deleteItemFromState = id => {
    const updatedItems = this.state.tableData.filter(item => item.id !== id)
    this.setState({ tableData: updatedItems })
  }

  render() {
    return (
      <Fragment>
        <br></br>
        <div>
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}

          >
            {localStorageService.getItem('auth_user').role === 'SALES-PERSON' &&
              <Grid container spacing={6} >
                <Grid item lg={6} md={6} sm={12} xs={12}  >

                  <div style={{ marginBottom: '15px', marginTop: '-20px' }}><strong>
                    <font color='black'>Select Date  </font>
                  </strong></div>
                  <DateRangePicker

                    isOutsideRange={() => false}
                    startDate={this.state.startDate}
                    startDateId="start_date_id"
                    endDate={this.state.endDate}
                    endDateId="end_date_id"
                    onDatesChange={({ startDate, endDate }) =>
                      this.hundleDateChange(startDate, endDate)
                    }
                    focusedInput={this.state.focusedInput}
                    onFocusChange={(focusedInput) => this.setState({ focusedInput })}
                  />


                </Grid>

              </Grid>
            }

            <Button onSubmit={this.handleSubmit} color="primary" variant="contained" type="submit"
              style={{ marginTop: '30px', }}>
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Search</span>
            </Button>
            {'   '}
            {this.state.tableData.length !== 0 &&
              <button className="btn btn-primary" style={{ marginTop: '30px' }} onClick={() => this.exportPDF()}>Generate Report</button>
            }

          </ValidatorForm>
          <br></br><br></br>
          <br></br>
          {

            this.state.isSubmitted === true && this.state.tableData &&
            <ActiveDataTable items={this.state.tableData} updateState={this.state.updateState} deleteItemFromState={this.state.deleteItemFromState} />

          }
        </div>
      </Fragment>
    );
  }
}
export default AppointmentFilterSalesperson;