import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../../services/localStorageService"
import axios from 'axios'
import myApi from '../../../auth/api'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,
  Input

} from "@material-ui/core";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import Select from 'react-select';
import Moment from 'moment'

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";


class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      appointment_date: '',
      car_number: '',
      driver_name: '',
      service_details: '',
      leads_corporate: [],
      discussionTypes_corporate: [],
      clients_corporate: [],
      prospectTypes_corporate: [],
      vehicleTypes_corporate: [],
      clients: [],
      selectedClient: "",
      propspectTypes: [],
      selectedProspectType: '',
      discussionTypes: [],
      selectedDiscussionType: "",
      CallTimeTypes: [],
      selectedCallTimeType: "",
      VehicleTypes: [],
      selectedVehicleType: "",
      Leads: [],
      selectedLead: "",

      validationError: '',
      date: '',
      time: new Date(),
      newTime: Moment(new Date()).format('HH:mm'),


      client_type_array: [
        { value: 0, label: "Corporate" },
        { value: 1, label: "Individual" }
      ],

      selectedClientType: '',
      selectedClientCorporate: '',
      selectedProspectTypeCorporate: '',
      selectedVehicleTypeCorporate: '',
      selectedLeadCorporate: '',

      selectedDiscussionTypeIndividual: '',
      selectedLeadIndividual: '',
      selectedProspectTypeIndividual: '',
      selectedVehicleTypeIndividual: '',
      selectedClientIndividual: '',

      selectedVehicleIndividualNew: [],
      selectedDiscussionIndividualNew: [],
      selectedVehicleCorporateNew: [],
      selectedDiscussionCorporateNew: [],
      defaultVehicles: [],
      isSubmitted: false
    }
  }

  componentDidMount() {
    console.log(this.props)

    this.setState({
      date: Moment(this.props.location.state.appointment_date).format('YYYY-MM-DD'),
      car_number: this.props.location.state.car_number,
      driver_name: this.props.location.state.driver_name,
      service_details: this.props.location.state.service_details,
      selectedClientType: this.props.location.state.client_type_id,
      selectedClientIndividual: Number(this.props.location.state.client_info &&
        this.props.location.state.client_info.map((e) => e.id).toString()),
    })




    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.name
          };
        });

        this.setState({
          clients: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting discussionType
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            id: discussionType.id,
            type: discussionType.type
          };
        });

        this.setState({
          discussionTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(propspectType => {
          return {
            id: propspectType.id,
            type: propspectType.type,
            value: propspectType.id,
            label: propspectType.type,
          };
        });

        this.setState({
          propspectTypes: [

          ].concat(clientsFromApi)
        });

        this.setState({
          prospectTypes_corporate: [

          ].concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });


    // getting Leads
    axios.get(
      myApi + "/intraco/lpg/lead/activeList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data.map(Lead => {
          return {
            id: Lead.id,
            name: Lead.name,
            value: Lead.id,
            label: Lead.name
          };
        });

        this.setState({
          Leads: [

          ]
            .concat(clientsFromApi)
        });
        this.setState({
          leads_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/task/create/data",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data && res.data.data.corporateClient.map(cl => {
          return {
            value: cl.id,
            label: cl.company_title + '_' + cl.company_sister_concerns_title
          };
        });

        this.setState({
          clients_corporate: [
            // {
            //   value: '',
            //   label:
            //     "Select Client"
            // }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting discussionType corporate
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            value: discussionType.id,
            label: discussionType.type
          };
        });

        this.setState({
          discussionTypes_corporate: [
            // {
            //   value: '',
            //   label:
            //     "Select Discussion Type"
            // }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = e => {
    e.preventDefault();

    axios
      .post(
        myApi + "/intraco/lpg/reminders/create_task", {
        'reminder_id': this.props.location.state.id,
        'prospect_type_id': this.state.selectedDiscussionTypeIndividual,
        "lead_id": this.state.selectedClientIndividual,
        "call_date": this.state.date,
        "call_time": Moment(this.state.time).format('HH:mm') || Moment(new Date()).format('HH:mm'),
      }, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
        },
      })
      .then(res => {
        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          if (res.data.error === false) {
            this.setState({
              selectedDiscussionTypeIndividual: '',
              selectedDiscussionType: "",
              selectedCallTimeType: "",
              selectedVehicleType: "",
              selectedLead: "",
              selectedClientIndividual: ''
            })
          }
        }
      })
  }



  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });

  };

  handleDateChange = date => {


    date = Moment(date).format('YYYY-MM-DD')

    this.setState({ date });
  };

  handleTimeChange = (time) => {
    console.log({ time })
    this.setState(
      { time }
    );

    this.state.newTime = Moment(time).format('HH:mm')

  };

  onSelectChangeDays = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedClientType: value.value });
      console.log(this.state.selectedClientType)
    }
    if (value === null) {
      this.setState({ selectedClientType: "" });
      console.log(this.state.selectedClientType)
    }
  };

  onSelectChangeCorporateClient = value => {
    if (value !== null) {
      this.setState({ selectedClientCorporate: value.value });
      console.log(this.state.selectedClientCorporate)
    }
    if (value === null) {
      this.setState({ selectedClientCorporate: "" });
      console.log(this.state.selectedClientCorporate)
    }
  };


  onChange = (e) => {
    this.setState({

      [e.target.name]: e.target.value,
    });
  };
  //individual
  onSelectChangeIndividualClient = value => {
    if (value !== null) {
      this.setState({ selectedClientIndividual: value.value });
      console.log(this.state.selectedClientIndividual)
    }
    if (value === null) {
      this.setState({ selectedClientIndividual: "" });
      console.log(this.state.selectedClientIndividual)
    }
  };

  onSelectChangeIndividualDiscussionType = value => {
    if (value !== null) {
      // this.setState({ selectedDiscussionTypeIndividual: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedDiscussionTypeIndividual: value.value });
      this.setState({ selectedDiscussionIndividualNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });
    }
    if (value === null) {
      this.setState({ selectedDiscussionTypeIndividual: "" });
      console.log(this.state.selectedDiscussionTypeIndividual)
    }
  };




  render() {
    let { user } = this.props
    let {

      date,
      time

    } = this.state;
    return (
      <div>

        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: '0px' }}>
              <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                <font color='black'>Select Lead</font>
              </strong></div>

              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.leads_corporate.filter(option => option.value === this.state.selectedClientIndividual)}
                isClearable="true"
                name="selectedClientIndividual"
                options={this.state.leads_corporate}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeIndividualClient}
              />


              <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                <font color='black'>Select Prospect Type</font>
              </strong></div>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.propspectTypes.filter(option => option.value === this.state.selectedDiscussionTypeIndividual)}
                isClearable="true"
                name="selectedDiscussionTypeIndividual"
                options={this.state.propspectTypes}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeIndividualDiscussionType}
              />


            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <div style={{ marginBottom: '-10px', marginTop: '15px' }}><strong>
                <font color='black'>Select Date</font>
              </strong></div>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>

                <KeyboardDatePicker
                  style={{ marginTop: '20px' }}
                  className="mb-4 w-full"
                  margin="none"
                  id="mui-pickers-date"
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  value={date}
                  onChange={this.handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
              </MuiPickersUtilsProvider>

              <div style={{ marginBottom: '5px', marginTop: '30px' }}><strong>
                <font color='black'>Service Time </font>
              </strong></div>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardTimePicker
                  className="mb-4 w-full"
                  margin="normal"
                  id="time-picker"
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  value={time}
                  onChange={this.handleTimeChange}
                  KeyboardButtonProps={{
                    'aria-label': 'change time',
                  }}
                />
              </MuiPickersUtilsProvider>

            </Grid>
          </Grid>

          <Button style={{ backgroundColor: '#007bff' }} color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Update</span>
          </Button>

        </ValidatorForm>

      </div>
    );
  }
}



export default withRouter(CreateForm);
