import React from "react";
import myApi from "../../auth/api";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import history from "../../../history";
import TaskModal from "./taskModal";

import Moment from "moment";
import {
  Card,
  Icon,
  IconButton,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Tooltip
} from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
const Reminder = (props) => {
  const [ReminderList, setReminderList] = React.useState([]);

  const fetchData = React.useCallback(() => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/reminders/dashboardReminders",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);

        let responsedData = response.data.data;

        if (responsedData) {
          setReminderList(
            Array.isArray(response.data.data) ? response.data.data : []
          );
        }
        if (
          response.data.message &&
          response.data.message === "Timeout Login Fast"
        ) {
          props.logoutUser();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleCompleteReminder = (id) => {
  };
  return (
    <Card elevation={3} className="pt-5 mb-6">
      <div className="card-title px-6 mb-3">Reminder For Today</div>
      <div className="overflow-auto">
        <Table className="product-table">
          {ReminderList.length === 0 ? (
            <div
              className="alert alert-info"
              role="alert"
              style={{ width: "100%", background: "white", border: "white" }}
            >
              No more tasks for Today !
            </div>
          ) : (
            <TableHead>
              <TableRow>
                <TableCell align="left" className="px-6" colSpan={3}>
                  Start Date
                </TableCell>
                <TableCell align="center" className="px-0" colSpan={3}>
                  End Date
                </TableCell>
                <TableCell align="center" className="px-0" colSpan={2}>
                  Client Type
                </TableCell>
                <TableCell align="center" className="px-0" colSpan={2}>
                  Client
                </TableCell>
                <TableCell align="center" className="px-0" colSpan={2}>
                  Details
                </TableCell>
                <TableCell align="center" className="px-0" colSpan={2}>
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
          )}
          {
            ReminderList.map((item) => (
              <TableBody>
                <TableRow key={item.id}>
                  <TableCell
                    className="px-0 capitalize"
                    colSpan={3}
                    align="left"
                  >
                    <div className="flex items-center">
                      <img
                        className="circular-image-small"
                        src="/assets/task.png"
                        alt="task"
                      />
                      <p className="m-0 ml-8">
                        {Moment(item.reminder_start_date).format("YYYY-MM-DD")}
                      </p>
                    </div>
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    colSpan={3}
                  >
                    {Moment(item.reminder_end_date).format("YYYY-MM-DD")}
                  </TableCell>

                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    colSpan={2}
                  >
                    {item.appointmentDetails.map((e) =>
                      e.client_type_id === 0 ? "Corporate" : "Individual"
                    )}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    colSpan={2}
                  >
                    {item.appointmentDetails.map((e) =>
                      e.client_info.map(
                        (d) => d.name || d.company_title + "_" + d.title
                      )
                    )}
                  </TableCell>

                  <TableCell className="px-0" align="center" colSpan={2}>
                  <TaskModal
                      style={{ marginLeft: "-15px" }}
                      buttonLabel="View"
                      item={item}
                      updateState={props.updateState}
                    />
                  </TableCell>
                  <TableCell className="px-0" align="center" colSpan={2}>
                    {item.is_created_task === 0 && (
                      <Tooltip
                      title="Create Task"
                      placement="right"
                    >
                      <Button
                        onClick={() =>
                          history.push({
                            pathname: "/reminder/edit/" + item.id,
                            state: item,
                          })
                        }
                        item={item}
                        color="btn"
                      >
                        <span
                          class="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                          aria-hidden="true"
                          // title="create"
                        >
                          create
                        </span>
                      </Button>
                      </Tooltip>
                    )}
                  </TableCell>
                </TableRow>
              </TableBody>
            ))
          }
        </Table>
      </div>
    </Card>
  );
};

Reminder.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(connect(mapStateToProps, { logoutUser })(Reminder));
