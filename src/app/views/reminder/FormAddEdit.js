import React, { useState, useEffect } from "react";
import 'date-fns';

import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
} from '@material-ui/pickers';
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
import Moment from "moment";

function AddEditForm(props) {
  const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

  const [leadTypes, setLeadTypes] = useState([]);
  const [prospectTypes, setProspectTypes] = useState([]);
  const [form, setValues] = useState({
    id: props.item.id,
    call_time: new Date(),
    lead_id: '',
    prospect_type_id: '',
    call_date: new Date()
  });
  const handleDateChange = (date) => {
    setSelectedDate(date)
    console.log(selectedDate)
    console.log(Moment(selectedDate).format('HH:mm'))
  };
  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    console.log(form)
    e.preventDefault();

    axios
      .post(
        myApi + "/intraco/lpg/reminders/create_task",
        {
          "reminder_id": form.id,
          "call_time": Moment(selectedDate).format('HH:mm') || Moment(new Date()).format('HH:mm'),
          "lead_id": form.lead_id,
          "prospect_type_id": form.prospect_type_id,
          "call_date": Moment(form.call_date).format('YYYY-MM-DD') || Moment(new Date()).format('YYYY-MM-DD')
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((response) => {
        // props.toggle();
        Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        if (response.data.error === false) {
          props.updateState(form);
        }
      })
   
  };

  useEffect(() => {
    // getting Leads
    axios
      .get(myApi + "/intraco/lpg/lead/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            name: client.name,
          };
        });

        setLeadTypes(
          [
            {
              id: "",
              name: "Select if you want to change",
            },
          ].concat(clientsFromApi)
        );
        // console.log(clientTypes);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting discussionType
    axios
      .get(myApi + "/intraco/lpg/prospect_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setProspectTypes(
          [
            {
              id: "",
              type: "Select if you want to change",
            },
          ].concat(clientsFromApi)
        );
        // console.log(clientTypes);
      })
      .catch((error) => {
        console.log(error);
      });
    console.log(props)



  }, false);

  return (
    <Form onSubmit={submitFormEdit}>
      <FormGroup>
        <Label for="client_id">Select Call Time</Label>

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          {/* <Grid container justify="space-around"> */}

          <KeyboardTimePicker
            style={{
              width: "100%",
              padding: "5px",
              backgroundColor: "white",
              color: "#444",
              borderBottomColor: "#949494",
              textAlign: "left",
              marginTop: "10px",
            }}
            margin="normal"
            id="time-picker"
            label=""
            value={selectedDate}
            onChange={handleDateChange}
            KeyboardButtonProps={{
              'aria-label': 'change time',
            }}
          />
          {/* </Grid> */}
        </MuiPickersUtilsProvider>

      </FormGroup>
      <FormGroup>
        <Label for="selectedDiscussionType">Select Lead </Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="lead_id"
          value={form.lead_id === null ? "" : form.lead_id}
          onChange={onChange}
        >
          {leadTypes.map((client) => (
            <option key={client.id} value={client.id}>
              {client.name}
            </option>
          ))}
        </select>
      </FormGroup>

      <FormGroup>
        <Label for="email">Prospect Type</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="prospect_type_id"
          value={form.prospect_type_id === null ? "" : form.prospect_type_id}
          onChange={onChange}
        >
          {prospectTypes.map((client) => (
            <option key={client.id} value={client.id}>
              {client.type}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <Label for="phone_number">Car Service</Label>
        <Input
          type="text"
          name="service_details"
          id="service_details"
          onChange={onChange}
          value={form.service_details === null ? "" : form.service_details}
          placeholder="ex. Parts / Tier Change"
          required
        />
      </FormGroup>

      <FormGroup>
        <Label for="address">Date</Label>
        <Input
          type="date"
          name="call_date"
          id="call_date"
          onChange={onChange}
          value={
            form.call_date === null
              ? ''
              : Moment(form.call_date).format("YYYY-MM-DD")
          }
        />
      </FormGroup>
      <Button>Create</Button>
    </Form>
  );
}

export default AddEditForm;
