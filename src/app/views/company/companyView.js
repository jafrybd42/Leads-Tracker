/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable no-unused-vars */
/* eslint-disable no-lone-blocks */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import Datatable from "./DataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Button, Icon, Grid } from "@material-ui/core";
import Select from "react-select";
import { ValidatorForm } from "react-material-ui-form-validator";
import localStorageService from "app/services/localStorageService";
import { useLocation } from "react-router-dom";
import Swal from "sweetalert2";
function Sa(props) {
  const [name, setNames] = useState("");
  const [selectedSisterConcern, setSelectedSisterConcern] = useState("");
  const [sisterConcernList, setSisterConcernList] = useState([]);
  const [items, setItems] = useState([]);
  const location = useLocation();
  const getItems = () => {
    console.log(location.state);
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/company/details/" + location.state,
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);
        {
          response.data.data && setItems(Array.isArray(response.data.data) ? response.data.data : []);
          setSisterConcernList(Array.isArray(response.data.data) ? response.data.data.map(e => ({
            "value": e.id,
            "label": e.title

          })) : [])
        }
      })
      .catch((error) => {
        console.log(error);
      });

  };
  const addItemToState = (item) => {
    setItems([...items, item]);
    setNames([...name, name]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectChangeSisterConcern = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }
  };

  const handleSubmit = () => {
    if (selectedSisterConcern === "Select All Sister Concern") {
      axios
        .get(myApi + "/intraco/lpg/company/details/" + location.state, {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
          },
        })
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    } else {
      axios
        .post(
          myApi +
          "/intraco/lpg/company_sister_concerns/" +
          location.state +
          "/searchSisterConcern",
          {
            title: selectedSisterConcern,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
                icon: "error",
                title: "No Data Found",
                showConfirmButton: false,
                timer: 1000,
              });
          }
        });
    }
  };

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Company", path: "/company/edit" },
                  { name: "Sister Concern View" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>

          <br></br>
          <Row>
            <Col>
              <div>
                <ValidatorForm onSubmit={handleSubmit}>
                  <Grid container spacing={6}>
                    <Grid
                      item
                      lg={6}
                      md={6}
                      sm={12}
                      xs={12}
                    >
                      {
                        <Select
                          style={{
                            width: "100%",
                            padding: "5px",
                            backgroundColor: "white",
                            color: "#444",
                            borderBottomColor: "#000000",
                            textAlign: "left",
                            marginTop: "40px",
                          }}
                          isClearable="true"
                          name="selectedSisterConcern"
                          options={sisterConcernList}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={onSelectChangeSisterConcern}
                        />
                      }

                      <Button
                        onSubmit={handleSubmit}
                        color="primary"
                        variant="contained"
                        type="submit"
                        style={{ height: "30px", marginTop: "20px" }}
                      >
                        <Icon>send</Icon>
                        <span className="pl-2 capitalize">Search</span>
                      </Button>
                    </Grid>
                  </Grid>
                </ValidatorForm>
              </div>
            </Col>
          </Row>
          <br></br>
          <Row>
            <Col>
              <Datatable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
// }

export default Sa;
