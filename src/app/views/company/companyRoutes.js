/* eslint-disable no-dupe-keys */

import React from "react";
import { authRoles } from "../../auth/authRoles";

const CompanyRoutes = [
  {
    path: "/company/create",
    component: React.lazy(() => import("./createCompany")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
  {
    path: "/company/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
  {
    path: "/company/:id",
    component: React.lazy(() => import("./companyView")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
  {
    path: "/search",
    component: React.lazy(() => import("./corporateListSaler")),
    auth: authRoles.guest,
  },
  {
    path: "/search_company/:id",
    component: React.lazy(() => import("./saler/companyView")),
    auth: authRoles.guest,
  },
];

export default CompanyRoutes;
