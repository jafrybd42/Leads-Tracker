import React from "react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import {

  Button,
  Icon,
  Grid,

} from "@material-ui/core";
import axios from "axios";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DeactiveDataTable from "./newTable";
import Select from 'react-select';

class TaskFilter extends React.Component {
  constructor(props) {
    super(props)
    const tableData = this.state.tableData

  }
  state = {
    selectedCompany: '',
    company: [],
    clients: [],
    selectedClient: "",
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",
    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",

    },
    tableData: [],
    SalesPersons: [],
    selectedSalesPerson: '',
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false
  };

  componentDidMount() {
    axios({
      "method": "GET",
      "url": myApi + "/intraco/lpg/company/allList",
      "headers": {
        'x-access-token': localStorageService.getItem('auth_user').token
      }
    })
      .then((response) => {

        this.setState({
          isSubmitted: true,
          tableData: response.data.data
        })

      })
      .catch((error) => {
        console.log(error)
      })


    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/company/allList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let companies = res.data.data.map(c => {
          return {
            value: c.id,
            label: c.title
          };
        });

        this.setState({
          company: [
            {
              value: 0,
              label:
                "Select Company"
            }
          ]
            .concat(companies)
        });
      })
      .catch(error => {
        console.log(error);
      });

  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  hundleDateChange(startDate, endDate) {

    this.setState(() => ({

      endDate,
      startDate,
    }));
    if (startDate != null) {
      this.setState(() => ({
        startDateFormatted: startDate.format("YYYY-MM-DD"),
      }));
    }
    if (startDate == null) {
      this.setState(() => ({
        startDateFormatted: '',
      }));
    }
    if (endDate != null) {
      this.setState(() => ({
        endDateFormatted: endDate.format("YYYY-MM-DD"),
      }));
    }
    if (endDate == null) {
      this.setState(() => ({
        endDateFormatted: '',
      }));
    }
  }

  handleSubmit = () => {

    axios.post(myApi + '/intraco/lpg/company/search', {

      "title": this.state.selectedCompany,

    }, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {

        if (res.data.data) {

          this.setState({
            isSubmitted: true,
            tableData : Array.isArray(res.data.data)?res.data.data:[]

          })


        }
      })
  }


  onSelectChange = label => {
    // this.toggleOpen();
    if (label !== null) {
      this.setState({ selectedCompany: label.label });
      console.log(this.state.selectedCompany)
    }
    if (label === null) {
      this.setState({ selectedCompany: "" });
      console.log(this.state.selectedCompany)
    }
  };


  render() {
    return (
      <>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
        >
          <Grid container spacing={6} >
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '33%' }}>
              {
                <Select
                  style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  isClearable="true"
                  name="selectedCompany"
                  options={this.state.company}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChange}
                />

              }

              <Button onSubmit={this.handleSubmit} color="primary" variant="contained" type="submit"
                style={{ height: '30px', marginTop: '20px' }}>
                <Icon>send</Icon>
                <span className="pl-2 capitalize">Search</span>
              </Button>
            </Grid>
          </Grid>
        </ValidatorForm>

        {
          this.state.isSubmitted && this.state.tableData &&
          <DeactiveDataTable items={this.state.tableData} />
        }
      </>
    );
  }
}
export default TaskFilter;