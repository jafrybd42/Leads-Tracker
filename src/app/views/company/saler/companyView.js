/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable no-lone-blocks */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import Datatable from "./DataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../../auth/api";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Button, Icon, Grid } from "@material-ui/core";
import Select from "react-select";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "app/services/localStorageService";
import { useLocation } from "react-router-dom";
import Swal from "sweetalert2";
function Sa(props) {
  const [name, setNames] = useState("");
  // eslint-disable-next-line no-unused-vars
  const [selectedSisterConcern, setSelectedSisterConcern] = useState("");
  const [sisterConcernList, setSisterConcernList] = useState([]);
  const [items, setItems] = useState([]);
  const location = useLocation();
  const [selectedCompany, setSelectedCompany] = useState("");

  const getItems = () => {
    axios
      .get(
        myApi +
        "/intraco/lpg/company_sister_concerns/" +
        location.state.id +
        "/allSisterConcernListForGlobalSearch",
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
          },
        }
      )
      .then((response) => {
        // console.log(response.data.data);
        {
          response.data.data &&
            setItems(
              Array.isArray(response.data.data) ? response.data.data : []
            );
          setSisterConcernList(
            Array.isArray(response.data.data)
              ? response.data.data.map((e) => ({
                value: e.id,
                label: e.title,
              }))
              : []
          );
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
    setNames([...name, name]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  const onSelectChangeSisterConcern = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }
  };

  const handleSubmit = () => {
    axios
      .post(
        myApi +
        "/intraco/lpg/company_sister_concerns/" +
        location.state.id +
        "/searchSisterConcern",
        {
          title: selectedCompany,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
          },
        }
      )
      .then((res) => {
        if (res.data.data) {
          setItems(res.data.data);
        }
        if (res.data.count === 0) {
          Swal.fire({
            icon: "error",
            title: "No Data Found",
            showConfirmButton: false,
          });
        }
      });
    // }
  };
  const handleChange = event => {
    event.persist();
    setSelectedCompany(event.target.value)
  };

  const handleGetAllData = event => {
    event.persist();
    axios
      .get(myApi + "/intraco/lpg/company/details/" + location.state.id, {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
        },
      })
      .then((res) => {
        if (res.data.data) {
          setItems(res.data.data);
        }
      });
    setSelectedCompany('')
  };
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Company", path: "/search" },
                  { name: location.state.title },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>

          <br></br>
          <Row>
            <Col>
              <div>
                <ValidatorForm onSubmit={handleSubmit}>
                  <Grid container spacing={6}>
                    <Grid
                      item
                      lg={6}
                      md={6}
                      sm={12}
                      xs={12}
                    >
                      {
                        <TextValidator
                          autoComplete='off'
                          className="mb-4 w-full"
                          label="Search here"
                          onChange={handleChange}
                          type="text"
                          name="selectedCompany"
                          value={selectedCompany}
                        />
                      }

                      <div className="flex items-center"> <Button
                        onSubmit={handleSubmit}
                        color="primary"
                        variant="contained"
                        type="submit"
                        style={{ height: "30px", marginTop: "0px" }}
                      >
                        <Icon>send</Icon>
                        <span className="pl-2 capitalize">Search</span>
                      </Button>
                        <span className="ml-4 mr-2">or</span>
                        <Button
                          className="btn btn-light capitalize"
                          onClick={handleGetAllData}
                        >
                          Get All Data
                      </Button>
                      </div>
                    </Grid>
                  </Grid>
                </ValidatorForm>
              </div>
            </Col>
          </Row>
          <br></br>
          <Row>
            <Col>
              <Datatable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
