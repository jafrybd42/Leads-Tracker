/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Table } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService';
import {
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination
} from "@material-ui/core";
import SisterConcernHistory from "../sisterConcern/sisterconcernHistory"
import ModalHistory from "../sisterConcern/ModalHistory"
function CompanyDetails(props) {
  const [CompanyDetails, setCompanyDetails] = useState([])

  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    image: '',

  })
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

  }


  useEffect(() => {

    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, image } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, image })

      //console.log('id', props.item.id)

      axios.get(myApi + '/intraco/lpg/company/details/' + props.item.id, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {
        console.log(response.data.data)

        if (response.data.data) {
          setCompanyDetails(response.data.data)
        }
      })

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  const locateValueById = (types, id) => {
    let item = types.find(it => it.id === Number(id))
    return item
  }

  return (

    <div>
      <div className="modal-dialog" style={{ maxWidth: '95%' }}>
         <div className="modal-content" style={{marginTop : '-35px'}}>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" />
            {CompanyDetails.length !== 0 && <h4 className="modal-title capitalize" id="myModalLabel">More About <font color='green'>{props.item.title}</font></h4>}
          </div>

          {CompanyDetails.length === 0 &&
            <div className="modal-body">
              <div className="alert alert-primary" role="alert">
                Sorry, There is no details available for <font color='green'>{props.item.title}</font>
              </div>
            </div>}

          {CompanyDetails.length !== 0 &&
            <div className="modal-body">
              <Table responsive hover>

                <TableHead>
                  <TableRow>
                    <TableCell className="px-0" style={{width : '5%'}}>#</TableCell>
                    <TableCell className="px-0">Sister Concern</TableCell>
                    <TableCell className="px-0">Sales Person</TableCell>
                    <TableCell className="px-0">Contact</TableCell>
                    <TableCell className="px-0">View</TableCell>

                  </TableRow>
                </TableHead>
                <TableBody>

                  {CompanyDetails
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((item, id) => (
                      <TableRow key={id} >
                        <TableCell align="left" className="px-0" style={{width : '5%'}}>
                         {page * rowsPerPage + id + 1}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {item.title}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {item.salesperson_id === null ? <font color="red">N/A</font> :

                            item.sales_person_name}


                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {item.contact_number === null ? <font color="red">N/A</font> :

                            item.contact_number}


                        </TableCell>
                        <TableCell className="px-0 capitalize">
                          {item.salesperson_id === null ? <font color="red">N/A</font> :
                            <ModalHistory
                              style={{ maxWidth: "700px!important" }}
                              buttonLabel="View"
                              item={item}
                            />
                          }
                        </TableCell>

                      </TableRow>
                    ))}
                </TableBody>
              </Table>
              {CompanyDetails.length !== 0 && <TablePagination
                className="px-4"
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={CompanyDetails && CompanyDetails.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "Previous Page"
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page"
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />}
            </div>
          }

        </div>

      </div>
    </div>
  )
}

export default CompanyDetails
