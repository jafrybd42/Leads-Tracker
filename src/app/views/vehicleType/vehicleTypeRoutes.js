/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const VehicleTypeRoutes = [
  {
    path: "/vehicleType/create",
    component: React.lazy(() => import("./createVehicleType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/vehicleType/manage",
    component: React.lazy(() => import("./manageProspectType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/vehicleType/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }
];

export default VehicleTypeRoutes;
