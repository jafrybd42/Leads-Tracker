import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import TaskView from "./appointmentView";

function TaskModal(props) {
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  const label = props.buttonLabel;

  let button = "";
  let title = "";

  if (label === "Edit") {
    button = (
      <Button
        color="warning"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        {label}
      </Button>
    );
    title = "Edit Profile";
  } else {
    button = (
      <Button
        color="btn"
        onClick={toggle}
        style={{
          float: "left",
          height: "40px",
          marginTop: "0px",
          // borderColor: "gray",
          marginLeft: "-5px",
          // margin:' auto!important'
        }}
      >
        <img src="/assets/view.png" />
      </Button>
      
    );
    title = "View Details";
  }

  return (
    <div style={{ margin: "auto", marginLeft: "37%" }}>
      {button}
      <Modal isOpen={modal} toggle={toggle} className={props.className} >
        <ModalHeader toggle={toggle} close={closeBtn}>
          {title}
        </ModalHeader>
        <ModalBody style={{ width: "110%!important" }}>
          <TaskView
            viewState={props.viewState}
            toggle={toggle}
            item={props.item}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default TaskModal;
