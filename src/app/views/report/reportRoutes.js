import React from "react";
import { authRoles } from "../../auth/authRoles";

const ReportRoutes = [

  {
    path: "/report/appointment",
    component: React.lazy(() => import("./Filter")),
    auth: authRoles.md
  },
  {
    path: "/report/task",
    component: React.lazy(() => import("./task")),
    auth: authRoles.md
  }
];

export default ReportRoutes;
