import React, { Component, Fragment } from "react";
import { Grid, Card } from "@material-ui/core";
import UpgradeCard from "../dashboard/shared/UpgradeCard";
import { withStyles } from "@material-ui/styles";
import localStorageService from 'app/services/localStorageService'
import Axios from "axios";
import myApi from "app/auth/api";



class SalesPersonProfileForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      phone: '',
      email: '',
      photo: '',
      question: ''
    }
  }
  componentDidMount() {
    //getting profile data

    Axios.get(myApi + '/intraco/lpg/salesPerson/profile/view/' + parseInt(localStorageService.getItem('auth_user').userId), {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token
      }
    }).then(response => {
      this.setState({
        name: response.data.data.name,
        phone: response.data.data.phone_number,
        email: response.data.data.email,
        photo: response.data.data.image,
      })
    })

      .catch(err => console.log(err))
  }

  render() {

    return (
      <Fragment>
        <div className="analytics m-sm-30 " style={{ marginTop: '35px !important' }}>
          <Grid container spacing={3}>
            <Grid item lg={8} md={8} sm={12} xs={12}>

              <table style={{ marginTop: '90px', width: '60%', height: '60%', textAlign: 'justify' }}>
                <tbody><tr>
                  <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Full Name</td>
                  <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                  <td>{this.state.name}</td>
                </tr>
                  <tr>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Email</td>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                    <td>{this.state.email}</td>
                  </tr>
                  <tr>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Phone</td>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                    <td>{this.state.phone}</td>
                  </tr>
                  <tr>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Position</td>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                    <td><strong>{localStorageService.getItem('auth_user').role}</strong></td>
                  </tr>
                </tbody></table>


            </Grid>

            <Grid item lg={4} md={4} sm={12} xs={12}>
              <Card className="px-6 py-4 mb-6">

                {localStorageService.getItem('auth_user') && <img src={localStorageService.getItem('auth_user').photoURL} style={{ maxWidth: '80%' }} alt='' />}

              </Card>

              <UpgradeCard />

            </Grid>
          </Grid>
        </div>
      </Fragment>
    );
  }
}

export default withStyles({}, { withTheme: true })(SalesPersonProfileForm);;
