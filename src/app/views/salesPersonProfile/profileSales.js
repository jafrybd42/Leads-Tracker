/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const ProfileSales = [
  {
    path: "/account/profile",
    component: React.lazy(() => import("./salesPersonProfile")),
    auth: authRoles.guest
  }
  
];

export default ProfileSales;
