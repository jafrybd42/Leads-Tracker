import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SalesPersonProfileFormOther from "./salesPersonProfileFormOther";

class SalesPersonProfileOther extends Component {
 
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/account/profile/:id" },
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SalesPersonProfileFormOther /></Card>
      </div>
    );
  }
}

export default SalesPersonProfileOther;
