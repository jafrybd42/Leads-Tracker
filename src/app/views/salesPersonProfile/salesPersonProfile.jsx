import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SalesPersonProfileForm from "./salesPersonProfileForm";

class SalesPersonProfile extends Component {
 
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/account/profile" },
              { name: JSON.parse(localStorage.getItem('auth_user')).displayName }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SalesPersonProfileForm /></Card>
      </div>
    );
  }
}

export default SalesPersonProfile;
