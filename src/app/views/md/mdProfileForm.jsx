import React, { Component, Fragment } from "react";
import {
  Card,
  Grid,
  Button,
} from "@material-ui/core";

import UpgradeCard from "../dashboard/shared/UpgradeCard";
import { withStyles } from "@material-ui/styles";
import localStorageService from 'app/services/localStorageService'
import Axios from "axios";
import myApi from "app/auth/api";
import Swal from "sweetalert2";


class MDProfileForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      image: '',
      phone: '',
      role: '',
    }
  }

  componentDidMount() {
    Axios.get(myApi + '/mdSir/profile/view/1', {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token
      }
    }).then((response) => {

      this.setState({
        name: response.data.data.name,
        email: response.data.data.email,
        image: response.data.data.image,
        phone: response.data.data.phone_number,
        role: 'Managing Director',
      })
    })
      .catch((error) => {
        //console.log(error)
      })
  }
  reset = event => {
    event.persist();

    Swal.fire({
      // style={{margin}}
      title: "Set a new PIN",
      // text: "Type a new pin here ...",
      input: 'text',
      showCancelButton: true
    }).then((result) => {
      if (result.value) {
        Axios.post(myApi + '/mdSir/profile/pinChange', { 'id': 1, 'new_pin': result.value }, {

          "headers": {
            'x-access-token': localStorageService.getItem("auth_user").token
          }
        })
          .then(response => {
            if (response.data.error === false) {
              if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
            }
          })

          .catch(err => console.log(err))
      }

    })
  }

  render() {

    return (
      <Fragment>
        <div className="analytics m-sm-30 " style={{ marginTop: '35px !important' }}>
          <Grid container spacing={3}>
            <Grid item lg={8} md={8} sm={12} xs={12}>

              <table style={{ marginTop: '90px', width: '60%', height: '60%', textAlign: 'justify' }}>
                <tbody><tr style={{ marginTop: '50px' }}>
                  <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Full Name</td>
                  <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                  <td>{this.state.name}</td>
                </tr>
                  <tr>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Email</td>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                    <td>{this.state.email}</td>
                  </tr>
                  <tr>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Phone</td>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                    <td>{this.state.phone}</td>
                  </tr>
                  <tr>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>Position</td>
                    <td style={{ color: '#1a71b5', fontWeight: '500', width: '30%' }}>:</td>
                    <td><strong>{this.state.role}</strong></td>
                  </tr>
                </tbody></table>


            </Grid>

            <Grid item lg={4} md={4} sm={12} xs={12}>
              <Card className="px-6 py-4 mb-6">

                <img src={'/profile/' + this.state.image} />
                {''}
                <Button
                  onClick={this.reset}
                  variant="contained"
                  color="primary"
                  // disabled={this.props.login.loading}
                  type="submit"
                  style={{ display: 'block', margin: 'auto' }}
                >  Reset PIN </Button>

              </Card>

              <UpgradeCard />

            </Grid>
          </Grid>
        </div>
      </Fragment>
    );
  }
}

export default withStyles({}, { withTheme: true })(MDProfileForm);;
