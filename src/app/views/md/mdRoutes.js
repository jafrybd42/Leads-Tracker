/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const MDRoutes = [
  {
    path: "/profile/MD/view",
    component: React.lazy(() => import("./mdProfile")),
    auth: authRoles.admin,
    auth: authRoles.md,
    auth: authRoles.editor
    // auth: authRoles.editor
  }
  // ,
  // {
  //   path: "/profile/MD/reset",
  //   component: React.lazy(() => import("./resetPassForm")),
  //   auth: authRoles.admin,
  //   auth: authRoles.md,
  //   auth: authRoles.editor
  //   // auth: authRoles.editor
  // }
];

export default MDRoutes;
