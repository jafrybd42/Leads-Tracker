/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Table } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
// import history from '../../../history'
// import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
import moment from 'moment'
import {
  IconButton,
  // Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination
} from "@material-ui/core";
function CompanyDetails(props) {
  // const [item, setItem] = useState([])
  const [CompanyDetails, setCompanyDetails] = useState([])
  // const [prospectTypes, setProspectTypes] = useState([])

  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    image: '',

  })
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()


  }


  useEffect(() => {


    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, image } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, image })


      axios.post(myApi + '/intraco/lpg/company_sister_concerns/getAccessHistory', {
        "company_sister_concern_id": props.item.id
      }, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {
        console.log(response.data.data)

        if (response.data.data) {
          setCompanyDetails(response.data.data)
        }
      })

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  const locateValueById = (types, id) => {
    let item = types.find(it => it.id === Number(id))
    return item
  }

  return (

    <div>
      {/* <div className="modal-dialog" style={{ maxWidth: '95%' }}>
         <div className="modal-content" style={{marginTop : '-35px'}}>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" />
          </div> */}

          {CompanyDetails.length === 0 &&
            <div className="modal-body">
              <div className="alert alert-primary" role="alert">
                Sorry, There is no data available !
              </div>
            </div>}

          {CompanyDetails.length !== 0 &&
            <div className="modal-body">
              <Table responsive hover>

                <TableHead>
                  <TableRow>
                    <TableCell className="px-0" style={{ width: "5%", textAlign : 'center' }}>#</TableCell>
                    <TableCell className="px-0" style={{ width: "20%", textAlign : 'center' }}>Sales Person</TableCell>
                    <TableCell className="px-0" style={{ width: "25%", textAlign : 'center' }}>Start Date</TableCell>
                    <TableCell className="px-0" style={{ width: "25%", textAlign : 'center' }}>End Date</TableCell>
                    <TableCell className="px-0" style={{ width: "25%", textAlign : 'center' }}>Reason</TableCell>

                  </TableRow>
                </TableHead>
                <TableBody>

                  {CompanyDetails
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((item, id) => (
                      <TableRow key={id}>
                        <TableCell className="px-0" style={{ width: "5%", textAlign : 'center' }}>
                         {page * rowsPerPage + id + 1}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left" style={{ width: "20%", textAlign : 'center' }}>
                          {item.sales_person_name}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left" style={{ width: "25%", textAlign : 'center' }}>
                          {moment(item.start_date).format('DD/MM/YYYY')}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left" style={{ width: "25%", textAlign : 'center' }}>
                          {item.end_date === "0000-00-00 00:00:00" || item.end_date === null ? " " : moment(item.end_date).format('DD/MM/YYYY')}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left" style={{ width: "25%", textAlign : 'center' }}>
                          {item.remove_reason}
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
              {CompanyDetails.length !== 0 && <TablePagination
                className="px-4"
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={CompanyDetails && CompanyDetails.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "Previous Page"
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page"
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />}
            </div>
          }

        </div>

    //   </div>
    // </div>
  )
}

export default CompanyDetails
