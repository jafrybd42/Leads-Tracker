/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';

import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
function SisterConcernDetails(props) {
  const [item, setItem] = useState([])
  const [form, setValues] = useState({
    id: 0,
    sister_concern: '',
    company: '',
    image: ''
  })

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()


  }


  useEffect(() => {

    if (props.item) {
      const { id, sister_concern,company  } = props.item
      setValues({ id,sister_concern, company })



      axios.get(myApi + '/intraco/lpg/company_sister_concerns/details/' + props.item.id, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {

        if (response.data.data) {
          setValues({

            sister_concern: response.data.data.name,
            company: response.data.data.company,


          })
        }
      })

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  return (


    <div >
      <div className="modal-dialog">
         <div className="modal-content" style={{marginTop : '-35px'}}>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" />
            <h4 className="modal-title" id="myModalLabel">More About <font color='green'>{props.item.sister_concern}</font></h4>
          </div>
          <div className="modal-body">
            <center>
              <img src='/assets/companies.png' name="aboutme" className="circular-image-medium"
              width={140} height={140} border={0} alt="profile_image" />
              <br></br>
              <br></br>
              <h3 className="media-heading">{props.item.sister_concern}</h3><br />
              <span><strong>Company : </strong></span>
              <span className="label label-warning">{props.item.company}</span>

            </center>
            <hr />

          </div>

        </div>
      </div>
    </div>


  )
}

export default SisterConcernDetails
