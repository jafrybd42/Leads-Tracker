import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'

import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";

import "date-fns";

import myApi from "app/auth/api";
import Select from 'react-select';



class CreateForm extends Component {
  constructor() {
    super();
    this.state = {
      selectedSaler: '',
      salesPersonList: [],
      selectedCompany: '',
      company: [],
      name: "",
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    // getting companyInformation
    axios.get(
      myApi + "/intraco/lpg/company/activeList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let companies = res.data.data.map(c => {
          return {
            value: c.id,
            label: c.title
          };
        });

        this.setState({
          company: [
            // {
            // value: 0,
            // label:
            //   "Select Company"
            // }
          ]
            .concat(companies)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting salerInformation
    axios.get(
      myApi + "/intraco/lpg/salesPerson/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let saler = res.data.data.map(c => {
          return {
            value: c.id,
            label: c.name
          };
        });

        this.setState({
          salesPersonList: [
            // {
            // value: 0,
            // label:
            //   "Select Salesperson"
            // }
          ]
            .concat(saler)
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }


  handleSubmit = event => {

    try {

      axios.post(myApi + '/intraco/lpg/company_sister_concerns/addSisterConcern',
        {
          "title": this.state.name,
          "company_id": Number(this.state.selectedCompany),
          "sales_person_id": Number(this.state.selectedSaler)
        }, {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token
        }
      })
        .then(res => {
          console.log(res)
          if (res.data.message) {
            if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

            if (res.data.error === false) {
              this.setState({
                name: '',
                selectedCompany: '',
                selectedSaler: ''
              })
            }
          }
        })
    } catch (err) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
  }


  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };


  onSelectChangeCompany = value => {
    if (value !== null) {
      this.setState({ selectedCompany: value.value });
    }
    if (value === null) {
      this.setState({ selectedCompany: "" });
    }
  };
  onSelectChangeSaler = value => {
    if (value !== null) {
      this.setState({ selectedSaler: value.value });
    }
    if (value === null) {
      this.setState({ selectedSaler: "" });
    }
  };

  render() {
    let { name } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12} >
              <div style={{ margin: '10px', marginLeft: '0px' }}><font color='grey' ><b>Select Company</b></font></div>
              {
                <Select
                  style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.company.filter(option => option.value === this.state.selectedCompany)}
                  isClearable="true"
                  name="selectedCompany"
                  options={this.state.company}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeCompany}
                />
              }
            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} >
              <div style={{ margin: '10px', marginLeft: '0px' }}><font color='grey' ><b>Select Salesperson</b></font></div>
              {
                <Select
                  style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  value={this.state.salesPersonList.filter(option => option.value === this.state.selectedSaler)}
                  isClearable="true"
                  name="selectedSaler"
                  options={this.state.salesPersonList}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeSaler}
                />
              }
            </Grid>
          </Grid>
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Sister Concern Name (Max length : 50)"
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={[
                  "required",
                  "maxStringLength: 50"
                ]}
                errorMessages={["this field is required"]}
              />
            </Grid>
          </Grid>

          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
