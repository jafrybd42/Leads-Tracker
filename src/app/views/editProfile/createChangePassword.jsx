import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SuperAdminEditProfile from "./CreateForm";

class CreateChangePassword extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/admin/edit" },
              { name: "Update Profile" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SuperAdminEditProfile /></Card>
      </div>
    );
  }
}

export default CreateChangePassword;
