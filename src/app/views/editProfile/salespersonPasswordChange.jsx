import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SalespersonChangepassword from "./salespersonChangePassword";

class SalespersonPasswordChange extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/salesperson/edit" },
              { name: "Update Profile" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SalespersonChangepassword /></Card>
      </div>
    );
  }
}

export default SalespersonPasswordChange;
