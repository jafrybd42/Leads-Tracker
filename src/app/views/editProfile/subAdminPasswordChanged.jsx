import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SubAdminChangePass from "./subAdminChangePass";

class SubAdminPasswordChanged extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/subadmin/edit" },
              { name: "Update Profile" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SubAdminChangePass /></Card>
      </div>
    );
  }
}

export default SubAdminPasswordChanged;
