import React from "react";
import { authRoles } from "../../auth/authRoles";

const editProfile = [
  {
    path: "/profile/admin/edit",
    component: React.lazy(() => import("./createChangePassword")),
    auth: authRoles.admin
    // auth: authRoles.editor
  },
  {
    path: "/profile/subadmin/edit",
    component: React.lazy(() => import("./subAdminPasswordChanged")),
    // auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/profile/salesperson/edit",
    component: React.lazy(() => import("./salespersonPasswordChange")),
    // auth: authRoles.admin,
    auth: authRoles.guest
  }
];

export default editProfile;
