import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,
  // Radio,
  // RadioGroup,
  // FormControlLabel,
  // Checkbox
} from "@material-ui/core";
// import {
//   MuiPickersUtilsProvider,
//   KeyboardDatePicker
// } from "@material-ui/pickers";
import "date-fns";
// import DateFnsUtils from "@date-io/date-fns";
// import themeOptions from "app/MatxLayout/MatxTheme/themeOptions";
//console.log(localStorageService.getItem('auth_user').token);

class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      name: "",
      questions: [],
      selectedQuestion: '',
      primary_phone_no: "",
      email: "",
      address: "",
      secondary_phone_no: "",
      quesAns: '',
    };
  }
  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    // let { user } = this.props

    // getting questions
    axios.get(
      myApi + "/intraco/lpg/question/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let questionsFromApi = res.data.data.map(question => {
          return {
            id: question.id,
            question_details: question.question_details
          };
        });

        this.setState({
          questions: [
            {
              id: 0,
              question_details:
                "Select question"
            }
          ]
            .concat(questionsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    event.preventDefault();
    //console.log(this.state.phone)
    // let baseURL;
    // //console.log({
    //   "id": localStorageService.getItem('auth_user').userId,
    //   "password_recovery_questions_id": parseInt(this.state.selectedQuestion),
    //   "question_ans": this.state.quesAns,
    //   "new_password": "12345678"
    // })
    axios.post(myApi + '/intraco/lpg/salesPerson/profile/questionUpdate', {
      "id": localStorageService.getItem('auth_user').userId,
      "password_recovery_questions_id": parseInt(this.state.selectedQuestion),
      "question_ans": this.state.quesAns,
    }, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        //console.log(res)
        if (res.data.error === false) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

          this.setState ({
            name : '',
            selectedQuestion: '',
            quesAns: '',
          })
        }
        // else {
          // if (!res.data.error) {
          //   Swal.fire({
          //     icon: "success",
          //     title: res.data.message,
          //     showConfirmButton: false,
          //   });
          // } else {
          //   Swal.fire({
          //     icon: "error",
          //     title: res.data.message,
          //     showConfirmButton: false,
          //   });
          // }
        // }
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    // //console.log(date);

    this.setState({ date });
  };

  render() {
    let { user } = this.props
    let {
      
      // name,
      // primary_phone_no,
      // secondary_phone_no,
      // email,
      // address,
      // question_details,
      // selectedQuestion,
      quesAns,

    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left' , marginTop : '20px'}}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedQuestion"
                value={this.state.selectedQuestion}
                onChange={e =>
                  this.setState({
                    selectedQuestion: e.target.value,
                    validationError:
                      e.target.value === ""
                        ? "You must select Question"
                        : 'select'
                  })
                }
              >
                {this.state.questions.map(question => (
                  <option
                    key={question.id}
                    value={question.id}
                  >
                    {question.question_details}
                  </option>
                ))}
              </select>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Your Answer"
                onChange={this.handleChange}
                type="text"
                name="quesAns"
                value={quesAns}
                // validators={["required", "isemail"]}
                errorMessages={["this field is required"]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
