import React from "react";
import { authRoles } from "../../auth/authRoles";

const ForgetPassRoutes = [
  {
    path: "/setting/forgetpassword",
    component: React.lazy(() => import("./forgetPasswordSetting")),
    // auth: authRoles.admin,
    // auth: authRoles.editor,
    auth: authRoles.guest,

  },
  {
    path: "/password/reset"
    // component: React.lazy(() => import("./clientManage")),
    // auth: authRoles.admin,
    // auth: authRoles.editor,
    // auth: authRoles.guest
  }
];

export default ForgetPassRoutes;
