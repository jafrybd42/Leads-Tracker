import React, { Component } from "react";
import { Breadcrumb } from "matx";
import AppointmentFilterSalesperson from "./appointmentFilterSaler";

class Filter extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Appointment", path: "/appointment/manage" },
              { name: "Search" }
            ]}
          />
        </div>
        <AppointmentFilterSalesperson />
      </div>
    );
  }
}

export default Filter;
