import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../../services/localStorageService"
import axios from 'axios'
import myApi from '../../../auth/api'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,
  Input

} from "@material-ui/core";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import Select from 'react-select';
import Moment from 'moment'

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
// import Input from ''


class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      appointment_date: '',
      car_number: '',
      driver_name: '',
      service_details: '',
      leads_corporate: [],
      discussionTypes_corporate: [],
      clients_corporate: [],
      prospectTypes_corporate: [],
      vehicleTypes_corporate: [],
      clients: [],
      selectedClient: "",
      propspectTypes: [],
      selectedProspectType: '',
      discussionTypes: [],
      selectedDiscussionType: "",
      CallTimeTypes: [],
      selectedCallTimeType: "",
      VehicleTypes: [],
      selectedVehicleType: "",
      Leads: [],
      selectedLead: "",

      validationError: '',
      date: '',
      time: new Date(),
      newTime: Moment(new Date()).format('HH:mm'),


      client_type_array: [],

      selectedClientType: '',
      selectedClientCorporate: '',
      selectedProspectTypeCorporate: '',
      selectedVehicleTypeCorporate: '',
      selectedLeadCorporate: '',

      selectedDiscussionTypeIndividual: '',
      selectedLeadIndividual: '',
      selectedProspectTypeIndividual: '',
      selectedVehicleTypeIndividual: '',
      selectedClientIndividual: '',

      selectedVehicleIndividualNew: [],
      selectedDiscussionIndividualNew: [],
      selectedVehicleCorporateNew: [],
      selectedDiscussionCorporateNew: [],
      defaultVehicles: [],
      isSubmitted: false
    }
  }

  componentDidMount() {
    console.log(this.props)

    this.setState({
      date: Moment(this.props.location.state.appointment_date).format('YYYY-MM-DD'),
      car_number: this.props.location.state.car_number,
      driver_name: this.props.location.state.driver_name,
      service_details: this.props.location.state.service_details,
      selectedClientType: this.props.location.state.client_type_id,
      selectedClientIndividual: Number(this.props.location.state.client_info &&
        this.props.location.state.client_info.map((e) => e.id).toString()),
    })




    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    // getting clientType
    axios.get(
      myApi + "/intraco/lpg/client_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.type,
            id: client.id,
            type: client.type
          };
        });

        this.setState({
          client_type_array: []
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });


    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        let clientsFromApi = res.data.data &&
          res.data.data
            .filter(x => Number(x.client_type_id) === Number(this.props.location.state.client_type_id))
            .map(client => {
              return {
                value: client.id,
                label: client.name,
              }
            })

        this.setState({
          clients: []
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        // console.log(error);
      });

    // getting discussionType
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            id: discussionType.id,
            type: discussionType.type
          };
        });

        this.setState({
          discussionTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(propspectType => {
          return {
            id: propspectType.id,
            type: propspectType.type,
            value: propspectType.id,
            label: propspectType.type,
          };
        });

        this.setState({
          propspectTypes: [

          ].concat(clientsFromApi)
        });

        this.setState({
          prospectTypes_corporate: [

          ].concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting vehicleType
    axios.get(
      myApi + "/intraco/lpg/vehicle_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(VehicleType => {
          return {
            id: VehicleType.id,
            type: VehicleType.type,
            value: VehicleType.id,
            label: VehicleType.type
          };
        });

        this.setState({
          VehicleTypes: [

          ]
            .concat(clientsFromApi)
        });
        this.setState({
          vehicleTypes_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting call type
    axios.get(
      myApi + "/intraco/lpg/call_time/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(CallTimeType => {
          return {
            id: CallTimeType.id,
            type: CallTimeType.type
          };
        });

        this.setState({
          CallTimeTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Leads
    axios.get(
      myApi + "/intraco/lpg/lead/activeList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data.map(Lead => {
          return {
            id: Lead.id,
            name: Lead.name,
            value: Lead.id,
            label: Lead.name
          };
        });

        this.setState({
          Leads: [

          ]
            .concat(clientsFromApi)
        });
        this.setState({
          leads_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/task/create/data",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data && res.data.data.corporateClient.map(cl => {
          return {
            value: cl.id,
            label: cl.company_title + '_' + cl.company_sister_concerns_title
          };
        });

        this.setState({
          clients_corporate: []
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting discussionType corporate
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            value: discussionType.id,
            label: discussionType.type
          };
        });

        this.setState({
          discussionTypes_corporate: []
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    console.log(this.state)

    axios.post(myApi + '/intraco/lpg/appointment/update', {
      'id': this.props.location.state.id,
      "client_type_id": this.state.selectedClientType,
      'client_id': this.state.selectedClientIndividual || this.state.selectedClientCorporate,
      'discussion_type_id': this.state.selectedDiscussionTypeIndividual || this.props.location.state.discussion_type_id,
      "service_details": this.state.service_details,
      "car_number": this.state.car_number,
      "driver_name": this.state.driver_name,
      "appointment_date": this.state.date
    }, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          if (res.data.error === false) {
            this.setState({
              selectedClient: "",
              selectedDiscussionType: "",
              selectedCallTimeType: "",
              selectedVehicleType: "",
              selectedLead: "",
            })
          }
        }
      })
  }

  handleSubmitCorporate = event => {

    axios.post(myApi + '/intraco/lpg/task/update', {
      'id': this.props.location.state.id,
      "client_id": this.state.selectedClientCorporate,
      "discussion_type_id": this.state.selectedDiscussionType || this.props.location.state.discussion_type.map(e => e.discussion_type_id),
      "client_type_id": this.state.selectedClientType,
      "vehicle_type_id": this.state.selectedVehicleTypeCorporate || this.props.location.state.vehicle_type.map(e => e.vehicle_type_id),
      "prospect_type": this.state.selectedProspectTypeCorporate ? this.state.selectedProspectTypeCorporate : this.props.location.state.prospect_type_id,
      "call_time": this.state.newTime ? this.state.newTime : Moment(new Date()).format('HH:mm'),
      "call_date": this.state.date,
      "lead": this.state.selectedLeadCorporate
    }, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        //console.log(res)
        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          if (res.data.error === false) {
            this.setState({
              selectedClientCorporate: "",
              selectedDiscussionType: "",
              selectedCallTimeType: "",
              selectedVehicleTypeCorporate: "",
              selectedLeadCorporate: "",
              selectedClientType: '',
              selectedProspectTypeCorporate: '',
            })
          }
        }
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });

  };

  handleDateChange = date => {


    date = Moment(date).format('YYYY-MM-DD')

    this.setState({ date });
  };

  handleTimeChange = (time) => {
    console.log({ time })
    this.setState(
      { time }
    );

    this.state.newTime = Moment(time).format('HH:mm')

  };

  onSelectChangeDays = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedClientType: value.value });
      if (this.props.location.state.client_type_id !== '') {
        // getting clientInformation
        axios.get(
          myApi + "/intraco/lpg/client/list",
          {
            headers: {
              'x-access-token': localStorageService.getItem('auth_user').token
            }
          }
        )
          .then(res => {
            let clientsFromApi = res.data.data &&
              res.data.data
                .filter(x => Number(x.client_type_id) === Number(this.props.location.state.client_type_id))
                .map(client => {
                  console.log(client)
                  return {
                    value: client.id,
                    label: client.name,
                  }
                })

            this.setState({
              clients: []
                .concat(clientsFromApi)
            });
            console.log(this.state.clients)
          })
          .catch(error => {
            // console.log(error);
          });
      }
    }
    if (value === null) {
      this.setState({ selectedClientType: "" });
      console.log(this.state.selectedClientType)
    }
  };

  onSelectChangeCorporateClient = value => {
    if (value !== null) {
      this.setState({ selectedClientCorporate: value.value });
      console.log(this.state.selectedClientCorporate)
    }
    if (value === null) {
      this.setState({ selectedClientCorporate: "" });
      console.log(this.state.selectedClientCorporate)
    }
  };


  onChange = (e) => {
    this.setState({

      [e.target.name]: e.target.value,
    });
  };
  //individual
  onSelectChangeIndividualClient = value => {
    if (value !== null) {
      this.setState({ selectedClientIndividual: value.value });
      console.log(this.state.selectedClientIndividual)
    }
    if (value === null) {
      this.setState({ selectedClientIndividual: "" });
      console.log(this.state.selectedClientIndividual)
    }
  };

  onSelectChangeIndividualDiscussionType = value => {
    if (value !== null) {
      // this.setState({ selectedDiscussionTypeIndividual: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedDiscussionTypeIndividual: value.value });
      this.setState({ selectedDiscussionIndividualNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });
    }
    if (value === null) {
      this.setState({ selectedDiscussionTypeIndividual: "" });
      console.log(this.state.selectedDiscussionTypeIndividual)
    }
  };




  render() {
    let { user } = this.props
    let {

      date,
      time

    } = this.state;
    return (
      <div>
        <Grid container spacing={6}>
          <Grid item lg={12} md={6} sm={12} xs={12} style={{ maxWidth: '100%' }}>
            <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong> <font color='black'>Select Client Type</font></strong></div>
            <Select
              style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
              value={this.props.location.state.client_type_id !== 0 ? this.state.client_type_array.filter(option => option.value === this.props.location.state.client_type_id && this.props.location.state.client_type_id) : this.state.client_type_array.filter(option => option.value === 0)}
              isClearable="true"
              name="selectedClientType"
              options={this.state.client_type_array}
              className="basic-multi-select"
              classNamePrefix="select"
              onChange={this.onSelectChangeDays}
              isDisabled
            />

          </Grid>
        </Grid>

        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ marginTop: '0px' }}>
              <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                <font color='black'>Select Client</font>
              </strong></div>

              {this.state.selectedClientType === '' ?

                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  isClearable="true"
                  name="selectedClientIndividual"
                  className="basic-multi-select"
                  classNamePrefix="select"
                />
                :
                this.state.selectedClientType === 0 && this.state.selectedClientType !== '' ?
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    value={this.state.selectedClientCorporate !== '' ? this.state.clients_corporate.filter(option => option.value === this.state.selectedClientCorporate) : this.state.clients_corporate.filter(option => option.value === Number(this.props.location.state.client_info && this.props.location.state.client_info.map((e) => e.id).toString()))}
                    isClearable="true"
                    name="selectedClientIndividual"
                    options={this.state.clients_corporate}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeCorporateClient}
                  />
                  :
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    value={this.state.selectedClientIndividual !== '' ? this.state.clients.filter(option => option.value === this.state.selectedClientIndividual) : this.state.clients.filter(option => option.value === Number(this.props.location.state.client_info && this.props.location.state.client_info.map((e) => e.id).toString()))}
                    isClearable="true"
                    name="selectedClientIndividual"
                    options={this.state.clients}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeIndividualClient}
                  />
              }
              <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                <font color='black'>Select Discussion Type</font>
              </strong></div>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.selectedDiscussionTypeIndividual !== '' ? this.state.discussionTypes_corporate.filter(option => option.value === this.state.selectedDiscussionTypeIndividual) : this.state.discussionTypes_corporate.filter(option => option.value === this.props.location.state.discussion_type_id)}
                isClearable="true"
                name="selectedDiscussionTypeIndividual"
                options={this.state.discussionTypes_corporate}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeIndividualDiscussionType}
              />
              <div style={{ marginBottom: '15px', marginTop: '30px' }}><strong>
                <font color='black'>Driver Name </font>
              </strong></div>
              <Input
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left' }}

                type="text"
                name="driver_name"
                id="driver_name"
                onChange={this.onChange}
                value={this.state.driver_name}
              />

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <div style={{ marginBottom: '-10px', marginTop: '15px' }}><strong>
                <font color='black'>Select Date</font>
              </strong></div>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>

                <KeyboardDatePicker
                  style={{ marginTop: '20px' }}
                  className="mb-4 w-full"
                  margin="none"
                  id="mui-pickers-date"
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  value={date}
                  onChange={this.handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date"
                  }}
                />
              </MuiPickersUtilsProvider>

              <div style={{ marginBottom: '15px', marginTop: '35px' }}><strong>
                <font color='black'>Car Number</font>
              </strong></div>
              <Input
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left' }}

                type="text"
                name="car_number"
                id="car_number"
                onChange={this.onChange}
                value={this.state.car_number}
              />
              <div style={{ marginBottom: '15px', marginTop: '30px' }}><strong>
                <font color='black'>Service Details </font>
              </strong></div>
              <Input
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left' }}

                type="text"
                name="service_details"
                id="service_details"
                onChange={this.onChange}
                value={this.state.service_details}
              />

            </Grid>
          </Grid>

          <Button style={{ backgroundColor: '#007bff', marginTop: '15px' }} color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Update</span>
          </Button>

        </ValidatorForm>

      </div>
    );
  }
}



export default withRouter(CreateForm);
