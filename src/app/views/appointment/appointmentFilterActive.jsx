import React from "react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import { Container, Row, Col } from 'reactstrap'
import {

  Button,
  Icon,
  Grid,

} from "@material-ui/core";
import axios from "axios";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'
import { ValidatorForm } from "react-material-ui-form-validator";
import ActiveDataTable from "./activeDataTable";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Moment from 'moment'
import Select from 'react-select';
import Swal from "sweetalert2";

class AppointmentFilterActive extends React.Component {
  constructor(props) {
    super(props)
    const tableData = []


  }
  state = {
    clients_corporate: [],

    selectedClientIndividual: '',
    selectedClientCorporate: '',
    selectedClientType: '',
    client_type_array: [],
    client_type_arrayy: [],
    selectedVehicleIndividualNew: [],
    selectedDiscussionCorporateNew: [],
    prospectTypes: [],
    selectedProspectType: '',
    clients: [],
    selectedClient: "",
    sales: [],
    selectedSales: '',
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",

    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",

    },
    tableData: [],
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false

  };

  componentDidMount() {

    axios.post(myApi + '/intraco/lpg/appointment/searchActive',
      {

        "from_date": Moment(new Date()).format('YYYY-MM-DD'),
        "to_date": Moment(new Date()).format('YYYY-MM-DD')
      })

      .then((response) => {

        if (response.data.data.length !== 0) {
          this.setState({
            isSubmitted: true,
            tableData: Array.isArray(response.data.data) ? response.data.data : []
          })
        }
        else {
          this.setState({
            isSubmitted: false,
            tableData: []
          })
        }

      })
      .catch((error) => {
        console.log(error)
      })

    // getting clientType
    axios.get(
      myApi + "/intraco/lpg/client_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.type,
            id: client.id,
            type: client.type
          };
        });

        this.setState({
          client_type_array: [
            // {
            //   // value: 0,
            //   // label: 'Corporate Client',
            //   id: 0,
            //   type: 'Corporate Client',
            // }
          ]
            .concat(clientsFromApi)
        });
        this.setState({
          client_type_arrayy: [
            {
              value: 0,
              label: 'Corporate Client',
              id: 0,
              type: 'Corporate Client',
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });
    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name,
            value: client.id,
            label: client.name
          };
        });

        this.setState({
          clients: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/company_sister_concerns/allList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        console.log(res.data.data)
        let clientsFromApi = res.data.data && res.data.data.map(cl => {
          return {
            value: cl.id,
            label: cl.company + '_' + cl.sister_concern,
            id: cl.id,
            name: cl.company + '_' + cl.sister_concern
          };
        });

        this.setState({
          clients_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });
    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(propspectType => {
          return {
            value: propspectType.id,
            label: propspectType.type
          };
        });

        this.setState({
          prospectTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/salesPerson/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.name,
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          sales: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting discussionType
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            value: discussionType.id,
            label: discussionType.type,
            id: discussionType.id,
            type: discussionType.type
          };
        });

        this.setState({
          discussionTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting vehicleType
    axios.get(
      myApi + "/intraco/lpg/vehicle_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(VehicleType => {
          return {
            value: VehicleType.id,
            label: VehicleType.type
          };
        });

        this.setState({
          VehicleTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting call type
    axios.get(
      myApi + "/intraco/lpg/call_time/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(CallTimeType => {
          return {
            id: CallTimeType.id,
            type: CallTimeType.type
          };
        });

        this.setState({
          CallTimeTypes: [
            {
              id: '',
              type:
                "Select Call Time Type"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Leads
    axios.get(
      myApi + "/intraco/lpg/lead/activeList",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data.map(Lead => {
          return {
            value: Lead.id,
            label: Lead.name
          };
        });

        this.setState({
          Leads: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

  }
  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  hundleDateChange(startDate, endDate) {

    this.setState(() => ({

      endDate,
      startDate,
    }));
    if (startDate != null) {
      this.setState(() => ({
        startDateFormatted: startDate.format("YYYY-MM-DD"),
      }));
    }
    if (startDate == null) {
      this.setState(() => ({
        startDateFormatted: '',
      }));
    }
    if (endDate != null) {
      this.setState(() => ({
        endDateFormatted: endDate.format("YYYY-MM-DD"),
      }));
    }
    if (endDate == null) {
      this.setState(() => ({
        endDateFormatted: '',
      }));
    }
  }

  handleSubmit = () => {
    console.log(this.state)
    let reqData = {
      "client_type_id": this.state.selectedClientType,
      "client_id": this.state.selectedClientIndividual || this.state.selectedClientCorporate,
      "sales_person_id": this.state.selectedSales,
      "discussion_type_id": this.state.selectedDiscussionType,
      "from_date": this.state.startDateFormatted,
      "to_date": this.state.endDateFormatted
    }
    if (reqData.to_date === ('' || "Invalid date" || null) && reqData.from_date !== ('' || "Invalid date" || null)) {
      delete reqData.to_date
    }
    if (reqData.from_date === ('' || "Invalid date" || null) && reqData.to_date !== ('' || "Invalid date" || null)) {
      delete reqData.from_date
    }
    if (reqData.from_date === ('' || "Invalid date" || null) && reqData.to_date === ('' || "Invalid date" || null)) {
      axios.post(myApi + '/intraco/lpg/appointment/searchActive', {
        "client_type_id": this.state.selectedClientType,
        "client_id": this.state.selectedClientIndividual || this.state.selectedClientCorporate,
        "sales_person_id": this.state.selectedSales,
        "discussion_type_id": this.state.selectedDiscussionType,
        "from_date": Moment(new Date()).format('YYYY-MM-DD'),
        "to_date": Moment(new Date()).format('YYYY-MM-DD')
      }, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
        }
      })
        .then(res => {
          if (res.data.data) {
            if (res.data.data) {
              if (res.data.count === 0) {
                Swal.fire({
                  icon: "error",
                  title: "No Data Found",
                  showConfirmButton: false,
                });
              }
              if (res.data.data.length !== 0) {
                this.setState({
                  isSubmitted: true,
                  tableData: Array.isArray(res.data.data) ? res.data.data : []
                })
              }
              else {
                this.setState({
                  isSubmitted: false,
                  tableData: []
                })
              }
            }
          }
        })
    }

    else {
      axios.post(myApi + '/intraco/lpg/appointment/searchActive', reqData, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
        }
      })
        .then(res => {
          if (res.data.data) {
            if (res.data.count === 0) {
              Swal.fire({
                icon: "error",
                title: "No Data Found",
                showConfirmButton: false,
                timer: 1000,
              });
            }
            if (res.data.data.length !== 0) {
              this.setState({
                isSubmitted: true,
                tableData: Array.isArray(res.data.data) ? res.data.data : []
              })
            }
            else {
              this.setState({
                isSubmitted: false,
                tableData: []
              })
            }
          }
        })
    }
  }

  exportPDF = () => {
    const filterPosition = [90, 103, 116, 129, 142, 155];
    let filterPositionPointer = 0;
    const locateValueById = (types, id) => {
      let item = types.find((it) => it.id === Number(id));
      return item;
    };
    const addFooters = doc => {


      const pageCount = doc.internal.getNumberOfPages()
      var footer = new Image();
      footer.src = '/assets/footerPdf.png';

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i)

        doc.addImage(image, 'JPEG', pageWidth - 110, 0, 100, 100)
        doc.addImage(leftBar, 'JPEG', 0, 0, 16, 270)
        doc.addImage(footer, 'JPEG', 0, pageHeight - 60, pageWidth, 60)
      }
    }

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    doc.setFontSize(15);

    var leftBar = new Image();
    leftBar.src = '/assets/leftBar.png';
    var image = new Image();
    image.src = '/assets/logoNew.png';
    const title = "Appointment Report";
    const headers = [["SL", "Date", "Sales Person", "Client Type", "Client", "Car Number", "Driver Name",
      "Service Details", "Status"]];

    const data = this.state.tableData.map((ap, i) => [i + 1, Moment(ap.appointment_date).format("YYYY-MM-DD"), ap.sales_person, locateValueById(this.state.client_type_arrayy, ap.client_type_id) &&
      locateValueById(this.state.client_type_arrayy, ap.client_type_id).type, ap.client_info.map(e => e.name || e.company_title + "_" + e.title),
    ap.car_number, ap.driver_name, ap.service_details, ap.status]);




    doc.setFont('helvetica', 'bold')
    doc.setFontSize(15)
    doc.text(title, marginLeft, 35);
    doc.setFont('helvetica', 'normal')
    doc.setFontSize(10)

    const filterData =
      this.state.startDate &&
      "Date : " +
      Moment(this.state.startDate).format("YYYY-MM-DD") +
      " > " +
      Moment(this.state.endDate).format("YYYY-MM-DD");

    const startDateFilter = "Date : " +
      Moment(this.state.startDate).format("YYYY-MM-DD");

    const endDateFilter = "Date : " +
      Moment(this.state.endDate).format("YYYY-MM-DD");

    const todayFilter = "Date : " +
      Moment(new Date()).format("YYYY-MM-DD");

    this.state.startDate && this.state.endDate && doc.text(filterData, marginLeft, 85);
    this.state.startDate && !this.state.endDate && doc.text(startDateFilter, marginLeft, 85);
    !this.state.startDate && this.state.endDate && doc.text(endDateFilter, marginLeft, 85);
    !this.state.startDate && !this.state.endDate && doc.text(todayFilter, marginLeft, 85);
    //edit

    const filterDiscussion = this.state.selectedDiscussionType !== '' ? locateValueById(this.state.discussionTypes, Number(this.state.selectedDiscussionType)) && locateValueById(this.state.discussionTypes, Number(this.state.selectedDiscussionType)).type : ''
    const filterSales = this.state.selectedSales !== '' ? locateValueById(this.state.sales, Number(this.state.selectedSales)) && locateValueById(this.state.sales, Number(this.state.selectedSales)).name : ''
    const filterClientIndividual = this.state.selectedClientIndividual !== '' && this.state.selectedClientCorporate === '' ? locateValueById(this.state.clients, this.state.selectedClientIndividual) && locateValueById(this.state.clients, Number(this.state.selectedClientIndividual)).name : ''
    const filterClientCorporate = this.state.selectedClientIndividual === '' && this.state.selectedClientCorporate !== '' ? locateValueById(this.state.clients_corporate, Number(this.state.selectedClientCorporate)) && locateValueById(this.state.clients_corporate, Number(this.state.selectedClientCorporate)).name : ''
    const filterClientType = this.state.selectedClientType !== '' ? locateValueById(this.state.client_type_arrayy, this.state.selectedClientType) && locateValueById(this.state.client_type_arrayy, this.state.selectedClientType).type : ''

    doc.setFont("helvetica", "strong");
    doc.setFontSize(11);
    doc.text('Search By : ', marginLeft, 70);
    doc.setFont("helvetica", "regular");
    doc.setFontSize(9);
    doc.text('Generated On : ' + Moment(new Date()).format("YYYY-MM-DD"), marginLeft, 50);

    doc.setFont("helvetica", "normal");
    doc.setFontSize(10);
    this.state.selectedDiscussionType !== '' && doc.text('\r\nDiscussion type : ' + filterDiscussion, marginLeft, filterPosition[filterPositionPointer++])
    this.state.selectedSales !== '' && doc.text('\r\nSalesperson : ' + filterSales, marginLeft, filterPosition[filterPositionPointer++])
    this.state.selectedClientIndividual !== '' && this.state.selectedClientCorporate === '' && doc.text('\r\nClient : ' + filterClientIndividual, marginLeft, filterPosition[filterPositionPointer++])
    this.state.selectedClientIndividual === '' && this.state.selectedClientCorporate !== '' && doc.text('\r\nClient : ' + filterClientCorporate, marginLeft, filterPosition[filterPositionPointer++])
    this.state.selectedClientType !== '' && doc.text('\r\nClient Type : ' + filterClientType, marginLeft, filterPosition[filterPositionPointer++])

    //end
    let content = {
      startY: 30 + filterPosition[filterPositionPointer++],
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 150
      }
    };

    doc.autoTable(content);

    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc)

    doc.save("appointment_report(" + Moment(new Date()).format('YYYY-MM-DD') + ").pdf")
  }

  onSelectChangeIndividualClient = value => {
    if (value !== null) {
      this.setState({ selectedClientIndividual: value.value });
      console.log(this.state.selectedClientIndividual)
    }
    if (value === null) {
      this.setState({ selectedClientIndividual: "" });
      console.log(this.state.selectedClientIndividual)
    }
  };
  onSelectChange = value => {
    if (value !== null) {
      this.setState({ selectedSalesNew: value.value });
      console.log(this.state.selectedSalesNew)
    }
    if (value === null) {
      this.setState({ selectedSalesNew: "" });
      console.log(this.state.selectedSalesNew)
    }
  };


  onSelectChangeDiscussionType = value => {
    if (value !== null) {
      this.setState({ selectedDiscussionType: value.value });
      // this.setState({ selectedDiscussionType: Array.isArray(value) ? value.map(x => x.value) : [] });
      // this.setState({ selectedDiscussionCorporateNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });

    }
    if (value === null) {
      this.setState({ selectedDiscussionType: "" });
      console.log(this.state.selectedDiscussionType)
    }
  };

  onSelectChangeVehicleType = value => {
    if (value !== null) {
      this.setState({ selectedVehicleType: Array.isArray(value) ? value.map(x => x.value) : [] });
      this.setState({ selectedVehicleIndividualNew: Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [] });

      console.log(Array.isArray(value) ? value.map(x => ({ 'value': x.value, 'label': x.label })) : [])
      console.log(this.state.selectedVehicleIndividualNew)
    }
    if (value === null) {
      this.setState({ selectedVehicleType: "" });
      console.log(this.state.selectedVehicleTypeIndividual)
    }
  };

  onSelectChangeIndividualLeads = value => {
    if (value !== null) {
      this.setState({ selectedLead: value.value });
      console.log(this.state.selectedLead)
    }
    if (value === null) {
      this.setState({ selectedLead: "" });
      console.log(this.state.selectedLead)
    }
  };
  onSelectChangeIndividualProspectype = value => {
    if (value !== null) {
      this.setState({ selectedProspectType: value.value });
      console.log(this.state.selectedProspectType)
    }
    if (value === null) {
      this.setState({ selectedProspectType: "" });
      console.log(this.state.selectedProspectType)
    }
  };
  onSelectChangeClientType = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedClientType: value.value });
      axios.get(
        myApi + "/intraco/lpg/client/list",
        {
          headers: {
            'x-access-token': localStorageService.getItem('auth_user').token
          }
        }
      )
        .then(res => {
          let clientsFromApi = res.data.data &&

            res.data.data
              .filter(x => Number(x.client_type_id) === this.state.selectedClientType)
              .map(client => {
                // console.log(client)
                return {
                  value: client.id,
                  label: client.name,
                  id: client.id,
                  name: client.name,
                }
              })

          this.setState({
            clients: []
              .concat(clientsFromApi)
          });
          console.log(this.state.clients)
        })
        .catch(error => {
          // console.log(error);
        });
    }
    if (value === null) {
      this.setState({ selectedClientType: "" });
      console.log(this.state.selectedClientType)
    }
  };
  onSelectChangeSaler = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedSales: value.value });
      console.log(this.state.selectedSales)
    }
    if (value === null) {
      this.setState({ selectedSales: "" });
      console.log(this.state.selectedSales)
    }
  };
  onSelectChangeCorporateClient = value => {
    if (value !== null) {
      this.setState({ selectedClientCorporate: value.value });
      console.log(this.state.selectedClientCorporate)
    }
    if (value === null) {
      this.setState({ selectedClientCorporate: "" });
      console.log(this.state.selectedClientCorporate)
    }
  };
  render() {
    return (
      <div>
        {this.state.tableData &&
          <Row>
            <Col>

            </Col>
          </Row>
        }
        <br></br>
        <div>
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}

          >
            {localStorageService.getItem('auth_user').role !== 'SALES-PERSON' &&
              <Grid container spacing={6} >
                <Grid item lg={6} md={6} sm={12} xs={12} >

                  <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                    <font color='black'>Select Client Type </font>
                  </strong></div>
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    value={this.state.client_type_array.filter(option => option.value === this.state.selectedClientType)}

                    isClearable="true"
                    name="selectedClientType"
                    options={this.state.client_type_array}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeClientType}

                  />



                </Grid>
                <Grid item lg={6} md={6} sm={12} xs={12} >

                  <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                    <font color='black'>Select Client </font>
                  </strong></div>
                  {this.state.selectedClientType === '' ?
                    <Select
                      style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                      isClearable="true"
                      name="selectedClientIndividual"
                      className="basic-multi-select"
                      classNamePrefix="select"
                    />
                    :
                    this.state.selectedClientType === 0 ?
                      <Select
                        style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                        isClearable="true"
                        name="selectedClientIndividual"
                        options={this.state.clients_corporate}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        onChange={this.onSelectChangeCorporateClient}
                      />
                      :
                      <Select
                        style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                        isClearable="true"
                        name="selectedClientIndividual"
                        options={this.state.clients}
                        className="basic-multi-select"
                        classNamePrefix="select"
                        onChange={this.onSelectChangeIndividualClient}
                      />
                  }

                </Grid>
                {/* <Grid item lg={6} md={6} sm={12} xs={12} >
                  <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                    <font color='black'>Select Date  </font>
                  </strong></div>
                  <DateRangePicker
                    // required
                    isOutsideRange={() => false}
                    startDate={this.state.startDate}
                    startDateId="start_date_id"
                    endDate={this.state.endDate}
                    endDateId="end_date_id"
                    onDatesChange={({ startDate, endDate }) =>
                      this.hundleDateChange(startDate, endDate)
                    }
                    focusedInput={this.state.focusedInput}
                    onFocusChange={(focusedInput) => this.setState({ focusedInput })}
                  />
                </Grid> */}
              </Grid>
            }
            <Grid container spacing={6} >
              <Grid item lg={6} md={6} sm={12} xs={12} >

                <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                  <font color='black'>Select Salesperson </font>
                </strong></div>

                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  isClearable="true"
                  name="selectedClientType"
                  options={this.state.sales}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeSaler}

                />
                <div style={{ marginBottom: '15px', marginTop: '20px' }}><strong>
                  <font color='black'>Select Date  </font>
                </strong></div>
                <DateRangePicker
                  // required
                  isOutsideRange={() => false}
                  startDate={this.state.startDate}
                  startDateId="start_date_id"
                  endDate={this.state.endDate}
                  endDateId="end_date_id"
                  onDatesChange={({ startDate, endDate }) =>
                    this.hundleDateChange(startDate, endDate)
                  }
                  focusedInput={this.state.focusedInput}
                  onFocusChange={(focusedInput) => this.setState({ focusedInput })}
                />
              </Grid>

              <Grid item lg={6} md={6} sm={12} xs={12} >

                <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                  <font color='black'>Select Discussion Type </font>
                </strong></div>
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  // isMulti
                  isClearable="true"
                  name="selectedDiscussionType"
                  options={this.state.discussionTypes}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeDiscussionType}
                />

              </Grid>
              {/* <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '25%', }}>
                <div style={{ marginBottom: '15px', marginTop: '-10px' }}><strong>
                  <font color='black'>Select Discussion Type </font>
                </strong></div>

                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  isMulti
                  isClearable="true"
                  name="selectedDiscussionType"
                  options={this.state.discussionTypes}
                  className="basic-multi-select"
                  classNamePrefix="select"
                  onChange={this.onSelectChangeDiscussionType}
                />

              </Grid> */}
            </Grid>
            <Button onSubmit={this.handleSubmit} color="primary" variant="contained" type="submit"
              style={{ marginTop: '30px', }}>
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Search</span>
            </Button>
            {'   '}
          </ValidatorForm>
          {this.state.tableData.length !== 0 &&
            <button
              className="btn btn-primary"
              style={{ marginTop: '-60px', marginLeft: '85%' }}
              onClick={() => this.exportPDF()}>Generate Report</button>
          }
          <br></br><br></br>
          <br></br>
          {

            this.state.isSubmitted && this.state.tableData &&
            <ActiveDataTable items={this.state.tableData}
              options={{ search: true, paging: false, filtering: true, exportButton: true }}
            />

          }
        </div>
      </div>
    );
  }
}
export default AppointmentFilterActive;