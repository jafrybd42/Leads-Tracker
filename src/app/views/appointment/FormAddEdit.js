/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
import Moment from "moment";

function AddEditForm(props) {
  console.log(props.item.discussion_type_id)

  const [item, setItem] = useState([]);
  const [clientInfo, setClientInfo] = useState([]);
  const [clientTypes, setClientTypes] = useState([]);
  const [clients_corporate, setClientCorporateTypes] = useState([]);
  const [discussionTypes, setDiscussionTypes] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    driver_name: props.item.driver_name,
    car_number: props.item.car_number,
    service_details: props.item.service_details,
    selectedClient: "",
    validationError: "",
    client_info: props.item.client_info,
    client_id: '',
    date: "",
    client_type_id: props.item.client_type_id,
    appointment_date: null,
    discussion_type_id: props.item.discussion_type_id,
    client_corporate_id: ''
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    console.log(form)
    e.preventDefault();

    axios
      .post(
        myApi + "/intraco/lpg/appointment/update",
        {
          id: form.id,
          client_type_id: form.client_type_id,
          client_id: form.client_id || form.client_corporate_id,
          discussion_type_id: form.discussion_type_id,
          appointment_date:
            form.appointment_date === null
              ? Moment(props.item.appointment_date).format("YYYY-MM-DD")
              : Moment(form.appointment_date).format("YYYY-MM-DD"),
          car_number: form.car_number,
          service_details: form.service_details,
          driver_name: form.driver_name,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((response) => {
        props.toggle();
        Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        if (response.data.error === false) {
          props.updateState(form);
        }
      })

      .catch((err) => console.log(err));
  };

  useEffect(() => {
    // getting clientType
    axios
      .get(myApi + "/intraco/lpg/client/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            name: client.name,
          };
        });

        setClientTypes(
          [
            {
              id: "",
              name: "Select if you want to change",
            },
          ].concat(clientsFromApi)
        );
        console.log(clientTypes);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/appointment/create/data",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        console.log(res)
        let clientsFromApi = res.data.data && res.data.data.corporateClient.map(cl => {
          return {
            id: cl.id,
            name: cl.company_title + '_' + cl.company_sister_concerns_title
          };
        });

        setClientCorporateTypes(
          [
            {
              id: "",
              name: "Select if you want to change",
            },
          ]
            .concat(clientsFromApi)
        );
        console.log(clients_corporate)
      })
      .catch(error => {
        console.log(error);
      });

    // getting discussionType
    axios
      .get(myApi + "/intraco/lpg/discussion_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setDiscussionTypes(
          [
            {
              id: "",
              type: "Select if you want to change",
            },
          ].concat(clientsFromApi)
        );
        console.log(clientTypes);
      })
      .catch((error) => {
        console.log(error);
      });
    console.log(props)
    if (props.item) {
      const {
        id,
        car_number,
        // driver_name,
        service_details,
        appointment_date,
        client_info,
        discussion_type_id
      } = props.item;
      setValues({
        id,
        car_number,
        // driver_name,
        service_details,
        appointment_date,
        client_info,
        discussion_type_id
      });
      setValues
        ({ driver_name: props.item.driver_name })
      // props.item.client_info &&
      //   setValues({
      //     client_id: props.item.client_info.map((e) => e.id),
      //     client_corporate_id: props.item.client_info.map((e) => e.id),
      //   })
      // console.log(form.discussion_type_id)
      //       props.item && setValues({
      // discussion_type_id : props.item.discussion_type_id
      //       })
      props.item && setValues({
        id: props.item.id,
        // client_info: props.item.client_info,
        discussion_type_id: props.item.discussion_type_id,
        client_type_id: props.item.client_type_id,
        sales_person_id: props.item.sales_person_id,
        service_details: props.item.service_details,
        driver_name: props.item.driver_name,
        car_number: props.item.car_number,
        client_id: Number(props.item.client_info.map((e) => e.id).toString()),
        client_corporate_id: Number(props.item.client_info.map((e) => e.id).toString()),
        appointment_date: Moment(props.item.appointment_date).format("YYYY-MM-DD")
      })
      // console.log(form.discussion_type_id)

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false);
  console.log(form.discussion_type_id)

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="client_id">Client</Label>
        {
          props.item.client_type_id === 1 ?
            <select
              style={{
                width: "100%",
                padding: "5px",
                backgroundColor: "white",
                color: "#444",
                borderBottomColor: "#949494",
                textAlign: "left",
                marginTop: "20px",
              }}
              className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
              name="client_id"
              value={form.client_id === null ? "" : form.client_id}
              onChange={onChange}
            >
              {clientTypes.map((client) => (
                <option key={client.id} value={client.id}>
                  {client.name}
                </option>
              ))}
            </select> : (
              <select
                style={{
                  width: "100%",
                  padding: "5px",
                  backgroundColor: "white",
                  color: "#444",
                  borderBottomColor: "#949494",
                  textAlign: "left",
                  marginTop: "20px",
                }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="client_corporate_id"
                value={form.client_corporate_id === null ? "" : form.client_corporate_id}
                onChange={onChange}
              >
                {clients_corporate.map((client) => (
                  <option key={client.id} value={client.id}>
                    {client.name}
                  </option>
                ))}
              </select>
            )
        }
      </FormGroup>
      <FormGroup>
        <Label for="selectedDiscussionType">Discussion Type</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="discussion_type_id"
          value={form.discussion_type_id === null ? "" : form.discussion_type_id}
          onChange={onChange}
        >
          {discussionTypes.map((client) => (
            <option key={client.id} value={client.id}>
              {client.type}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <Label for="name">Driver Name</Label>
        <Input
          type="text"
          name="driver_name"
          id="driver_name"
          onChange={onChange}
          value={form.driver_name === null ? "blank" : form.driver_name}
          minLength="3"
          required
        />
      </FormGroup>

      <FormGroup>
        <Label for="email">Car Number</Label>
        <Input
          type="text"
          name="car_number"
          id="car_number"
          onChange={onChange}
          value={form.car_number === null ? "" : form.car_number}
          minLength="3"
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for="phone_number">Car Service</Label>
        <Input
          type="text"
          name="service_details"
          id="service_details"
          onChange={onChange}
          value={form.service_details === null ? "" : form.service_details}
          placeholder="ex. ABC-555-5555"
          required
        />
      </FormGroup>

      <FormGroup>
        <Label for="address">Date</Label>
        <Input
          type="date"
          name="appointment_date"
          id="appointment_date"
          onChange={onChange}
          value={
            form.appointment_date === null
              ? Moment(props.item.appointment_date).format("YYYY-MM-DD")
              : Moment(form.appointment_date).format("YYYY-MM-DD")
          }
        />
      </FormGroup>
      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
