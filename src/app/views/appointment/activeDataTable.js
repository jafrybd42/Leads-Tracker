/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import {
  IconButton,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination,
} from "@material-ui/core";
import { Button } from "reactstrap";
import axios from "axios";
import Axios from "axios";
import Moment from "moment";
import ModalForm from "./Modal";
import localStorageService from "app/services/localStorageService";
import history from "../../../history";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import TaskModal from "../report/taskModal";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

export default function BasicDatatable(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [client_type_array, setClientType] = useState([]);
  const [client_type_arrayy, setClientTypeArray] = useState([]);
  const [form, setValues] = useState({
    id: "",
    appointment_date: "",
    car_number: "",
    client_info: [],
    client_type_id: "",
    discussion_type: "",
    discussion_type_id: "",
    driver_name: "",
    sales_person: "",
    service_details: "",
    status: "",
  });
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const deleteItem = (id) => {
    Swal.fire({
      title: "Warning!",
      text: "Give a reason , why you want to delete !",
      input: "text",
      showCancelButton: true,
    }).then((result) => {
      if (result.value) {
        axios
          .post(
            myApi + "/intraco/lpg/appointment/delete",
            { id: id, reason_for_delete: result.value },
            {
              headers: {
                "x-access-token": localStorageService.getItem("auth_user")
                  .token,
              },
            }
          )
          .then((response) => {})
          .then((item) => {
            props.deleteItemFromState(id);
            Swal.fire("Deleted Successfully !");
          })
          .catch((err) => console.log(err));
      }
    });
  };
  const items = props.items && props.items;
  useEffect(() => {
    // getting clientType
    Axios.get(myApi + "/intraco/lpg/client_type/list", {
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            id: client.id,
            type: client.type,
          };
        });

        setClientType([].concat(clientsFromApi));
        setClientTypeArray(
          [
            {
              value: 0,
              label: "Corporate Client",
              id: 0,
              type: "Corporate Client",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  }, false);
  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };
  const handleCompleteTask = (item) => {
    axios
      .post(
        myApi + "/intraco/lpg/appointment/complete",
        {
          apponitment_id: item.id,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )
      .then((response) => {
        if (response.data.error === false) {
          Swal.fire({
            icon: "success",
            title: "Completed Successful",
            showConfirmButton: false,
            timer: 1000,
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1000,
          });
        }

        if (response.data.error === false) {
          props.updateState({
            id: item.id,
            appointment_date: item.appointment_date,
            car_number: item.car_number,
            client_info: item.client_info,
            client_type_id: item.client_type_id,
            discussion_type: item.discussion_type,
            discussion_type_id: item.discussion_type_id,
            driver_name: item.driver_name,
            sales_person: item.sales_person,
            service_details: item.service_details,
            status: "complete",
          });
        }
      })
      .catch((error) => {
        // console.log(error);
      });
  };
  const classes = useStyles();
  return (
    <>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fafafa",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                className="px-0"
                style={{ width: "5%", textAlign: "center" }}
              >
                #
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Appointment Date
              </TableCell>
              {localStorageService.getItem("auth_user") &&
                localStorageService.getItem("auth_user").role !==
                  "SALES-PERSON" && (
                  <TableCell
                    align="center"
                    className="px-0"
                    style={{ width: "15%" }}
                  >
                    Sales Person
                  </TableCell>
                )}
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "20%" }}
              >
                Client
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Client Type
              </TableCell>

              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Details
              </TableCell>
              {localStorageService.getItem("auth_user") &&
                localStorageService.getItem("auth_user").role ===
                  "SALES-PERSON" && (
                  <TableCell
                    align="center"
                    className="px-0"
                    style={{ width: "15%" }}
                  >
                    Action
                  </TableCell>
                )}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell align="center" className="px-0 capitalize">
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell align="center" className="px-0 capitalize">
                      {Moment(item.appointment_date).format("YYYY-MM-DD")}
                    </TableCell>
                    {localStorageService.getItem("auth_user") &&
                      localStorageService.getItem("auth_user").role !==
                        "SALES-PERSON" && (
                        <TableCell className="px-0 capitalize" align="center">
                          {item.sales_person}
                        </TableCell>
                      )}
                    <TableCell className="px-0 capitalize" align="center">
                      {item.client_info &&
                        item.client_info.map((e) =>
                          e.name ? (
                            <a
                              // href=''
                              onClick={() =>
                                history.push({
                                  pathname:
                                    "/individual/" +
                                    Number(
                                      item.client_info
                                        .map((e) => e.id)
                                        .toString()
                                    ),
                                  state: item,
                                })
                              }
                            >
                              {" "}
                              <font
                                color="blue"
                                style={{ cursor: "pointer", fontWeight: "500" }}
                              >
                                {e.name}
                              </font>
                            </a>
                          ) : (
                            <a
                              // href=''
                              onClick={() =>
                                history.push({
                                  pathname:
                                    "/client/member/list/" +
                                    Number(
                                      item.client_info
                                        .map((e) => e.id)
                                        .toString()
                                    ),
                                  state: Number(
                                    item.client_info.map((e) => e.id).toString()
                                  ),
                                })
                              }
                            >
                              {" "}
                              <font
                                color="blue"
                                style={{ cursor: "pointer", fontWeight: "500" }}
                              >
                                {e.company_title + "_" + e.title}{" "}
                              </font>
                            </a>
                          )
                        )}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      {locateValueById(
                        client_type_arrayy,
                        item.client_type_id
                      ) &&
                        locateValueById(client_type_arrayy, item.client_type_id)
                          .type}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      {item.status && item.status === "complete" ? (
                        <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                          Completed
                        </small>
                      ) : (
                        <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                          Incomplete
                        </small>
                      )}{" "}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      <TaskModal
                        style={{ maxWidth: "700px!important" }}
                        buttonLabel="View"
                        item={item}
                      />
                    </TableCell>
                    {localStorageService.getItem("auth_user") &&
                      localStorageService.getItem("auth_user").role ===
                        "SALES-PERSON" && (
                        <TableCell className="px-0 capitalize" align="center">
                          {item.status && item.status === "complete" ? (
                            <font color="grey">Not available</font>
                          ) : (
                            Moment(item.appointment_date).format(
                              "YYYY-MM-DD"
                            ) === Moment(new Date()).format("YYYY-MM-DD") && (
                              <IconButton
                                onClick={() => handleCompleteTask(item)}
                              >
                                <Icon color="primary">done</Icon>
                              </IconButton>
                            )
                          )}
                          {item.status && item.status !== "complete" && (
                            <Button
                              onClick={() =>
                                history.push({
                                  pathname: "/appointment/edit/" + item.id,
                                  state: item,
                                })
                              }
                              item={item}
                              color="btn"
                              style={{ marginLeft: "0px", marginTop: "5px" }}
                            >
                              <span
                                className="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                                aria-hidden="true"
                                title="edit"
                              >
                                edit
                              </span>
                            </Button>
                          )}
                          {item.status && item.status !== "complete" && (
                            <Button
                              color="btn"
                              onClick={() => deleteItem(item.id)}
                            >
                              <span
                                className="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                                aria-hidden="true"
                                title="delete"
                              >
                                delete
                              </span>
                            </Button>
                          )}
                        </TableCell>
                      )}
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items && props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
}
