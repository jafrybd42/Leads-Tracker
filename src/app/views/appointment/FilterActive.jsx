import React, { Component } from "react";
import { Breadcrumb } from "matx";
import AppointmentFilterActive from "./appointmentFilterActive";

class Filter extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Appointment", path: "/appointment/search/active" },
              { name: "Search" }
            ]}
          />
        </div>
        <AppointmentFilterActive />
      </div>
    );
  }
}

export default Filter;
